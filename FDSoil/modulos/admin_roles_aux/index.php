<?php
use \FDSoil\Func as Func;

class SubIndex
{
    public function execute()
    {
        \FDSoil\Audit::validaReferenc();
        $aView['include'] = Func::getFileJSON(__DIR__."/js/include.json");
        $aView['userData'] = Func::usuarioData();
        $xtpl = new \FDSoil\XTemplate(__DIR__."/view.html");  
        Func::appShowId($xtpl);  
        $xtpl->assign('ID', (isset($_POST['id']))?$_POST['id']:0);
        $rowRol = \FDSoil\Usuario\Rol::rolRow();   
        $xtpl->assign('ROL_NAME', $rowRol[0]);  
        $xtpl->assign('ROL_PAG_INI_DEFAULT', $rowRol[2]);  
        $aView['load'] = "[".$_SESSION['menu'].",".\FDSoil\Usuario\Rol\Menu::menuOptMake().",".\FDSoil\Usuario\Rol::rolOpt()."]";
        $result = \FDSoil\Usuario\Rol\Menu::menuStatusList(); 
        while ($row = \FDSoil\DbFunc::fetchRow($result)) {
            $xtpl->assign('ID_STATUS', $row[0]);
            $xtpl->assign('DES_STATUS', $row[1]);
            if (!empty($_POST)) 
                $xtpl->assign('SELECTED_STATUS', ($rowRol[1] == $row[0]) ? 'selected' : '');
            $xtpl->parse('main.status');
        }
        Func::btnsPutPanel( $xtpl, [["btnName"=>"Return"], ["btnName"=>"Save", "btnClick"=>"sweepTable();"]]);
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    } 
}

