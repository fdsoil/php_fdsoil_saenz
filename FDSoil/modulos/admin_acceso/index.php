<?php
use \FDSoil\Func as Func;

class SubIndex
{
    public function execute($aReqs)
    {
        Func::validarPaquetesRequeridos();
        $aView['include'] = Func::getFileJSON(__DIR__."/js/include.json");
        $aView['userData'] = "";
        $aView['load'] = [];
        $xtpl = new \FDSoil\XTemplate(__DIR__."/view.html");  
        Func::appShowId($xtpl);
        $msg='';
        if ($aReqs['params']!=[]){
            if (@$aReqs['params'][0]==1)
                $msg='Usuario o Clave Incorrectos';
            else if (@$aReqs['params'][0]==2)
                $msg='El Usuario no está Activo. Comuníquese con un Administrador del Sistema';
        }
        $xtpl->assign('MENSAJE', $msg);   
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    }
}

