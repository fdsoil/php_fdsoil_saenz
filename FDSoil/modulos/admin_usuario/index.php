<?php
use \FDSoil\Usuario\Rol\Menu as Menu;
use \FDSoil\XTemplate as XTemplate;
use \FDSoil\DbFunc as DbFunc;
use \FDSoil\Func as Func;


class SubIndex
{
    public function execute()
    {
        \FDSoil\Audit::validaReferenc();
        $aView['include'] = Func::getFileJSON(__DIR__."/js/include.json");
        $aView['userData'] = Func::usuarioData();
        $aView['load'] = $_SESSION['menu'];
        $xtpl = new XTemplate(__DIR__."/view.html");
        Func::appShowId($xtpl);
        for ($i = 0; $i < 5; $i++)
            $aSolapa[$i] = self::solapa($i,$obj);

        $aSolapa[5] = self::solapa5($obj);
        $aSolapa[6] = self::solapa6($obj);

        Func::bldSolapas($xtpl, $aSolapa);
        Func::btnsPutPanel( $xtpl, [["btnName"=>"Exit"]  ,
                                    ["btnName"=>"Filter"   , "btnClick"=>"regresar();"            , "btnDisplay"=>"none"],
                                    ["btnName"=>"Search" , "btnClick"=>"sendUsuariosListGet();" , "btnLabel"=>"Usuarios"],
                                    ["btnName"=>"Add"    , "btnLabel"=>"Usuario" ] ]);
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    }

    private function solapa($n,$obj)
    {
        $xtpl = new XTemplate(__DIR__."/solapa".$n.".html");
        Func::appShowId($xtpl);
        $xtpl->parse('main');
        return $xtpl->out_var('main');
    }

    private function solapa5($obj)
    {
        $xtpl = new XTemplate(__DIR__."/solapa5.html");
        Func::appShowId($xtpl);
        $result = FDSoil\Usuario\Rol::rolList();
        while ($row = DbFunc::fetchRow($result)) {
            $xtpl->assign('ID_ROL', $row[0]);
            $xtpl->assign('DES_ROL', $row[1]);
            $xtpl->parse('main.roles');
        }

        $xtpl->parse('main');
        return $xtpl->out_var('main');
    }

    private function solapa6($obj)
    {
        $xtpl = new XTemplate(__DIR__."/solapa6.html");
        Func::appShowId($xtpl);
        $result = Menu::menuStatusListLessActive();
        while ($row = DbFunc::fetchRow($result)) {
            $xtpl->assign('ID_STATUS', $row[0]);
            $xtpl->assign('DES_STATUS', $row[1]);
            $xtpl->parse('main.status_less_active');
        }
        $result = Menu::menuStatusListOnlyActive();
        while ($row = DbFunc::fetchRow($result)) {
            $xtpl->assign('ID_STATUS', $row[0]);
            $xtpl->assign('DES_STATUS', $row[1]);
            $xtpl->parse('main.status_only_active');
        }
        $xtpl->parse('main');
        return $xtpl->out_var('main');
    }
}

