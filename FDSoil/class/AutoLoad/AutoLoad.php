<?php
namespace FDSoil;

class AutoLoad
{
  
    public function autoRequireOnce()
    {
        spl_autoload_register(function ($sNameSpaceClass)
        {
            $aNameSpaceClass=explode('\\',$sNameSpaceClass);
            $classNamePHP = self::classNamePHP( $aNameSpaceClass ) ;
            if (!file_exists($classNamePHP)) {
                die(
                    "<center>La clase: <strong>'".
                    $classNamePHP.
                    "'</strong> con espacio de nombre ".
                    $sNameSpaceClass.
                    " no existe. </center>"
                );
            }
            require_once $classNamePHP;
        });
    }

    private function classNamePHP($aNameSpaceClass)
    {
        return '../../../'.$_SESSION[$aNameSpaceClass[0]].'/class/'.$aNameSpaceClass[1].'/'.end($aNameSpaceClass).'.php';
    } 
}

