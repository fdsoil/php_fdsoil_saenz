<?php
namespace FDSoil;

require("../../../" . $_SESSION ['FDSoil'] . "/packs/tcpdf/config/lang/eng.php");
require("../../../" . $_SESSION ['FDSoil'] . "/packs/tcpdf/tcpdf.php");

class TCPDF extends \TCPDF
{
    public function Header()
    {
        $this->Image("../../../" . $_SESSION ['appOrg'] .'/img/header.jpg', 25, 10, 170, 15);
        $this->Cell(30, 1, $this->title, 0, 0, 'C');
    }
}

