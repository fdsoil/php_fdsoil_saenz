<?php

trait DbFuncIniRegist
{

    /** Inicializar Registro
    * Descripción: Inicializa un arreglo que representa un registro en blanco de una tabla de la base de datos.
    * Si es por defecto $schemaName es 'simple', lo busca dentro de la base de dato, no importa el esquema donde este ubicado.
    * En caso contrario, especifique el esquema donde esta ubicada la tabla.
    * @param string $tableName Nombre de la tabla que representa al registro.
    * @param string $schemaName Nombre del esquema donde esta la tabla.
    * @return array $array Devuelve un arreglo que representa un registro en blanco de tablaName.*/
    function iniRegist($tableName, $schemaName='simple', $dbName=null)
    {
        $arrName['table'] = $tableName;
        $arrName['ANDschema']=($schemaName=='simple')?' ':" AND A.table_schema='".$schemaName."'";
        $matrix = \FDSoil\DbFunc::fetchAll(
            self::exeQryFile( 
                "../../../" . $_SESSION['FDSoil'] . "/class/DbFunc/sql/IniReg/ini_reg_select.sql", 
                $arrName, false, '', $dbName 
            )
        );
        if (($schemaName=='simple' && $matrix[0]['tbl_tot']==1) || ($schemaName!='simple' && $matrix[0]['tbl_tot']==1)) {
            $array=[];
            foreach ($matrix as $row ){
                $array[$row['column_name']] = '';
                if ($row['data_type'] == 'boolean')
                    $array[$row['column_name']] .= 'f';
                if ($row['data_type'] == 'text')
                    $array[$row['column_name']] .= '';
                if ($row['data_type'] == 'character')
                    $array[$row['column_name']] .= '';
                if ($row['data_type'] == 'character varying')
                    $array[$row['column_name']] .= '';
                if ($row['data_type'] == 'date')
                    $array[$row['column_name']] .= date('Y-m-d');
                if ($row['data_type'] == 'timestamp without time zone')
                    $array[$row['column_name']] .= date('Y-m-d');
                if ($row['data_type'] == 'time without time zone')
                    $array[$row['column_name']] .= '00:00:00';
                if ($row['data_type'] == 'integer')
                    $array[$row['column_name']] .= 0;
                if ($row['data_type'] == 'double precision')
                    $array[$row['column_name']] .= 0;
                if ($row['data_type'] == 'smallint')
                    $array[$row['column_name']] .= 0;
            }
            return $array;
        }
        else if($schemaName == 'simple' && $matrix[0]['tbl_tot'] > 1)
            die("<center>Disculpe: El nombre de la tabla <b>$tableName</b> se encuentra creada en (<b>".$matrix[0]['tbl_tot']."</b>) esquemas de la Base de Dato. <br> Especifique el esquema.</center>");
        else
            die("<center>Disculpe: La tabla <b>$tableName</b> no está creada en la Base de Datos. <br> Revisar esquema y/o nombre de la tabla.");
    }

}


