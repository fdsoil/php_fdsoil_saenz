<?php

trait DbFuncResult
{
   
    private function _dbms($db)
    {
        require ('../../../'.$_SESSION['myApp'].'/config/db.php');
        return "\FDSoil\\".$dbms.'\\Resolution';
    }

    public function numFields($result, $db=null) { return self::_dbms($db)::numFields($result); }

    public function numRows($result, $db=null) { return self::_dbms($db)::numRows($result); }

    public function affectedRows($result, $db=null) { return self::_dbms($db)::affectedRows($result); }

    public function fetchArray($result, $db=null) { return ( self::numRows($result) === 0 ) ? [] : self::_dbms($db)::fetchArray($result); }

    public function fetchAssoc($result, $db=null) { return ( self::numRows($result) === 0 ) ? [] : self::_dbms($db)::fetchAssoc($result); }

    public function fetchRow($result, $db=null) { return ( self::numRows($result) === 0 ) ? [] : self::_dbms($db)::fetchRow($result); }

    public function fetchAll($result, $db=null) { return ( self::numRows($result) === 0 ) ? [] : self::_dbms($db)::fetchAll($result); }

    public function fetchAllArray($result, $db=null) { return ( self::numRows($result, $db) === 0 ) ? [] : self::resultToArray($result, 'Array'); }

    public function fetchAllAssoc($result, $db=null) { return ( self::numRows($result, $db) === 0 ) ? [] : self::resultToArray($result, 'Assoc'); }

    public function fetchAllRow($result, $db=null) { return ( self::numRows($result, $db) === 0 ) ? [] : self::resultToArray($result, 'Row'); }

    //public function fetchResult($result, $db=null) { return self::_dbms($db)::fetchResult($result); }    

    /** Resultado a arreglo bidireccional de índices numéricos y/o asociativos a partir de un resultado.
    * Descripción: Devuelve arreglo bidimencional de índices numéricos y/o asociativos a partir de un resultado.
    * @param result $result Resultado de la ejecución de un select
    * @param result $type Indica el tipo de extraccion: registro, asociativo o arreglo (por defecto).
    * @return array $arr Arreglo de índices numéricos y/o asociativos a partir de un resultado.*/
    public function resultToArray($result, $type = 'Array')
    {   
        if ( in_array( $type , [ 'Array', 'Assoc', 'Row', null ] ) ) {
            $arr = [];
            $j = 0;
            $funcionExtraer = 'fetch'.$type;
            while ($regist = self::$funcionExtraer($result))
    	        $arr[$j++] = $regist;
            return $arr;
        } else
            die("El tipo de array solo acepta 'Arreglo' (por defecto), 'Asociativo' o 'Registro'. ". "No puede ser :". $type .".");
    }

    /** Resultado a arreglo de cadena de caracteres.
    * Descripción: Devuelve una cadena de caracteres con separadores especiales que conforman un arreglo.
    *
    * Nota: Al hacer 'split' o 'splot' al retorno obtenemos una matriz. Los separadores deben ser distintos.
    *
    * @param result $result Resultado de la ejecución de un select
    * @param string $separate1 Caracter especial que separa los campos o columnas.
    * @param string $separate2 Caracter especial que separa los registros o filas.
    * @return array $array Caracteres con separadores especiales que conforman un arreglo.*/
    function resultToString($result,$separate1,$separate2, $db=null)
    {
        $col = self::numFields($result);
        $string='';
        while ($row =self::fetchRow($result, $db)) {
            for ($i = 0; $i < $col; $i++)
                $string.=$row[$i].$separate1;
            $string=substr($string,0,strlen($string)-1).$separate2;
        }
        return $string=substr($string,0,strlen($string)-1);
    }

}

