SELECT A.column_name AS column_name, A.data_type, 
    (SELECT COUNT(table_name) FROM information_schema.tables A
        WHERE table_name = '{fld:table}' {fld:ANDschema}
            GROUP BY table_name) AS tbl_tot
FROM information_schema.columns A
LEFT JOIN information_schema.element_types B ON A.table_catalog = B.object_catalog 
						AND A.table_schema = B.object_schema
					        AND A.table_name = B.object_name
WHERE A.table_name = '{fld:table}' {fld:ANDschema}
ORDER BY A.ordinal_position;