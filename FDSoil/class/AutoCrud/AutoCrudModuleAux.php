<?php

trait AutoCrudModuleAux
{

    private function _bldModuleAux()
    {

        self::_dirBld("modulos/".$this->_arr['table']."_aux/");
        self::_bldFileIndexPhpAux();
        self::_bldFileViewHtmlAux();
        self::_bldFileSavePhpAux();
        self::_dirBld("modulos/".$this->_arr['table']."_aux/js");
        self::_bldFileJsIncludeJsonAux();
        self::_bldFileJsInicioJsAux();
        self::_bldFileJsValidarJsAux();
        self::_bldFileJsIndexPhpAux();

    }

    private function _bldFileIndexPhpAux()
    {

        self::_fileBldStart("/modulos/".$this->_arr['table']."_aux/", "index.php" );

        fputs($this->_file,"<?php\n");
        fputs($this->_file,"use \\".$_SESSION['FDSoil']."\XTemplate as XTemplate;\n");
        fputs($this->_file,"use \\".$_SESSION['FDSoil']."\DbFunc as DbFunc;\n");
        fputs($this->_file,"use \\".$_SESSION['FDSoil']."\Func as Func;\n");
        fputs($this->_file,"use \\".$this->_dirLog."\\".$this->_tableModel." as ".$this->_tableModel.";\n\n");

        fputs($this->_file,"class SubIndex\n{\n    public function execute(\$aReqs)\n    {\n");
        fputs($this->_file,"        \$obj = new $this->_tableModel();\n");
        fputs($this->_file,"        \$aView['include'] = Func::getFileJSON(__DIR__.\"/js/include.json\");\n");
        fputs($this->_file,"        \$aView['userData'] = Func::usuarioData();\n");
        fputs($this->_file,"        \$aView['load'] = [];\n");
        fputs($this->_file,"        \$xtpl = new XTemplate(__DIR__.\"/view.html\");\n");
        fputs($this->_file,"        Func::appShowId(\$xtpl);\n");

        fputs($this->_file,"        \$aRegist = array_key_exists('".$this->_arr['aTableTablePrimaryKey'][0]."', \$_POST)\n");//?"
        fputs($this->_file,"            ? DbFunc::fetchAssoc(".$this->_tableModel."::".$this->_tableMetodo."Get('REGIST'))\n");
        fputs($this->_file,"                :\FDSoil\DbFunc::iniRegist('".$this->_arr['table']."','".$this->_arr['schema']."');\n");



        /*fputs($this->_file,"        \$aRegist = array_key_exists('".$this->_arr['aTableTablePrimaryKey'][0]."', \$_POST)?");
        fputs($this->_file,"\$obj->extraer_asociativo(\$obj->".$this->_tableMetodo."Get('REGIST'))");
        fputs($this->_file,":\$obj->iniRegist('".$this->_arr['table']."','".$this->_arr['schema']."');\n");*/



        for ($i=0;$i<count($this->_arr['aTableStructure']);$i++){
            fputs($this->_file, "        \$xtpl->assign('".strtoupper($this->_arr['aTableStructure'][$i]['column_name']));
            if ($this->_arr['aTableStructure'][$i]['data_type']=='boolean')
                fputs($this->_file,"_CHK', \$aRegist['".$this->_arr['aTableStructure'][$i]['column_name']."'] === 't' ? 'checked' : '');\n");
            else if ($this->_arr['aTableStructure'][$i]['data_type']=='date')
                fputs($this->_file,"', Func::change_date_format(\$aRegist['".$this->_arr['aTableStructure'][$i]['column_name']."']));\n");
            else if ($this->_arr['aTableStructure'][$i]['data_type']=='time without time zone')
                fputs($this->_file,"', Func::timeMilitarToNormal(\$aRegist['".$this->_arr['aTableStructure'][$i]['column_name']."']));\n");
            else {
                fputs($this->_file,"', \$aRegist['".$this->_arr['aTableStructure'][$i]['column_name']."']);\n");
                for ($j=0;$j<count($this->_arr['aTableUniqueConstraint']);$j++)
                        if ($this->_arr['aTableStructure'][$i]['column_name']==$this->_arr['aTableUniqueConstraint'][$j]['column_name']){
                            fputs($this->_file, "        \$xtpl->assign('".strtoupper($this->_arr['aTableStructure'][$i]['column_name'])
                                ."_READONLY', ");
                            fputs($this->_file, "array_key_exists('".$this->_arr['aTableTablePrimaryKey'][0]."', \$_POST) ? 'readonly' : '');\n");
                        }
            }

        }

        for ($i=0;$i<count($this->_arr['aTableForeignKeysAssoc']);$i++){
            $tableMetodo = self::_strCamel($this->_arr['aTableForeignKeysAssoc'][$i]['foreign_table_name'], 1);
            $FOREIGNNAME = strtoupper($this->_arr['aTableForeignKeysAssoc'][$i]['foreign_table_name']);
            fputs($this->_file, "        \$result = ".$this->_tableModel."::".$tableMetodo."List();\n");
            fputs($this->_file, "        while (\$row = DbFunc::fetchRow(\$result)) {\n");
            fputs($this->_file, "            \$xtpl->assign('ID_".$FOREIGNNAME."', \$row[0]);\n");
            fputs($this->_file, "            \$xtpl->assign('DES_".$FOREIGNNAME."', \$row[1]);\n");
            fputs($this->_file, "            \$xtpl->assign('SELECTED_".$FOREIGNNAME."', (\$aRegist['");
            fputs($this->_file, $this->_arr['aTableForeignKeysAssoc'][$i]['column_name']."'] == ");
            fputs($this->_file, "\$row[0]) ? 'selected' : '');\n            \$xtpl->parse('main.");
            fputs($this->_file, $this->_arr['aTableForeignKeysAssoc'][$i]['foreign_table_name']."');\n        }\n");
        }

        fputs($this->_file,"        Func::btnsPutPanel( \$xtpl, [");
        fputs($this->_file,"[\"btnName\" => \"Return\", \"btnBack\" => \"".$this->_arr['table']."\"],\n");
        fputs($this->_file,"                                    ");
        fputs($this->_file,"[\"btnName\" => \"Save\"  , \"btnClick\"=> \"valEnvio();\"]]);\n");
        fputs($this->_file,"        \$xtpl->parse('main');\n");
        fputs($this->_file,"        \$aView['content'] = \$xtpl->out_var('main');\n");
        fputs($this->_file,"        return \$aView;\n");
        fputs($this->_file,"    }\n}");

        self::_fileBldEnd("/modulos/".$this->_arr['table']."_aux/", "index.php" );

    }

    private function _bldFileViewHtmlAux()
    {

        self::_fileBldStart("/modulos/".$this->_arr['table']."_aux/", "view.html" );

        fputs($this->_file,"<!-- BEGIN: main -->\n");
        fputs($this->_file, "<script src=\"../../../../../{FDSOIL}/js/calendar.js\"></script>\n");
        fputs($this->_file, "<form action=\"\" name=\"form\" method=\"POST\">\n");
        fputs($this->_file, "    <input type=\"hidden\" name=\"".$this->_arr['aTableTablePrimaryKey'][0]."\"");
        fputs($this->_file," id=\"".$this->_arr['aTableTablePrimaryKey'][0]."\" value='{".strtoupper($this->_arr['aTableTablePrimaryKey'][0])."}'>\n");
        fputs($this->_file, "    <div id='sub_titulo'>Registro de ".$this->_arr['table']."</div>\n");
        fputs($this->_file, "    <div align=\"center\">\n");
        fputs($this->_file, "        <table class=\"tabla_grid\" style=\"width: 50%\">\n");
        fputs($this->_file, "            <tr>\n");
        fputs($this->_file, "                <th colspan=\"2\">Datos Básicos</th>\n");
        fputs($this->_file, "            </tr>\n");

        for ($i=0;$i<count($this->_arr['aTableStructure']);$i++){
            if ($this->_arr['aTableStructure'][$i]['column_name']!=$this->_arr['aTableTablePrimaryKey'][0] &&
            !self::_inMatrix($this->_arr['aTableStructure'][$i]['column_name'], $this->_arr['aTableForeignKeysAssoc'] )){
                $label=self::_strLabel($this->_arr['aTableStructure'][$i]['column_name']);
                fputs($this->_file, "            <tr>\n");
                fputs($this->_file, "                <td>".$label."</td>\n");
                fputs($this->_file, "                <td>\n");
                if ($this->_arr['aTableStructure'][$i]['data_type']==='boolean'){
                    fputs($this->_file, "                    <input type=\"checkbox\"\n");
                    fputs($this->_file, "                           id=\"id_".$this->_arr['aTableStructure'][$i]['column_name']."\"\n");
                    fputs($this->_file, "                           name=\"".$this->_arr['aTableStructure'][$i]['column_name']."\"\n");
                    fputs($this->_file, "                           {".strtoupper($this->_arr['aTableStructure'][$i]['column_name'])."_CHK}");
                    fputs($this->_file, ">\n");
                }else if ($this->_arr['aTableStructure'][$i]['data_type']==='text'){
                    fputs($this->_file, "                    <textarea id=\"id_".$this->_arr['aTableStructure'][$i]['column_name']."\"\n");
                    fputs($this->_file, "                              name=\"".$this->_arr['aTableStructure'][$i]['column_name']."\"\n");
                    fputs($this->_file, "                              style=\"width: 98%;\"\n");
                    fputs($this->_file, "                              rows=\"\"\n");
                    fputs($this->_file, "                              cols=\"\"\n");
                    fputs($this->_file, "                              data-format=\"uppercase\"\n");
                    fputs($this->_file, "                              data-constraints=\"textarea\"\n");
                    fputs($this->_file, "                              data-validation=\"textarea\"");
                    fputs($this->_file, ">{".strtoupper($this->_arr['aTableStructure'][$i]['column_name'])."}</textarea>\n");
                }else if ($this->_arr['aTableStructure'][$i]['data_type']==='date'){
                    fputs($this->_file, "                    <input id=\"id_".$this->_arr['aTableStructure'][$i]['column_name']."\"\n");
                    fputs($this->_file, "                           name=\"".$this->_arr['aTableStructure'][$i]['column_name']."\"\n");
                    fputs($this->_file, "                           autocomplete=\"off\"\n");
                    fputs($this->_file, "                           style=\"width: 98%;\"\n");
                    fputs($this->_file, "                           data-validation=\"date\"\n");
                    fputs($this->_file, "                           value=\"{".strtoupper($this->_arr['aTableStructure'][$i]['column_name'])."}\"");
                    fputs($this->_file, " readonly>\n");
                    fputs($this->_file, "                    <script>\n");
                    fputs($this->_file, "                        Calendar.setup({\n");
                    fputs($this->_file, "                            inputField    : \"id_".$this->_arr['aTableStructure'][$i]['column_name']."\",\n");
                    fputs($this->_file, "                            ifFormat      : \"%d/%m/%Y\",\n");
                    fputs($this->_file, "                            button        : \"id_".$this->_arr['aTableStructure'][$i]['column_name']."\",\n");
                    fputs($this->_file, "                            align         : \"Tr\"\n");
                    fputs($this->_file, "                        });\n");
                    fputs($this->_file, "                    </script>\n");
                }else{
                    fputs($this->_file, "                    <input id=\"id_".$this->_arr['aTableStructure'][$i]['column_name']."\"\n");
                    fputs($this->_file, "                           name=\"".$this->_arr['aTableStructure'][$i]['column_name']."\"\n");
                    fputs($this->_file, "                           autocomplete=\"off\"\n");
                    fputs($this->_file, "                           style=\"width: 98%;\"\n");
                    fputs($this->_file, "                           data-format=\"uppercase\"\n");
                    fputs($this->_file, "                           data-constraints=\"\"\n");
                    fputs($this->_file, "                           data-validation=\"required\"\n");
                    fputs($this->_file, "                           value=\"{".strtoupper($this->_arr['aTableStructure'][$i]['column_name'])."}\"");
                    for ($j=0;$j<count($this->_arr['aTableUniqueConstraint']);$j++)
                        if ($this->_arr['aTableStructure'][$i]['column_name']==$this->_arr['aTableUniqueConstraint'][$j]['column_name'])
                            fputs($this->_file, "\n                           {".strtoupper($this->_arr['aTableStructure'][$i]['column_name'])."_READONLY}");
                    fputs($this->_file, ">\n");
                }

                fputs($this->_file, "                </td>\n");
                fputs($this->_file, "            </tr>\n");
            }
        }


        for ($i=0;$i<count($this->_arr['aTableForeignKeysAssoc']);$i++){
            $FOREIGNNAME = strtoupper($this->_arr['aTableForeignKeysAssoc'][$i]['foreign_table_name']);
            $label=self::_strLabel($this->_arr['aTableForeignKeysAssoc'][$i]['foreign_table_name']);
            fputs($this->_file, "            <tr>\n");
            fputs($this->_file, "                <td>".$label."</td>\n");
            fputs($this->_file, "                <td>\n");
            fputs($this->_file, "                    <select id=\"id_".$this->_arr['aTableForeignKeysAssoc'][$i]['foreign_table_name']."\"");
            fputs($this->_file, " name=\"id_".$this->_arr['aTableForeignKeysAssoc'][$i]['foreign_table_name']."\" style=\"width: 100%;\">\n");
            fputs($this->_file, "                        <option value=null selected>Seleccione...</option>\n");
            fputs($this->_file, "                        <!-- BEGIN: ".$this->_arr['aTableForeignKeysAssoc'][$i]['foreign_table_name']." -->\n");
            fputs($this->_file, "                        <option value={ID_".$FOREIGNNAME."} {SELECTED_".$FOREIGNNAME."}>{DES_".$FOREIGNNAME."}</option>\n");
            fputs($this->_file, "                        <!-- END: ".$this->_arr['aTableForeignKeysAssoc'][$i]['foreign_table_name']." -->\n");
            fputs($this->_file, "                    </select>\n");
            fputs($this->_file, "                </td>\n");
            fputs($this->_file, "            </tr>\n");
        }

        fputs($this->_file, "        </table>\n");
        fputs($this->_file, "    </div>\n");
        fputs($this->_file, "    {BUTTONS_PANEL}\n");
        fputs($this->_file, "</form>\n");
        fputs($this->_file, "<!-- END: main -->\n");

        self::_fileBldEnd("/modulos/".$this->_arr['table']."_aux/", "view.html" );

    }

    private function _bldFileSavePhpAux()
    {

        self::_fileBldStart("/modulos/".$this->_arr['table']."_aux/", "save.php" );

        fputs($this->_file,"<?php\n");
        //fputs($this->_file,"use \\".$_SESSION['FDSoil']."\Func as Func;\n");
        fputs($this->_file,"use \\".$this->_dirLog."\\".$this->_tableModel." as ".$this->_tableModel.";\n\n");
        fputs($this->_file,"class SubIndex\n{\n    public function execute(\$aReqs)\n    {\n");
        fputs($this->_file,"        \$obj = new $this->_tableModel();\n");
        fputs($this->_file,"        \$obj->".$this->_tableMetodo."Register();\n    }\n}\n\n");

        self::_fileBldEnd("/modulos/".$this->_arr['table']."_aux/", "save.php" );

    }

    private function _bldFileJsIncludeJsonAux()
    {

        self::_fileBldStart("/modulos/".$this->_arr['table']."_aux/js/", "include.json" );

        fputs($this->_file,"{\"dir\":\"$this->_dirLog\",\n\"modulo\":\"".$this->_arr['table']."_aux\",\n\"arr\":[\n");
        fputs($this->_file,"    {\"dir\":\"FDSoil\", \"typ\":\"js\" , \"arc\":\"relocate\"       },\n");
        fputs($this->_file,"    {\"dir\":\"FDSoil\", \"typ\":\"js\" , \"arc\":\"combo\"          },\n");
        fputs($this->_file,"    {\"dir\":\"FDSoil\", \"typ\":\"js\" , \"arc\":\"validar\"        },\n");
        fputs($this->_file,"    {\"dir\":\"FDSoil\", \"typ\":\"js\" , \"arc\":\"validateData\"   },\n");
        fputs($this->_file,"    {\"dir\":\"FDSoil\", \"typ\":\"js\" , \"arc\":\"formatData\"     },\n");
        fputs($this->_file,"    {\"dir\":\"FDSoil\", \"typ\":\"js\" , \"arc\":\"constraintsData\"},\n");
        fputs($this->_file,"    {\"dir\":\"appOrg\", \"typ\":\"js\" , \"arc\":\"app\"            },\n");
        fputs($this->_file,"    {\"dir\":\"appOrg\", \"typ\":\"js\" , \"arc\":\"validar\"        },\n");
        fputs($this->_file,"    {\"dir\":\"appOrg\", \"typ\":\"css\", \"arc\":\"tpl\"            },\n");
        fputs($this->_file,"    {\"dir\":\"appOrg\", \"typ\":\"css\", \"arc\":\"form\"           },\n");
        fputs($this->_file,"    {\"dir\":\"appOrg\", \"typ\":\"css\", \"arc\":\"calendar\"       },\n");
        fputs($this->_file,"    {\"dir\":\"appOrg\", \"typ\":\"css\", \"arc\":\"validateData\"   },\n");
        fputs($this->_file,"    {\"dir\":\"modulo\", \"typ\":\"js\" , \"arc\":\"validar\"        }\n");
        fputs($this->_file,"]}");

        self::_fileBldEnd("/modulos/".$this->_arr['table']."_aux/js/", "include.json" );

    }

    private function _bldFileJsInicioJsAux()
    {

        self::_fileBldStart("/modulos/".$this->_arr['table']."_aux/js/", "inicio.js" );

        fputs($this->_file,"function inicio()\n{\n    initView();\n");
        fputs($this->_file,"    jQryFormatData(document);\n");
        fputs($this->_file,"    jQryConstraintsData(document);\n");
        fputs($this->_file,"    jQryValidateData(document.getElementById('contenido'));\n}");

        self::_fileBldEnd("/modulos/".$this->_arr['table']."_aux/js/", "inicio.js" );

    }

    private function _bldFileJsValidarJsAux()
    {

        self::_fileBldStart("/modulos/".$this->_arr['table']."_aux/js/", "validar.js" );

        fputs($this->_file,"function valEnvio()\n{\n");
        fputs($this->_file,"    if (validateObjs(document.getElementById('contenido'))){\n");
        fputs($this->_file,"        document.forms[0].action='../".$this->_arr['table']."_aux/save/';\n");
        fputs($this->_file,"        document.forms[0].submit();\n    }\n}");

        self::_fileBldEnd("/modulos/".$this->_arr['table']."_aux/js/", "validar.js" );

    }

    private function _bldFileJsIndexPhpAux()
    {

        self::_fileBldStart("/modulos/".$this->_arr['table']."_aux/js/", "index.php" );

        fputs($this->_file,"<?php\n");
        fputs($this->_file,"session_start();");
        fputs($this->_file,"header(\"Location: ../../../../\".strtolower(\$_SESSION['FDSoil']).\"/admin_session_closed/\");\n\n");

        self::_fileBldEnd("/modulos/".$this->_arr['table']."_aux/js/", "index.php" );

    }

}
