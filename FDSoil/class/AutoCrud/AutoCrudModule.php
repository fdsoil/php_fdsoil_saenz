<?php

trait AutoCrudModule
{
    private function _bldModule()
    {

        self::_dirBld("modulos/".$this->_arr['table']."/");
        self::_bldFileIndexPhp();
        self::_bldFileViewHtml();
        self::_bldFileDeletePhp();
        self::_dirBld("modulos/".$this->_arr['table']."/js");
        self::_bldFileJsIncludeJson();
        self::_bldFileJsInicioJs();
        self::_bldFileJsSubmitJs();
        self::_bldFileJsIndexPhp();

    }

    private function _bldFileIndexPhp()
    {

        self::_fileBldStart("/modulos/".$this->_arr['table']."/", "index.php" );

        fputs($this->_file,"<?php\n");
        fputs($this->_file,"use \\".$_SESSION['FDSoil']."\DbFunc as DbFunc;\n");
        fputs($this->_file,"use \\".$_SESSION['FDSoil']."\Func as Func;\n\n");
        //fputs($this->_file,"use \\".$this->_dirLog."\\".$this->_tableModel." as ".$this->_tableModel.";\n\n");

        fputs($this->_file,"class SubIndex\n{\n    public function execute()\n    {\n");
        fputs($this->_file,"        \\FDSoil\\Audit::validaReferenc();\n");
        

        //fputs($this->_file,"        \$obj = new $this->_tableModel();\n");
        fputs($this->_file,"        \$aView['include'] = Func::getFileJSON(__DIR__.\"/js/include.json\");\n");
        fputs($this->_file,"        \$aView['userData'] = Func::usuarioData();\n");
        fputs($this->_file,"        \$aView['load'] = \$_SESSION['menu'];\n");
        fputs($this->_file,"        \$xtpl = new \\".$_SESSION['FDSoil']."\XTemplate(__DIR__.\"/view.html\");\n");
        fputs($this->_file,"        Func::appShowId(\$xtpl);\n");
        fputs($this->_file,"        Func::btnRecordAdd( \$xtpl ,[\"btnRecordName\"=>\"$this->_tableModel\"]);\n");
        fputs($this->_file,"        \$result = \\".$this->_dirLog."\\".$this->_tableModel."::".$this->_tableMetodo."Get('LIST');\n");
        fputs($this->_file,"        while (\$row = DbFunc::fetchAssoc(\$result)){\n");

        for ($i=0;$i<count($this->_arr['aTableStructure']);$i++){
            if ($this->_arr['aTableStructure'][$i]['column_name']!=$this->_arr['aTableTablePrimaryKey'][0]){
                fputs($this->_file, "            \$xtpl->assign('".strtoupper($this->_arr['aTableStructure'][$i]['column_name']));
                if ($this->_arr['aTableStructure'][$i]['data_type']=='boolean')
                   fputs($this->_file,"', (\$row['".$this->_arr['aTableStructure'][$i]['column_name']."']==='t')?'TRUE':'FALSE');\n");
                else if ($this->_arr['aTableStructure'][$i]['data_type']=='date')
                    fputs($this->_file,"', Func::change_date_format(\$row['".$this->_arr['aTableStructure'][$i]['column_name']."']));\n");
                else if ($this->_arr['aTableStructure'][$i]['data_type']=='time without time zone')
                    fputs($this->_file,"', Func::timeMilitarToNormal(\$row['".$this->_arr['aTableStructure'][$i]['column_name']."']));\n");
                else
                    fputs($this->_file,"', \$row['".$this->_arr['aTableStructure'][$i]['column_name']."']);\n");
            }
        }

        for ($i=0;$i<count($this->_arr['aTableForeignKeysAssoc']);$i++){
            $a['schema']=$this->_arr['aTableForeignKeysAssoc'][$i]['table_schema'];
            $a['table']=$this->_arr['aTableForeignKeysAssoc'][$i]['foreign_table_name'];
            $aForeinKey=self::_getTableStructure($a);
            for ($j=0;$j<count($aForeinKey);$j++){
                if ($aForeinKey[$j]['column_name']!=$this->_arr['aTableForeignKeysAssoc'][$i]['foreign_column_name']){
                    fputs($this->_file, "            \$xtpl->assign('DES_".strtoupper($a['table']));
                    fputs($this->_file,"', \$row['des_".$a['table']."']);\n");
                }
            }
        }

        fputs($this->_file,"            Func::btnRecordEdit( \$xtpl ,[\"btnId\"=>\$row['".$this->_arr['aTableTablePrimaryKey'][0]."']]);\n");
        fputs($this->_file,"            Func::btnRecordDelete( \$xtpl ,[\"btnId\"=>\$row['".$this->_arr['aTableTablePrimaryKey'][0]."']]);\n");
        fputs($this->_file,"            \$xtpl->parse('main.rows');\n        }\n");
        fputs($this->_file,"        Func::btnsPutPanel( \$xtpl ,[[\"btnName\"=>\"Exit\"]]);\n");
        fputs($this->_file,"        \$xtpl->parse('main');\n");
        fputs($this->_file,"        \$aView['content'] = \$xtpl->out_var('main');\n");
        fputs($this->_file,"        return \$aView;\n");
        fputs($this->_file,"    }\n}\n\n");

        self::_fileBldEnd("/modulos/".$this->_arr['table']."/", "index.php" );

    }

    private function _bldFileViewHtml()
    {

        self::_fileBldStart("/modulos/".$this->_arr['table']."/", "view.html" );

        fputs($this->_file,"<!-- BEGIN: main -->\n");
        fputs($this->_file,"<div id=\"sub_titulo\">Registros de $this->_tableModel</div>\n");
        fputs($this->_file,"{BUTTON_ADD}\n");
        fputs($this->_file,"<table id=\"tablaRows\" class=\"display\">\n");
        fputs($this->_file,"    <thead>\n        <tr>\n");

        for ($i=0;$i<count($this->_arr['aTableStructure']);$i++){
            if ($this->_arr['aTableStructure'][$i]['column_name']!=$this->_arr['aTableTablePrimaryKey'][0] &&
            !self::_inMatrix($this->_arr['aTableStructure'][$i]['column_name'], $this->_arr['aTableForeignKeysAssoc'] )){
                fputs($this->_file,"            <th>".self::_strLabel($this->_arr['aTableStructure'][$i]['column_name']."</th>\n"));
            }
        }

        for ($i=0;$i<count($this->_arr['aTableForeignKeysAssoc']);$i++){
            $a['schema']=$this->_arr['aTableForeignKeysAssoc'][$i]['table_schema'];
            $a['table']=$this->_arr['aTableForeignKeysAssoc'][$i]['foreign_table_name'];
            $aForeinKey=self::_getTableStructure($a);
            for ($j=0;$j<count($aForeinKey);$j++)
                if ($aForeinKey[$j]['column_name']!=$this->_arr['aTableForeignKeysAssoc'][$i]['foreign_column_name'])
                    fputs($this->_file,"            <th>".self::_strLabel($a['table'])."</th>\n");
        }

        fputs($this->_file,"            <th>Acción(es)</th>\n");
        fputs($this->_file,"        </tr>\n    </thead>\n    <tbody>\n        <!-- BEGIN: rows -->\n        <tr>\n");

        for ($i=0;$i<count($this->_arr['aTableStructure']);$i++){
            if ($this->_arr['aTableStructure'][$i]['column_name']!=$this->_arr['aTableTablePrimaryKey'][0] &&
            !self::_inMatrix($this->_arr['aTableStructure'][$i]['column_name'], $this->_arr['aTableForeignKeysAssoc'] )){
                fputs($this->_file,"            <td>{".strtoupper($this->_arr['aTableStructure'][$i]['column_name'])."}</td>\n");
            }
        }

        for ($i=0;$i<count($this->_arr['aTableForeignKeysAssoc']);$i++){
            $a['schema']=$this->_arr['aTableForeignKeysAssoc'][$i]['table_schema'];
            $a['table']=$this->_arr['aTableForeignKeysAssoc'][$i]['foreign_table_name'];
            $aForeinKey=self::_getTableStructure($a);
            for ($j=0;$j<count($aForeinKey);$j++)
                if ($aForeinKey[$j]['column_name']!=$this->_arr['aTableForeignKeysAssoc'][$i]['foreign_column_name'])
                    fputs($this->_file,"            <td>{DES_".strtoupper($a['table'])."}</td>\n");
        }

        fputs($this->_file,"            <td align=\"center\">\n");
        fputs($this->_file,"                {BUTTON_EDIT}\n");
        fputs($this->_file,"                {BUTTON_DELETE}\n");
        fputs($this->_file,"            </td>\n");
        fputs($this->_file,"        </tr>\n        <!-- END: rows -->\n    </tbody>\n</table>\n");
        fputs($this->_file,"{BUTTONS_PANEL}\n");
        fputs($this->_file,"<!-- END: main -->\n");

        self::_fileBldEnd("/modulos/".$this->_arr['table']."/", "view.html" );

    }

    private function _bldFileDeletePhp()
    {

        self::_fileBldStart("/modulos/".$this->_arr['table']."/", "delete.php" );

        fputs($this->_file,"<?php\n\n");
        fputs($this->_file,"class SubIndex\n{\n    public function execute() { ");
        fputs($this->_file,"\\".$this->_dirLog."\\".$this->_tableModel."::".$this->_tableMetodo."Delete(); }\n}\n\n");
        self::_fileBldEnd("/modulos/".$this->_arr['table']."/", "delete.php" );

    }

    private function _bldFileJsIncludeJson()
    {

        self::_fileBldStart("/modulos/".$this->_arr['table']."/js/", "include.json" );

        fputs($this->_file,"{\"dir\":\"$this->_dirLog\",\n\"modulo\":\"".$this->_arr['table']."\",\n\"arr\":[\n");
        fputs($this->_file,"    {\"dir\":\"packs\" , \"typ\":\"js\" , \"arc\":\"jquery/jquery.dataTables\"},\n");
        fputs($this->_file,"    {\"dir\":\"FDSoil\", \"typ\":\"js\" , \"arc\":\"menu\"      },\n");
        fputs($this->_file,"    {\"dir\":\"FDSoil\", \"typ\":\"js\" , \"arc\":\"alerts\"    },\n");
        fputs($this->_file,"    {\"dir\":\"FDSoil\", \"typ\":\"js\" , \"arc\":\"relocate\"  },\n");
        fputs($this->_file,"    {\"dir\":\"FDSoil\", \"typ\":\"js\" , \"arc\":\"validar\"   },\n");
        fputs($this->_file,"    {\"dir\":\"FDSoil\", \"typ\":\"js\" , \"arc\":\"tabla\"     },\n");
        fputs($this->_file,"    {\"dir\":\"appOrg\", \"typ\":\"js\" , \"arc\":\"app\"       },\n");
        fputs($this->_file,"    {\"dir\":\"appOrg\", \"typ\":\"js\" , \"arc\":\"validar\"   },\n");
        fputs($this->_file,"    {\"dir\":\"appOrg\", \"typ\":\"css\", \"arc\":\"tpl\"       },\n");
        fputs($this->_file,"    {\"dir\":\"appOrg\", \"typ\":\"css\", \"arc\":\"form\"      },\n");
        fputs($this->_file,"    {\"dir\":\"appOrg\", \"typ\":\"css\", \"arc\":\"menu\"      },\n");
        fputs($this->_file,"    {\"dir\":\"appOrg\", \"typ\":\"css\", \"arc\":\"alerts\"    },\n");
        fputs($this->_file,"    {\"dir\":\"appOrg\", \"typ\":\"css\", \"arc\":\"jqry_table\"},\n");
        fputs($this->_file,"    {\"dir\":\"modulo\", \"typ\":\"js\" , \"arc\":\"submit\"    }\n");
        fputs($this->_file,"]}\n\n");

        self::_fileBldEnd("/modulos/".$this->_arr['table']."/js/", "include.json" );

    }

    private function _bldFileJsInicioJs()
    {

        self::_fileBldStart("/modulos/".$this->_arr['table']."/js/", "inicio.js" );

        fputs($this->_file,"function inicio(oJSON)\n{\n    initView();\n");
        fputs($this->_file,"    menu(oJSON);\n    jQryTableRefresh('tablaRows');\n}\n\n");

        self::_fileBldEnd("/modulos/".$this->_arr['table']."/js/", "inicio.js" );

    }

    private function _bldFileJsSubmitJs()
    {

        self::_fileBldStart("/modulos/".$this->_arr['table']."/js/", "submit.js" );

        fputs($this->_file,"function submitInsert(val)\n{\n");
        fputs($this->_file,"    relocate('../".$this->_arr['table']."_aux/', {});\n}\n\n");
        fputs($this->_file,"function submitEdit(val)\n{\n");
        fputs($this->_file,"    relocate('../".$this->_arr['table']."_aux/', {'".$this->_arr['aTableTablePrimaryKey'][0]."':val});\n}\n\n");
        fputs($this->_file,"function submitDelete(val)\n{\n");
        fputs($this->_file,"    confirm('¿Desea eliminar este registro?', 'Confirmar', function(ok){\n");
        fputs($this->_file,"        if (ok)\n");
        fputs($this->_file,"            relocate('delete/', {'".$this->_arr['aTableTablePrimaryKey'][0]."':val});\n");
        fputs($this->_file,"        });\n}\n\n");

        self::_fileBldEnd("/modulos/".$this->_arr['table']."/js/", "submit.js" );

    }

    private function _bldFileJsIndexPhp()
    {
        self::_fileBldStart("/modulos/".$this->_arr['table']."/js/", "index.php" );
        fputs($this->_file,"<?php\n");
        fputs($this->_file,"session_start();");
        fputs($this->_file,"header(\"Location: ../../../../\".strtolower(\$_SESSION['FDSoil']).\"/admin_session_closed/\");\n\n");
        self::_fileBldEnd("/modulos/".$this->_arr['table']."/js/", "index.php" );
    }


}
