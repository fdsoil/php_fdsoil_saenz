<?php

trait AutoCrudReqs
{

    private function _bldReqs()
    {
        self::_dirBld("reqs/".$this->_arr['table']."/");
        self::_bldFileReqMasterPhp();
        self::_bldFileReqDetailPhp();
    }

    private function _bldFileReqMasterPhp()
    {

        self::_fileBldStart("/reqs/".$this->_arr['table']."/", "index.php");
        fputs($this->_file,"<?php\n");
        //fputs($this->_file,"use \\".$this->_dirLog."\\".$this->_tableModel." as ".$this->_tableModel.";\n");
        //fputs($this->_file,"use \\".$_SESSION['FDSoil']."\Func as Func;\n\n");
        fputs($this->_file,"class SubIndex\n");
        fputs($this->_file,"{\n");
        fputs($this->_file,"    public function execute()\n");
        fputs($this->_file,"    {\n");
        fputs($this->_file,"        \$_POST = \\".$_SESSION['FDSoil']."\Func::base64DecodeArrValKey(\$_POST);\n");
        //fputs($this->_file,"        \$obj = new $this->_tableModel();\n");
        fputs($this->_file,"        echo base64_encode(\\".$this->_dirLog."\\".$this->_tableModel."::".$this->_tableMetodo."Register());\n");
        fputs($this->_file,"    }\n");
        fputs($this->_file,"}\n");
        fputs($this->_file,"SubIndex::execute();\n");

        self::_fileBldEnd("/reqs/".$this->_arr['table']."/", "index.php");

    }

    private function _bldFileReqDetailPhp()
    {
        for ( $i=0 ; $i < count($this->_arr['aTableDetailOfMaster']) ; $i++ ) {

            self::_fileBldStart("/reqs/".$this->_arr['table']."/", $this->_arr['aTableDetailOfMaster'][$i]['table_name'].".php" );

            $tableModel  = self::_strCamel($this->_arr['aTableDetailOfMaster'][$i]['table_name'], 0);
            $tableMetodo = self::_strCamel($this->_arr['aTableDetailOfMaster'][$i]['table_name'], 1);

            fputs($this->_file,"<?php\n");
            fputs($this->_file,"use \\".$this->_dirLog."\\".$this->_tableModel."\\".$tableModel." as ".$tableModel.";\n\n");
            fputs($this->_file,"class SubIndex\n{\n\n");
            fputs($this->_file,"    public function __construct(\$method)\n    {\n");
            fputs($this->_file,"        \$_POST = \\".$_SESSION['FDSoil']."\Func::base64DecodeArrValKey(\$_POST);\n");
            fputs($this->_file,"        self::\$method();\n    }\n\n");
            fputs($this->_file,"    private function ".$tableMetodo."Register() { echo base64_encode($tableModel::".$tableMetodo
                ."Register()); }\n\n");
            fputs($this->_file,"    private function ".$tableMetodo."Get() { echo base64_encode(json_encode($tableModel::".$tableMetodo
                ."Get())); }\n\n");
            fputs($this->_file,"    private function ".$tableMetodo."Delete() { echo base64_encode($tableModel::".$tableMetodo
                ."Delete()); }\n\n}\n\n");
            self::_fileBldEnd("/reqs/".$this->_arr['table']."/", $this->_arr['aTableDetailOfMaster'][$i]['table_name'].".php" );
        }

    }

}
