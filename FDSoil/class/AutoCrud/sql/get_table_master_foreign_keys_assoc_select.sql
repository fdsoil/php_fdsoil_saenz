SELECT  B.column_name,
        C.table_name AS foreign_table_name,
        C.column_name AS foreign_column_name,
        A.constraint_name,
        A.table_schema
FROM information_schema.table_constraints AS A
JOIN information_schema.key_column_usage AS B ON A.constraint_name = B.constraint_name
JOIN information_schema.constraint_column_usage AS C ON C.constraint_name = A.constraint_name
WHERE  constraint_type = 'FOREIGN KEY'
AND A.table_name='{fld:table}'
AND B.table_name='{fld:table}'
AND A.table_schema='{fld:schema}'
AND B.table_schema='{fld:schema}'
AND C.table_schema='{fld:schema}'
AND A.constraint_schema='{fld:schema}'
AND B.constraint_schema='{fld:schema}'
AND C.constraint_schema='{fld:schema}';

/*
SELECT  B.column_name,
        C.table_name AS foreign_table_name,
        C.column_name AS foreign_column_name,
        A.constraint_name,
        C.table_schema
FROM information_schema.table_constraints AS A
JOIN information_schema.key_column_usage AS B ON A.constraint_name = B.constraint_name
JOIN information_schema.constraint_column_usage AS C ON C.constraint_name = A.constraint_name
WHERE  constraint_type = 'FOREIGN KEY'
AND A.table_name='voucher'
AND B.table_name='voucher'
--AND A.table_schema='admini'
--AND B.table_schema='admini'
AND C.table_schema='admini'
--AND A.constraint_schema='admini'
--AND B.constraint_schema='admini'
--AND C.constraint_schema='admini';
*/
