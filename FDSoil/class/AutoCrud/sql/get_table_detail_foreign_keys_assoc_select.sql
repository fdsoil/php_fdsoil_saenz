SELECT  B.column_name,
        C.table_name AS foreign_table_name,
        C.column_name AS foreign_column_name,
        A.constraint_name,
        A.table_schema
FROM information_schema.table_constraints AS A
JOIN information_schema.key_column_usage AS B
ON A.constraint_name = B.constraint_name
JOIN information_schema.constraint_column_usage AS C
ON C.constraint_name = A.constraint_name
WHERE  constraint_type = 'FOREIGN KEY'
AND A.table_name='{fld:table_detail}'
AND B.table_name='{fld:table_detail}'
AND A.table_schema='{fld:schema}'
AND B.table_schema='{fld:schema}'
AND C.table_schema='{fld:schema}'
AND A.constraint_schema='{fld:schema}'
AND B.constraint_schema='{fld:schema}'
AND C.constraint_schema='{fld:schema}'
AND B.column_name NOT IN (SELECT B.column_name
                          FROM information_schema.table_constraints A
                          JOIN information_schema.key_column_usage AS B
                          ON A.constraint_name = B.constraint_name
                          JOIN information_schema.constraint_column_usage C
                          ON A.constraint_catalog = C.constraint_catalog
                          AND A.constraint_schema = C.constraint_schema
                          AND A.constraint_name = C.constraint_name
                          AND C.table_schema IN ('{fld:schema}')
                          AND C.table_name IN ('{fld:table_master}')
                          WHERE lower(A.constraint_type) IN ('foreign key')
                          AND A.table_name='{fld:table_detail}' );
