<?php
use \FDSoil\DbFunc as DbFunc;

trait UsuarioValidar {

    private function validarAutenticacionNivel_0_Crea($aId)
    {
        return DbFunc::resultToString(DbFunc::exeQryFile(self::path()."n0_select.sql", $aId),'|',"%");
    }

    private function validarAutenticacionNivel_1_Crea($aId)
    {
        return DbFunc::resultToString(DbFunc::exeQryFile(self::path()."n1_select.sql", $aId),'|',"%");
    }

    private function validarAutenticacionNivel_2_Crea($aId)
    {
        return DbFunc::resultToString(DbFunc::exeQryFile(self::path()."n2_select.sql", $aId),'|',"%");
    }

    private function validarAutenticacionNivel_3_Crea($aId)
    {
        return DbFunc::resultToString(DbFunc::exeQryFile(self::path()."n3_select.sql", $aId),'|',"%");
    }

    function validarAutenticacion($aId)
    {
        $strArray0=$this->validarAutenticacionNivel_0_Crea($aId);
        $strArray1=$this->validarAutenticacionNivel_1_Crea($aId);
        $strArray2=$this->validarAutenticacionNivel_2_Crea($aId);
        $strArray3=$this->validarAutenticacionNivel_3_Crea($aId);
        $strArray0=substr($strArray0,0,strlen($strArray0));
        $strArray1=substr($strArray1,0,strlen($strArray1));
        $strArray2=substr($strArray2,0,strlen($strArray2));
        $strArray3=substr($strArray3,0,strlen($strArray3));
        $strArray=$strArray0.'$'.$strArray1.'$'.$strArray2.'$'.$strArray3;
	if (strstr($strArray, $_SERVER["REQUEST_URI"]) === false){
		if ($_SESSION['audit']){
			$msj='ALERTA, Están Intentando Hackear el Sistema Entrando por URL...';
                	$this->doAudit($_SERVER["REQUEST_URI"],$msj);
			include_once("../../../".$_SESSION['FDSoil']."/class/email_send.class.php");
			$obj=new emailSend();
			$obj->sendMailAudit($msj);
		}
		header("Location: ../../../".strtolower($_SESSION['FDSoil'])."/admin_session_closed/");
		die();
	}
    }

    function valPswdOld()
    {
        $_POST['id_usuario']=$_SESSION['id_usuario'];
        $row = DbFunc::fetchRow(DbFunc::exeQryFile(self::path()."val_pswd_old_select.sql",$_POST));
        return $row[0];
    }

    function validarAcceso($Post)
    {
       return DbFunc::exeQryFile(self::path()."select_acceso.sql", $Post, false, 'INICIO DE SESION');
    }

    function validarClaveUsuario()
    {
        $aId['id']=$_SESSION['id_usuario'];
	$row=\FDSoil\DbFunc::fetchRow(DbFunc::exeQryFile(self::path()."status_clave_usuario_select.sql", $aId));
	if ($row[0]!='t'){
            $_SESSION['dp']="../../../../".strtolower($_SESSION['FDSoil'])."/admin_usuario_change_pswd_forced/";
            header("Location: ../../../".strtolower($_SESSION['FDSoil'])."/admin_msj/Y/0");
	}
    }

    function validarCedulaId()
    {
        $arr=[];
        $arr['id']= $_SESSION['id_usuario'];
        $arr['ci']= $_POST['ci'];
        $_POST=[];
	$row = DbFunc::fetchRow(DbFunc::exeQryFile(self::path()."validar_cedula_id_select.sql", $arr));
	return $row[0];
    }

    function validarEmail()
    {
        $arr=[];
        $arr['id']= $_SESSION['id_usuario'];
        $arr['email']= $_POST['email'];
        $_POST=[];
	$row = DbFunc::fetchRow(DbFunc::exeQryFile(self::path()."validar_email_select.sql", $arr));
	return $row[0];
    }

    function validarCelular()
    {
        $arr=[];
        $arr['id']= $_SESSION['id_usuario'];
        $arr['cel']= $_POST['cel'];
        $_POST=[];
	$row = DbFunc::fetchRow(DbFunc::exeQryFile(self::path()."validar_celular_select.sql", $arr));
	return $row[0];
    }

    function validarRespSeguridad()
    {
	$row = DbFunc::fetchRow(DbFunc::exeQryFile(self::path()."validar_resp_seguridad_select.sql", $_GET));
	return $row[0];
    }

}

