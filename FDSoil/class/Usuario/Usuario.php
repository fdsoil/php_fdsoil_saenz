<?php
namespace FDSoil;

require_once("UsuarioConsultar.php");
require_once("UsuarioRegistrar.php");
require_once("UsuarioValidar.php");

use \FDSoil\Func as Func;

class Usuario
{
    /** Usuario
    * @author Ernesto Jiménez <fdsoil123@gmail.com>
   **/
    use \UsuarioConsultar, \UsuarioRegistrar, \UsuarioValidar;

    /** Arreglo asociativo que contiene campos y su respectiva característica.
    * dichos campos son los únicos valores permitidos por $_REQUEST, $_POST y $_GET
    * para esta clase*/
    static private $aUserValReqs = array(
        "id" => array(
            "label"       => "Id",
            "required"   => false,
            "constrain"  => "number"
        ),
        "cedula" => array(
            "label"       => "N° de C.I.",
            "required"   => true,
            "constrain"  => "ced",
            "format"     => "uppercase",
            "length-max" => 15
        ),
       "nombre" => array(
            "label"       => "Nombre(s)",
            "required"   => true,
            "constrain"  => "fullname",
            "format"     => "uppercase",
            "length-max" => 100
        ),
        "apellido" => array(
            "label"       => "Apellido(s)",
            "required"   => true,
            "constrain"  => "fullname",
            "format"     => "uppercase",
            "length-max" => 100
        ),
        "usuario" => array(
            "label"       => "Nombre de Usuario",
            "required"   => true,
            "constrain"  => "username",
            "format"     => "lowercase",
            "length-max" => 20
        ),
        "clave" => array(
            "length-max" => 100
        ),
        "clave_simple" => array(
            "label"       => "Clave de acceso",
            "required"   => true,
            "constrain"  => "password",
            "length-max" => 100
        ),
        "correo" => array(
            "label"       => "Correo Electrónico",
            "required"   => true,
            "constrain"  => "email",
            "format"     => "lowercase",
            "length-max" => 100
        ),
        "celular" => array(
            "label"           => "Telefono Celular",
            "required"       => true ,
            "constrain"      => "number",
            "length"         => "11"
        ),
        "telefono1" => array(
            "label"       => "Telefono Local",
            "required"   => true,
            "constrain"  => "number",
            "length-max" => "20"
        ),
        "telefono2" => array(
            "label"       => "Otro Telefono",
            "required"   => false ,
            "constrain"  => "number",
            "length-max" => "20"
        ),
        "pregunta_seguridad" => array(
            "label"      => "Pregunta de Seguridad",
            "required"   => false,
            "constrain"  => "textarea",
            "format"     => "uppercase",
            "length-max" => "100"
        ),
        "respuesta_seguridad" => array(
            "label"      => "Respuesta de Seguridad",
            "required"   => false,
            "constrain"  => "textarea",
            "format"     => "uppercase",
            "length-max" => "100"
        ),
        "id_rol" => array(
            "required"   => true,
            "constrain"  => "number"
        ),
        "id_status" => array(
            "required"   => true,
            "constrain"  => "number"
        )
    );

    /** Ruta donde deben colocarse los archivos planos que contienen los query.
    * Descripción: Devuelve la ruta donde deben colocarse los archivos planos
    * que contienen los query o sentencia(s) de SQL.
    * No recibe parametros.
    * @return string Devuelve ruta donde deben colocarse los query.*/
    private function path() { return "../../../".$_SESSION['FDSoil']."/class/Usuario/sql/usuario/"; }

    public function usuarioValReqs( $aReq) { return Func::valReqs( $aReq, self::$aUserValReqs); }

    public function usuarioFormatReqs( $aReq) { return Func::formatReqs( $aReq, self::$aUserValReqs); }

    function tipoLogin()
    {
        if ($_POST['opcion'] == 1){
            $array['usuario'] = $_POST['rif1'].$_POST['rif2'].$_POST['rif3'];
            $array['campo'] = 'rif';
        }
        else if ($_POST['opcion'] == 2){
            $array['usuario'] = $_POST['nacionalidad'].$_POST['cedula'];
            $array['campo'] = 'cedula';
        }
        else if ($_POST['opcion'] == 3){
            $array['usuario'] = $_POST['correo'];
            $array['campo'] = 'correo';
        }
        else if ($_POST['opcion'] == 4){
            $array['usuario'] = $_POST['user'];
            $array['campo'] = 'usuario';
        }
        return $array;
    }

    function inicioSession($row)
    {
        $_SESSION['usuario']=$row['usuario'];
        $_SESSION['id_rol']=$row['id_rol'];
        $_SESSION['nombre_apellido']=$row['nombre'].', '.$row['apellido'];
        $_SESSION['id_usuario']=$row['id_usuario'];
        $_SESSION['cedula']=$row['cedula'];
	$aId['id']=$_SESSION['id_rol'];
	$_SESSION['menu_user']=\FDSoil\Usuario\Rol\Menu::menuMake($aId);
	$_SESSION['menu']=$_SESSION['menu_user'];
	$_SESSION['pag_ini_default']=str_replace("FDSoil",strtolower($_SESSION['FDSoil']),$row['pag_ini_default']);
	$_SESSION['pag_ini_default']=str_replace("appOrg",strtolower($_SESSION['appOrg']),$_SESSION['pag_ini_default']);
	$_SESSION['pag_ini_default']=str_replace("myApp",strtolower($_SESSION['myApp']),$_SESSION['pag_ini_default']);
    }

    function usuarioClaveUltimoCambio($Post)
    {
        $fecha_inicio = $Post['fecha_key'];
        $fecha_fin = $Post['fecha_actual'];
        $datetime1 = new \DateTime($fecha_inicio);
        $datetime2 = new \DateTime($fecha_fin);
        # obtenemos la diferencia entre las dos fechas
        $interval = $datetime2->diff($datetime1);
        # obtenemos la diferencia en meses
        $intervalMeses = $interval->format("%m");
        # obtenemos la diferencia en años y la multiplicamos por 12 para tener los meses
        $intervalAnos = $interval->format("%y") * 12;
        #meses de diferencia
        $diferencia = $intervalMeses + $intervalAnos;
        return $diferencia;
    }

}
