SELECT 
A.id AS id_usuario, A.usuario, A.clave, A.nombre, A.apellido, 
A.id_rol, A.id_status, A.cedula, A.fecha_key, current_date as fecha_actual,
B.pag_ini_default
FROM seguridad.usuario A
INNER JOIN seguridad.roles B ON A.id_rol = B.id 
WHERE A.id_status = 1 AND A.{fld:campo}='{fld:usuario}' AND A.clave='{fld:clave}';


