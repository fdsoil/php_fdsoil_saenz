<?php
use \FDSoil\Func as Func;
use \FDSoil\DbFunc as DbFunc;

trait UsuarioRegistrar {

    static private $_post = [];

    private function _POST_To_post()
    {
        if ( array_key_exists("usuario",$_POST) )
            self::$_post['usuario'] = $_POST['usuario'];
        if ( array_key_exists("nombres",$_POST) )
            self::$_post['nombre'] = $_POST['nombres'];
        if ( array_key_exists("apellidos",$_POST) )
            self::$_post['apellido'] = $_POST['apellidos'];
        if ( array_key_exists("correo",$_POST) )
            self::$_post['correo'] = $_POST['correo'];
        if ( array_key_exists("pregunta_seguridad",$_POST) )
            self::$_post['pregunta_seguridad'] = $_POST['pregunta_seguridad'];
        if ( array_key_exists("respuesta_seguridad",$_POST) )
            self::$_post['respuesta_seguridad'] = $_POST['respuesta_seguridad'];
        if ( array_key_exists("rol_usuario",$_POST) )
            self::$_post['id_rol'] =  $_POST['rol_usuario'];
        if ( array_key_exists("status",$_POST) )
            self::$_post['id_status'] = $_POST['status'];
        if ( array_key_exists("nacionalidad",$_POST) && array_key_exists("cedula",$_POST) ) {
            self::$_post['cedula'] = $_POST['nacionalidad'] . $_POST['cedula'];
            unset($_POST['nacionalidad']);
        }  
        if ( array_key_exists("celular_cod",$_POST) && array_key_exists("celular",$_POST) ) {
            self::$_post['celular'] = $_POST['celular_cod'].$_POST['celular'];
            unset($_POST['celular_cod']);
        }
        if ( array_key_exists("local_cod",$_POST) && array_key_exists("telefono1",$_POST) ) {
            self::$_post['telefono1'] = $_POST['local_cod'].$_POST['telefono1'];
            unset($_POST['local_cod']);
        }
        if ( array_key_exists("local_cod2",$_POST) && array_key_exists("telefono2",$_POST) ) {
            self::$_post['telefono2'] = $_POST['local_cod2'].$_POST['telefono2'];
            unset($_POST['local_cod2']);
        }
        $_POST = [];
    }

    function registrarUsuarioWeb($pag="acceso")
    {

        self::$_post['clave'] = md5($_POST['clave']);
        self::$_post['clave_simple'] = $_POST['clave'];
        self::_POST_To_post();
        $_POST=[];
        $strFile = "../../../".$_SESSION['myApp']."/config/app.json";
        file_exists($strFile) ? $oJSON = json_decode(file_get_contents($strFile)) : die("File nor Found " . $strFile);
        self::$_post['id_rol'] = $oJSON->rol->id_rol;
        self::$_post['id_status'] = $oJSON->rol->id_status;
        //self::$_post=$this->validateInput(self::$_post);
        $aValReqs = self::usuarioValReqs(self::$_post);
        if (!$aValReqs){
            self::$_post = self::usuarioFormatReqs(self::$_post);
            $row = DbFunc::fetchRow(DbFunc::exeQryFile(self::path()."row_web.sql", self::$_post, true, 'REGISTRO USUARIO WEB'));
            if ($row[0] == 'C') {
                include_once("../../../".$_SESSION['FDSoil']."/packs/email/send_email.php");
                $nombre = self::$_post['nombre'].' '.self::$_post['apellido'];
                include_once("../../../".$_SESSION['myApp']."/config/usuario.php");
                switch ($typeOfLogin) {//JJJJSSSSSOOOONNNNN AAAARRRREEEGGGGLLLLAAAARRRR
                    case 2:
                        $tipoDeLogin=self::$_post['cedula'];
                        break;
                    case 3:
                        $tipoDeLogin=self::$_post['correo'];
                        break;
                    default:
                        $tipoDeLogin=self::$_post['usuario'];
                        break;
                }
                $usuario=$tipoDeLogin;
                $clave_simple = self::$_post['clave_simple'];
                $email_destino = self::$_post['correo'];
                //correo_enviar($nombre,$usuario,$clave_simple,$email_destino,'msj_registro');
            }
            $msj=$row[0];
        }else{
            $_SESSION['messages'] = $aValReqs;
            $msj = 'N';
        }
        $_SESSION['dp']="../../../../".strtolower($_SESSION['FDSoil'])."/admin_$pag/";
        Func::adminMsj($msj,0);
    }

    function registrarUsuario()
    {

        self::$_post['id'] = $_POST['id_usuario_reg'];
        self::$_post['clave'] = ($_POST['cedula']!='')?md5($_POST['cedula']):'';
        self::_POST_To_post();
        $aValReqs = self::usuarioValReqs(self::$_post);
        if (!$aValReqs){
            self::$_post = self::usuarioFormatReqs(self::$_post);
            self::$_post['id_user_action'] = $_SESSION['id_usuario'];
            $row = DbFunc::fetchRow(DbFunc::exeQryFile(self::path()."row.sql", self::$_post, true, 'REGISTRO USUARIO'));
            $msj = $row[0];
        }else{
            $_SESSION['messages'] = $aValReqs;
            $msj = 'N';
        }
        Func::adminMsj($msj,2);
    }

    function registrarMisDatos()
    {
        self::$_post['id'] = $_POST['id_usuario'];
        self::_POST_To_post();
        $aValReqs = self::usuarioValReqs(self::$_post);
        if (!$aValReqs){
            self::$_post = self::usuarioFormatReqs(self::$_post);
            $msj  = (DbFunc::exeQryFile(self::path()."mis_datos_update.sql", self::$_post, true, 'ACTUALIZAR MIS DATOS'))?'A':'Z';
        }else{
            $_SESSION['messages'] = $aValReqs;
            $msj = 'N';
        }
        $_SESSION['dp']="../../../".strtolower($_SESSION['FDSoil'])."/admin_inicio/";
        Func::adminMsj($msj,0);
    }

    function usuarioResetKey($recuperacion = "Recuperacion")
    {
        if ($recuperacion == "Reseteo") {
            $resp = 'Z';
            $nueva_clave = $this->randomString(8,true,true,true);
            /*$nueva_clave=$_POST['ced'];
            $nueva_clave=str_replace("V", "", $nueva_clave);
            $nueva_clave=str_replace("E", "", $nueva_clave);*/
            $_POST['clave'] = md5($nueva_clave);
            if ($this->usuarioChangePswdReset($_POST)) {
                include_once("../../../".$_SESSION['FDSoil']."/class/email_send.class.php");
                $strFileName = "../../../".$_SESSION['myApp']."/config/app.json";
                file_exists($strFileName) ? $oJSON = json_decode(file_get_contents($strFileName)) : die("File nor Found " . $strFileName);
                $arrEmail = Func::pasPropOfObjToArr( $oJSON, array('email_usuario'));
                $row = DbFunc::fetchAssoc(DbFunc::exeQryFile(self::path()."row_select.sql", $_POST));
                $nombre = $row['nombre'].', '.$row['apellido'];
                $usuario = $row['usuario'];
                $arrEmail['msj'] = "Notificacion de cambio de clave para el usuario : <b>"
                    . $usuario . "</b> <br>Perteneciente a <b>: ". $nombre 
                    . "</b> <br>Indicando que su nueva clave es : <b>". $nueva_clave . "</b>";
                $arrEmail['addAddress'] = $row['correo'];
                $arrEmail['subject'] = "Reseteo de Clave";
                $obj = new emailSend();
                $obj->sendMail($arrEmail);
                $resp='E';
            }
        } else {
            $_POST["id"] = $recuperacion["id"];
            $_POST["clave"] = $recuperacion["clave"];
            if ($this->usuarioChangePswdReset($_POST)) {
                include_once("../../../".$_SESSION['FDSoil']."/class/email_send.class.php");
                $strFileName = "../../../".$_SESSION['myApp']."/config/app.json";
                file_exists($strFileName) ? $oJSON = json_decode(file_get_contents($strFileName)) : die("File nor Found " . $strFileName);
                $arrEmail = Func::pasPropOfObjToArr( $oJSON, array('email_usuario'));
                $arrEmail['msj'] = "Recuperacion de clave para el usuario : <b>". $recuperacion['usuario'] 
                    . "</b> <br>Perteneciente a <b>: ". $recuperacion['nombre'] . "</b> <br>Indicando que su nueva clave es : <b>"
                    . $recuperacion['clave_correo'] . "</b>";
                $arrEmail['addAddress'] = $recuperacion['correo'];
                $arrEmail['subject'] = "Reseteo de Clave";
                $obj = new emailSend();
                $obj->sendMail($arrEmail);
                $resp = 'D';
            }
        }

        return $resp;
    }

    function usuarioChangePswdForced()
    {
        $_POST['usuario']= $_SESSION['id_usuario'];
        $_POST['setPregunta']=($_POST['pregunta_seguridad2']==="0")?
        '':Func::replace_data($_POST, " pregunta_seguridad = '{fld:pregunta_seguridad2}', ");
        $_POST['setRespuesta']=($_POST['respuesta_seguridad2']==="")?
        '':Func::replace_data($_POST, " respuesta_seguridad = '{fld:respuesta_seguridad2}', ");
        unset($_POST['pregunta_seguridad2']);
        unset($_POST['respuesta_seguridad2']);
        return (DbFunc::exeQryFile(self::path()."change_pswd_update_forced.sql", $_POST,true, 'CAMBIO DE CLAVE FORZADO'))?'F':'Z';
    }

    function usuarioChangePswdReset()
    {
         return DbFunc::exeQryFile(self::path()."change_pswd_update_reset.sql", $_POST, true, 'CAMBIO DE CLAVE RESETEADO');
    }

    function usuarioChangePswd()
    {
         return (DbFunc::exeQryFile(self::path()."change_pswd_update.sql", $_POST,true,'CAMBIO DE CLAVE'))?'A':'Z';
    }

    function recoverPassWord($Post)
    {
        return DbFunc::exeQryFile(self::path()."recover_pass_word.sql", $Post, true, 'RECUPERAR CLAVE');
    }

}

