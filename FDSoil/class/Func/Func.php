<?php
namespace FDSoil;

require_once(__DIR__.'/../../packs/CacheLite/Cache.php');
require_once("FuncCache.php");
require_once("FuncOnSet.php");
require_once("FuncArr.php");
require_once("FuncReqs.php");
require_once("FuncXtpl.php");

class Func
{

    use \FuncCache, \FuncOnSet, \FuncArr, \FuncReqs, \FuncXtpl;

    /** Mensaje Administrativo.
    * Descripción: Envia un mensaje administrativo (información) al usuario dinámicamente.
    * Consiste en enviar la aplicación hacia una página para tal fin indicando los parámetros.
    * @param string $msj Mensaje que se le enviará al usuario.
    * @param integer $np Número de páginas que retrocedera despues de ser enviado el mensaje.*/
    public function adminMsj($msj='' , $np=0)
    {
        header("Location: ../../../../".strtolower($_SESSION['FDSoil'])."/admin_msj/$msj/$np");
    }

    /** Construye y llena una tabla a partir de una matriz.
    * Descripción: Construir y llenar una tabla HTML (table) a partir de una matriz (arreglo bidimencional)
    * @param array $rows Matriz (arreglo bidimencional)
    * @param string $id Identificador del elemento (table)
    * @return element Elemento tabla HTML (table).*/
    function bldTable($rows,$id)
    {
        $th="";
        $tr="";
        if ($id!=null)
            $idid="id='".$id."'";
        for ($i=0;$i<count($rows);$i++){
            $tr.='<tr>';
            foreach ($rows[$i] as $key => $value){
                if ($i==0) $th.='<th>'.utf8_decode($key).'</th>';
                $tr.='<td>'.utf8_decode($value).'</td>';
            }
            $tr.='</tr>';
        }
        return "<table ".$idid.">".$th.$tr."</table>";
    }

    /** Construye y llena una tabla maquillada a partir de una matriz.
    * Descripción: Construir y llenar una tabla HTML (table) maquillada a partir de una matriz (arreglo bidimencional)
    * @param array $rows Matriz (arreglo bidimencional)
    * @param string $id Identificador del elemento (table)
    * @return element Elemento tabla HTML (table) llenada y maquillada.*/
    function bldTableCss($rows,$id)
    {
        $th="";
        $tr="";
        $idid="";
        if ($id!=null)
            $idid="id='".$id."'";
        $class_tr="losnone";
        for ($i=0;$i<count($rows);$i++){
            $tr.="<tr class=".$class_tr.">";
            foreach ($rows[$i] as $key => $value){
                if ($i==0) $th.='<th>'.utf8_decode($key).'</th>';
                $tr.='<td>'.utf8_decode($value).'</td>';
            }
            $tr.='</tr>';
            $class_tr=($class_tr=="losnone")?"lospare":"losnone";
        }
        return "<table ".$idid." class='tabla_grid'>".$th.$tr."</table>";
    }

    /** Instancia Clases.
    * Descripción: Instanciar Clases
    * @param string $class_to_call La clase a ser instanciada
    @return object Una instancia de la clase*/
    function callClass($class_to_call)
    {
        return new $class_to_call();
    }
    /** Cambia el formato de una fecha.
    * Descripción: Cambia el formato de una fecha. Si esta en formato AAAA-MM-DD se cambia a DD/MM/AAAA y viceversa
    *
    * @param string $fecha Cadena con formato de fecha
    * @return string $string Cadena con formato de fecha cambiada.*/
    public function change_date_format($fecha)
    {
        if (substr_count($fecha, '-') > 0) {//si esta en formato AAAA-MM-DD se cambia a DD/MM/AAAA
            $f = explode("-", $fecha);
            $cs = "/"; //$cs="/";
        } else if (substr_count($fecha, '/') > 0) { //si esta en formato DD/MM/AAAA se cambia a AAAA-MM-DD
            $f = explode("/", $fecha);
            $cs = "-";
        }
        return $f[2] . $cs . $f[1] . $cs . $f[0];
    }

    /** Obtener archivo JSON.
    * Descripción: Obtener el contenido de una archivo JSON especificado.
    * @return string La IP remota del cliente*/
    function getFileJSON($file)
    {
        if (!file_exists($file))
            die("File nor Found ".$file);
        else
            return json_encode(file_get_contents($file));
    }
     /** Obtener archivo.
    * Descripción: Obtener el contenido de una archivo especificado.
    * @return string La IP remota del cliente*/
    function filGetContents($file)
    {
        if (!file_exists($file))
            die("File nor Found ".$file);
        else
            return file_get_contents($file);
    }
     /** Obtener la IP real.
    * Descripción: Obtener la IP remota del cliente.
    * @return string La IP remota del cliente*/
    function getRealIP()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP']))
            return $_SERVER['HTTP_CLIENT_IP'];
        if (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
            return $_SERVER['HTTP_X_FORWARDED_FOR'];
        return $_SERVER['REMOTE_ADDR'];
    }

    function oneMonthAgo() 
    {
        //$fechaActual = date('Y-m-j');
        //$fechaMesPasado = strtotime ('-1 month', strtotime($fechaActual));
        //$fechaMesPasadoDate = date('Y-m-j', $fechaMesPasado);
        return date('Y-m-d', strtotime ('-1 month', strtotime(date('Y-m-d'))));         
    }

    /** Elimina caracteres especiales de una cadena de texto.
    * Descripción: Eliminar caracteres especiales de una cadena de texto.
    * @param string $cadena Cadena de texto
    * @return string Devuelve cadena de texto sin caracteres especiales.*/
    public function quita_caracteres_especiales($cadena)
    {
        $cadena = str_replace("'", "", $cadena);
        $cadena = str_replace("_", "", $cadena);
        $cadena = str_replace("@", "", $cadena);
        $cadena = str_replace("\"", "", $cadena);
        return $cadena;
    }

    /** Elimina el formato numérico de un valor dado.
    * Descripción: Eliminar el formato numérico de un valor dado.
    * @param string $num Número con formato
    * @return float Devuelve el número sin formato.*/
    public function quita_formato_numerico($num)
    {
        return $num = str_replace(",", ".", str_replace(".", "", $num));
    }

    /** Crea una cadena de caracteres aleatoria.
    * Descripción: Crear una cadena de caracteres aleatoria (random).
    * @param integer $length Representa el largo de la cadena de caracteres resultado; su valor por defecto es 8
    * @param boolean $uc Indica que la cadena de caracteres resultado tenga letras mayúsculas; su valor por defecto es TRUE
    * @param boolean $n Indica que la cadena de caracteres resultado tenga números; su valor por defecto es TRUE
    * @param boolean $sc Indica que la cadena de caracteres resultado tenga caracteres especiales; su valor por defecto es FALSE
    * @return string $rstr Devuelve una cadena de caracteres aleatoria (random).*/
    function randomString($length=8,$uc=true,$n=true,$sc=false) //echo RandomString(15,TRUE,TRUE,TRUE);
    {
        $source = 'abcdefghijklmnopqrstuvwxyz';
        if($uc==1)
            $source .= 'ABCDEFGHIJKLMNPQRSTUVWXYZ';//'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        if($n==1)
            $source .= '123456789';//'1234567890' Eliminé 0, O y o para evitarle problemas al usuario por la fuente.
        if($sc==1)
            $source .= '|@$%!()';// OJO: Porque dan error así Â, no incluir aquí estos dos caracteres... ¿ ¬
        if($length>0){
            $rstr = "";
            $source = str_split($source,1);
            for($i=1; $i<=$length; $i++){
                mt_srand((double)microtime() * 1000000);
                $num = mt_rand(1,count($source));
                //$rstr.=($this->valKey($source[$num-1])==1)?$source[$num-1]:'///';
                $rstr .= $source[$num-1];
            }
        }
        return $rstr;
    }

    /** Lee un archivo de texto.
    * Descripción: Leer un archivo de texto.
    * @param string $file Concatenación de la ruta donde está el archivo de texto y su respectivo nombre
    * @return string Devuelve el contenido del archivo en cadena de texto.*/
    public function read_txt($file)
    {
        $texto = "";
        $linea = "";
        $fp = fopen($file, "r");
        while (!feof($fp)) {
            $linea = fgets($fp);
            $texto .= $linea;
        }
        fclose($fp);
        return $texto;
    }

    /** Reemplaza dentro de una cadena de texto las etiquetas especificadas por los valores suministrados en un arreglo.
    * Descripción: Reemplazar dentro de una cadena de texto las etiquetas especificadas por los valores suministrados en un arreglo.
    * @param array $request Arreglo que contiene los valores para remplazar
    * @param string $string Cadena de texto con las etiquetas a ser remplazadas
    * @return string $string Devuelve cadena de texto con valores reemplazados.*/
    public function replace_data($request, $string)
    {
        if (!empty($string) && !empty($request)) {
            foreach ($request as $campo => $valor) {
                $fields = "{fld:" . $campo . "}";
                $string = str_replace($fields, $valor, $string);
            }
        }
        return $string;
    }

    /** Elimina los acentos de una cadena de texto.
    * Descripción: Eliminar los acentos de una cadena de texto.
    * @param string $String Cadena de texto
    * @return string Devuelve cadena de texto sin acentos.*/
    public function strip_accents($String)
        {
            $String = ereg_replace("[äáàâãª]", "a", $String);
            $String = ereg_replace("[ÁÀÂÃÄ]", "A", $String);
            $String = ereg_replace("[ÍÌÎÏ]", "I", $String);
            $String = ereg_replace("[íìîï]", "i", $String);
            $String = ereg_replace("[éèêë]", "e", $String);
            $String = ereg_replace("[ÉÈÊË]", "E", $String);
            $String = ereg_replace("[óòôõöº]", "o", $String);
            $String = ereg_replace("[ÓÒÔÕÖ]", "O", $String);
            $String = ereg_replace("[úùûü]", "u", $String);
            $String = ereg_replace("[ÚÙÛÜ]", "U", $String);
            $String = str_replace("^", "", $String);
            $String = str_replace("´", "", $String);
            $String = str_replace("`", "", $String);
            $String = str_replace("¨", "", $String);
            $String = str_replace('\\', "", $String);
            $String = str_replace('\'', "", $String);
            $String = str_replace("~", "", $String);
            $String = str_replace("ç", "c", $String);
            $String = str_replace("Ç", "C", $String);
            $String = str_replace("ñ", "n", $String);
            $String = str_replace("Ñ", "N", $String);
            $String = str_replace("Ý", "Y", $String);
            $String = str_replace("ý", "y", $String);
            return $String;
        }


        /** Poner entre comillas a los valores de cadena tipo arreglo
        * Descripción: Encerrar entre comillas a los valores de una cadena tipo Arreglo
        *
        * @param string Cadena de caracteres tipo Arreglo
        * @return string Cadena de caracteres tipo Arreglo con valores entrecomillados.*/
        function strArrComillasPoner($strArr)
        {
            $arr = explode(',', $strArr);
            $str_arr = "";
	    for ( $i = 0; $i < count($arr); $i++ )
	        $str_arr.= "'" . trim($arr[$i]) . "',";
	    return substr($str_arr, 0,strlen($str_arr) -1);
        }

        /** Tiempo Militar a Normal.
        * Descripción: Convertir el tiempo (hora) de formato militar a normal (cotidiano).
        *
        * Ejemplo: De '17:33:45' a '05:33:PM'
        * @param string $hora Tiempo (hora) en formato militar
        * @return string  $hora Devuelve el tiempo (hora) en formato normal o cotidiano.*/
        function timeMilitarToNormal($hora)
        {
            if ($hora == null)
                return '';
            $arr = explode(":", $hora);
            if ( $arr[0] > 12 || $arr[0] == '00' ) {
                if ( $arr[0] > 12 )
                    $arr[0] -= 12;
                else if ( $arr[0] == '00' )
                    $arr[0] = 12;
                $arr[2] = 'PM';
            }
            else
                $arr[2] = 'AM';
            $hora=str_pad($arr[0], 2, "0", STR_PAD_LEFT) . ':' . str_pad($arr[1], 2, "0", STR_PAD_LEFT) . ':' . $arr[2];
            return $hora;
        }

        /** Tiempo Normal a Militar.
        * Descripción: Convertir el tiempo (hora) de formato normal (cotidiano) a militar.
        *
        * Ejemplo: De '05:33:PM' a '17:33:00'
        * @param string $hora Tiempo (hora) en formato normal (cotidiano)
        * @return string  $hora Devuelve el tiempo (hora) en formato militar.*/
        function timeNormalToMilitar($hora)
        {
            $meridian = substr($hora, 6, 1);
            $hora = str_replace('AM', '00', $hora);
            $hora = str_replace('PM', '00', $hora);
            if ( $meridian == 'P' ) {
                $arr = explode(":", $hora);
                $arr[0] += 12;
                if ( $arr[0] == 24 )
                    $arr[0] = '00';
                $hora = str_pad($arr[0], 2, "0", STR_PAD_LEFT) . ':' . str_pad($arr[1], 2, "0", STR_PAD_LEFT) . ':' . $arr[2];
            }
            return $hora;
        }

        /** Reemplaza los acentos y convierte en mayúscula una cadena.
        * Descripción: Reemplazar los acentos y convertir en mayúscula una cadena de caracteres.
        * @param string $cadena Cadena de caracteres
        * @return string Cadena de caracteres sin acentos y en mayúscula.*/
        function unaccentedUppercase($cadena)
        {
            $respuesta = $cadena;
            $respuesta = str_replace('á','A', $respuesta);
            $respuesta = str_replace('é','E', $respuesta);
            $respuesta = str_replace('í','I', $respuesta);
            $respuesta = str_replace('ó','O', $respuesta);
            $respuesta = str_replace('ú','U', $respuesta);
            $respuesta = str_replace('Á','A', $respuesta);
            $respuesta = str_replace('É','E', $respuesta);
            $respuesta = str_replace('Í','I', $respuesta);
            $respuesta = str_replace('Ó','O', $respuesta);
            $respuesta = str_replace('Ú','U', $respuesta);
            return  strtoupper($respuesta);
        }

        /** Valida la sesión.
        * Descripción: Validar que un usuario haya activado la sesión.
        *
        * Nota: Si no hay un usuario activo en sesión, automaticamente envia a la página de cierre de sesión.
        * @return null Retorna null si hay un usuario activo en sesión, de lo contrario, envia a la página de cierre de sesión.*/
        function validar_session()
        {
            if (empty($_SESSION['id_usuario'])) {
                header("Location: ../../../".$_SESSION['FDSoil']."/".$_SESSION['modulos']."/admin_session_closed/");
                die();
            } else {
                return;
            }
        }

        /** Voltea el formato de una fecha.
        * Descripción: Voltear el formato de una fecha; de DD/MM/AAAA cambia a AAAA-MM-DD.
        *
        * @param string $fecha Cadena con formato de fecha
        * @return string $string Cadena con formato de fecha cambiada.*/
        function voltearFecha($fecha)
        {
            return implode("/", array_reverse(explode("-", $fecha)));
        }

        /** Fecha de hoy.
        * Descripción: Fecha del día de hoy en formato AAAA-MM-DD.
        *
        * No recibe parametros
        * @return string Devuelve la fecha del día de hoy en formato AAAA-MM-DD.*/
        function dateOfToday()
        {
            $arr = getdate();
                /*[seconds] => 40
                [minutes] => 58
                [hours]   => 21
                [mday]    => 17
                [wday]    => 2
                [mon]     => 6
                [year]    => 2003
                [yday]    => 167
                [weekday] => Tuesday
                [month]   => June
                [0]       => 1055901520*/
            return $arr['year'].'-'.str_pad($arr['mon'], 2, "0", STR_PAD_LEFT).'-'.str_pad($arr['mday'], 2, "0", STR_PAD_LEFT);
            //$dt_Ayer= date('Y-m-d', strtotime('-1 day')) ; // resta 1 día
            //$dt_laSemanaPasada = date('Y-m-d', strtotime('-1 week')) ; // resta 1 semana
            //$dt_elMesPasado = date('Y-m-d', strtotime('-1 month')) ; // resta 1 mes
            //$dt_ElAnioPasado = date('Y-m-d', strtotime('-1 year')) ; // resta 1 año
            //Mostrar fechas
            //echo $dt_Ayer;
            //echo $dt_laSemanaPasada;
            //echo $dt_elMesPasado;
            //echo $dt_ElAnioPasado;
        }

        /**
        * @description: Methodo encargado de extraer la fecha inicio, fin de una semana pasandole la fecha actual
        * @author: Gregorio J. Bolivar B.
        * @param data $fecha variable de entrada, basado en el siguiente formato año-mes-día
        * @return array $dataArray con la fecha inicio, fin, actual de la semana, numero de la semana en curso y año actual
        */
        public function diaIniciofinSemana($fecha)
        {
            # Descomponemos la fecha que se paso por parametro en un arreglo para ser usada luego por parte
            $dato = explode('-', $fecha);

            # Asinamos los datos en variables luego de extraerla del arreglo
            $month=$dato[1];
            $day=$dato[2];
            $year=$dato[0];

            # Obtenemos el numero de la semana
            $semana=date("W",mktime(0,0,0,$month,$day,$year));
            # Obtenemos el día de la semana de la fecha dada
            $diaSemana=date("w",mktime(0,0,0,$month,$day,$year));

            # el 0 equivale al domingo...
            if($diaSemana==0)
                $diaSemana=7;
            # A la fecha recibida, le restamos el dia de la semana y obtendremos el lunes
            $primerDia=date("Y-m-d",mktime(0,0,0,$month,$day-$diaSemana+1,$year));
            # A la fecha recibida, le sumamos el dia de la semana menos siete y obtendremos el domingo
            $ultimoDia=date("Y-m-d",mktime(0,0,0,$month,$day+(7-$diaSemana),$year));
            $dataArray= array('fechaInicio' => $primerDia, 'fechaFin' =>$ultimoDia, 'fechaActual'=>$fecha,'semanaActual'=>$semana, 'anioActual'=>$year );
            return $dataArray;
        }

        /** Convertir fechas a castellano.
        * Descripción: Pasar de una fecha hecha con la función date() a un texto en castellano, 
        * conteniendo el nombre del día, el número del mismo, el mes y el año.
        * @param date $fecha Fecha con formato por defecto para convertir a formato en castellano.
        * @return string Cadena de caracteres con la fecha en formato en castellano.*/
        function fechaCastellano($fecha)
        {
            $fecha = substr($fecha, 0, 10);
            $numeroDia = date('d', strtotime($fecha));
            $dia = date('l', strtotime($fecha));
            $mes = date('F', strtotime($fecha));
            $anio = date('Y', strtotime($fecha));
            $dias_ES = array("Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado", "Domingo");
            $dias_EN = array("Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday");
            $nombredia = str_replace($dias_EN, $dias_ES, $dia);
            $meses_ES = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
            $meses_EN = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
            $nombreMes = str_replace($meses_EN, $meses_ES, $mes);
            return $nombredia." ".$numeroDia." de ".$nombreMes." de ".$anio;
        }

        /**
         * Generador de base64 de archivos
         * @author Gregorio Bolivar B <elalconxvii@gmail.com>
         * @version 1.0
         * @param string $ruta Ruta del archivo donde se guarda los documentos
         * @return string Base64 del archivo que se codifico
         *
         */
        function generarBase64($ruta)
        {
            self::validateFile($ruta);
            $data = base64_encode(file_get_contents($ruta));
            return $data;
        }

        /**
         * Eliminar archivo fisico del servidor luego que se genera el string del base 64
         * @author Gregorio Bolivar B <elalconxvii@gmail.com>
         * @version 1.0
         * @param string $ruta ruta donde esta creada el archivo en el servidor
         */
        function eliminarArchivoFisico($ruta)
        {
            self::validateFile($ruta);
            unlink($ruta);
        }

        /**
         * Permite obtener el mime del archivo
         * @author Gregorio Bolivar B <elalconxvii@gmail.com>
         * @param string $ruta Ruta donde se encuentra la imagen
         */
        public function obtenerMime($ruta)
        {
            return mime_content_type($ruta);
        }

        /**
         * Eliminar archivo fisico del servidor luego que se genera el string del base 64
         * @author Gregorio Bolivar B <elalconxvii@gmail.com>
         * @version 1.0
         * @param string $ruta ruta donde esta creada el archivo en el servidor
         */
        function validateFile($ruta)
        {
            if (file_exists($ruta)){
                return true;
            }else{
                die("<br/><center>Error 661: El archivo  <b>[".$ruta.']</b> ruta no encontrado, <br/>por favor revisar el nombre o la ruta del archivo.');
            }
        }

        /**
         * Permite generarte la imagen del capcha y te devuelve un base64
         * @example(titulo="Enviar generar un captcha",codigo="$respuesta=file_get_contents(&#34;http://esbutil.mppi.gob.ve/generarCaptcha/&#34;&#46;http_build_query(array(&#34;prm&#34;=>array(&#34;04126839727&#34;))))
         * @param ($code=" <b>codigo del captcha: </b>el valor numerico del capcha")
         * @return (string)
         *
         */
        public function generarCaptcha($code)
        {
            $this->setCache('rutaFuente',"../../../".$_SESSION['FDSoil']."/packs/fuente/");
            $appName=end(explode('/',substr($_SERVER['DOCUMENT_ROOT'],1)));
            $this->setCache('rutaSave','/tmp/'.$appName.'/');

            $code = trim($code);
            if ($code == '*') {
                $code = self::randomString($length=8,$uc=true,$n=true,$sc=2);
            }else{
                $code = self::randomString($length=$code,$uc=true,$n=true,$sc=2);
            }
            $width = 300;
            $heigth = 65;
            // Genero la imagen
            $img = imagecreatetruecolor($width, $heigth);

            // Colores
            $bgColor = imagecolorallocate($img, 230, 230, 230);
            $stringColor = imagecolorallocate($img, 90, 90, 90);
            $lineColor = imagecolorallocate($img, 245, 245, 245);

            // Fondo
            imagefill($img, 0, 0, $bgColor);

            imageline($img, 0, 5, $width, 5, $lineColor);
            imageline($img, 0, 10, $width, 10, $lineColor);
            imageline($img, 0, 15, $width, 15, $lineColor);
            imageline($img, 0, 20, $width, 20, $lineColor);
            imageline($img, 0, 25, $width, 10, $lineColor);
            imageline($img, 0, 30, $width, 30, $lineColor);
            imageline($img, 0, 35, $width, 35, $lineColor);
            imageline($img, 0, 40, $width, 40, $lineColor);
            imageline($img, 0, 45, $width, 45, $lineColor);
            imageline($img, 0, 50, $width, 50, $lineColor);
            imageline($img, 0, 55, $width, 55, $lineColor);
            imageline($img, 0, 60, $width, 60, $lineColor);

            imageline($img, 12, 0, 24, $heigth, $lineColor);
            imageline($img, 24, 0, 24, $heigth, $lineColor);
            imageline($img, 36, 0, 36, $heigth, $lineColor);
            imageline($img, 48, 0, 48, $heigth, $lineColor);
            imageline($img, 60, 0, 60, $heigth, $lineColor);
            imageline($img, 72, 0, 24, $heigth, $lineColor);
            imageline($img, 60, 0, 84, $heigth, $lineColor);
            imageline($img, 96, 0, 96, $heigth, $lineColor);
            imageline($img, 108, 0, 108, $heigth, $lineColor);
            imageline($img, 120, 0, 120, $heigth, $lineColor);
            imageline($img, 132, 0, 132, $heigth, $lineColor);
            imageline($img, 144, 0, 144, $heigth, $lineColor);
            imageline($img, 156, 0, 120, $heigth, $lineColor);
            imageline($img, 168, 0, 168, $heigth, $lineColor);
            imageline($img, 180, 0, 180, $heigth, $lineColor);
            imageline($img, 144, 0, 192, $heigth, $lineColor);
            imageline($img, 204, 0, 204, $heigth, $lineColor);
            imageline($img, 216, 0, 216, $heigth, $lineColor);
            imageline($img, 228, 0, 228, $heigth, $lineColor);
            imageline($img, 240, 0, 240, $heigth, $lineColor);
            imageline($img, 252, 0, 252, $heigth, $lineColor);
            imageline($img, 264, 0, 264, $heigth, $lineColor);
            imageline($img, 276, 0, 276, $heigth, $lineColor);
            imageline($img, 252, 0, 288, $heigth, $lineColor);
            imageline($img, 288, 0, 240, $heigth, $lineColor);
            imageline($img, 288, 0, 168, $heigth, $lineColor);


            //Cargar fuente
            $font = imageloadfont(self::getCache('rutaFuente') . 'bubblebath.gdf');

            // Escribo el codigo
            imageString($img, $font, 35, 15, $code, $stringColor);

        // Image output.
        $ruta = self::getCache('rutaSave') . "" . $code . ".png";
        imagepng($img, $ruta);

        // Extraer el tipo de mime del archivo
        $mime = self::obtenerMime($ruta);
        //
        $img = self::generarBase64($ruta);
        // Eliminar la imagen fisica del servidor
        self::eliminarArchivoFisico($ruta);
        // Ruta para el usuario
        $uri = 'data:' . $mime . ';base64,' . $img;
        $data=array("result" => false, "code"=>$code,"message" => "Generar imagen base64", "uri" => $uri);
        return json_encode($data);

    }

}

