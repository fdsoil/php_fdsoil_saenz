<?php
namespace FDSoil\Func;

class NumToStr
{

    private function _units($num)
    {

        switch($num)
        {
            case 1: return "UN";
            case 2: return "DOS";
            case 3: return "TRES";
            case 4: return "CUATRO";
            case 5: return "CINCO";
            case 6: return "SEIS";
            case 7: return "SIETE";
            case 8: return "OCHO";
            case 9: return "NUEVE";
        }
        return "";
    }

    private function _tens($num)
    {
        $decena = floor($num/10);
        $unidad = $num - ($decena * 10);
        switch($decena) {
            case 1:
                switch($unidad)
                {
                    case 0: return "DIEZ";
                    case 1: return "ONCE";
                    case 2: return "DOCE";
                    case 3: return "TRECE";
                    case 4: return "CATORCE";
                    case 5: return "QUINCE";
                    default: return "DIECI" . self::_units($unidad);
                }
            case 2:
                switch($unidad)
                {
                    case 0: return "VEINTE";
                    default: return "VEINTI" . self::_units($unidad);
                }
            case 3: return self::_tensAnd("TREINTA", $unidad);
            case 4: return self::_tensAnd("CUARENTA", $unidad);
            case 5: return self::_tensAnd("CINCUENTA", $unidad);
            case 6: return self::_tensAnd("SESENTA", $unidad);
            case 7: return self::_tensAnd("SETENTA", $unidad);
            case 8: return self::_tensAnd("OCHENTA", $unidad);
            case 9: return self::_tensAnd("NOVENTA", $unidad);
            case 0: return self::_units($unidad);
        }
    }

    private function _tensAnd($strSin, $numUnidades)
    {
        if ($numUnidades > 0)
            return $strSin . " Y " . self::_units($numUnidades);
        return $strSin;
    }

    private function _hundreds($num)
    {
        $centenas = floor($num / 100);
        $decenas = $num - ($centenas * 100);
        switch($centenas)
        {
            case 1:
                if ($decenas > 0)
                    return "CIENTO " . self::_tens($decenas);
                return "CIEN";
            case 2: return "DOSCIENTOS " . self::_tens($decenas);
            case 3: return "TRESCIENTOS " . self::_tens($decenas);
            case 4: return "CUATROCIENTOS " . self::_tens($decenas);
            case 5: return "QUINIENTOS " . self::_tens($decenas);
            case 6: return "SEISCIENTOS " . self::_tens($decenas);
            case 7: return "SETECIENTOS " . self::_tens($decenas);
            case 8: return "OCHOCIENTOS " . self::_tens($decenas);
            case 9: return "NOVECIENTOS " . self::_tens($decenas);
        }
        return self::_tens($decenas);
    }

    private function _section($num, $divisor, $strSingular, $strPlural)
    {
        $cientos = floor($num / $divisor);
        $resto = $num - ($cientos * $divisor);
        $letras = "";
        if ($cientos > 0){
            if ($cientos > 1)
                $letras = self::_hundreds($cientos) . " " . $strPlural;
            else
                $letras = $strSingular;
        }
        if ($resto > 0)
            $letras .= "";
        return $letras;
    }

    private function _thousands($num)
    {
        $divisor = 1000;
        $cientos = floor($num / $divisor);
        $resto = $num - ($cientos * $divisor);
        $strMiles = self::_section($num, $divisor, "UN MIL", "MIL");
        $strCentenas = self::_hundreds($resto);
        if($strMiles == "")
            return $strCentenas;
       return $strMiles . " " . $strCentenas;
    }

    private function _millions($num)
    {
        $divisor = 1000000;
        $cientos = floor($num / $divisor);
        $resto = $num - ($cientos * $divisor);

        $strMillones = self::_section($num, $divisor, "UN MILLON ", "MILLONES ");
        $strMiles = self::_thousands($resto);

        if($strMillones == "")
            return $strMiles;

        return $strMillones . " " . $strMiles;
    }

    public function numToStr($num)
    {
        $data = [
            "numero" => $num,
            "enteros" => floor($num),
            "centavos" => (((round($num * 100)) - (floor($num) * 100))),
            "letrasCentavos" => "",
            "letrasMonedaPlural" => "", //BOLÍVAR
            "letrasMonedaSingular" => "", //BOLÍVARES
            "letrasMonedaCentavoPlural" => "CTS",
            "letrasMonedaCentavoSingular" => "CT"
        ];
        //if ($data["centavos"] > 0)
            $data["letrasCentavos"] = "CON " . $data["centavos"] . " " . $data["letrasMonedaCentavoPlural"];
        if($data["enteros"] == 0)
            return "CERO " . $data["letrasMonedaPlural"] . " " . $data["letrasCentavos"];
        if ($data["enteros"] == 1)
            return self::_millions($data["enteros"]) . " " . $data["letrasMonedaSingular"] . " " . $data["letrasCentavos"];
        else
            return self::_millions($data["enteros"]) . " " . $data["letrasMonedaPlural"] . " " . $data["letrasCentavos"];
        /*if ($data.centavos > 0) {
            $data.letrasCentavos = "CON " + (function (){
                if ($data.centavos == 1)
                    return _millions($data.centavos) + " " + $data.letrasMonedaCentavoSingular;
                else
                    return _millions($data.centavos) + " " + $data.letrasMonedaCentavoPlural;
                })();
        };*/
    }

}
/*
echo '0.1 = '.\FDSoil\Func\NumToStr::numToStr(0.1).'<br>';
echo '1.2 = '.\FDSoil\Func\NumToStr::numToStr(1.2).'<br>';
echo '2.3 = '.\FDSoil\Func\NumToStr::numToStr(2.3).'<br>';
echo '3.4 = '.\FDSoil\Func\NumToStr::numToStr(3.4).'<br>';
echo '4.5 = '.\FDSoil\Func\NumToStr::numToStr(4.5).'<br>';
echo '5.6 = '.\FDSoil\Func\NumToStr::numToStr(5.6).'<br>';
echo '6.7 = '.\FDSoil\Func\NumToStr::numToStr(6.7).'<br>';
echo '7.8 = '.\FDSoil\Func\NumToStr::numToStr(7.8).'<br>';
echo '8.9 = '.\FDSoil\Func\NumToStr::numToStr(8.9).'<br>';
echo '9.9 = '.\FDSoil\Func\NumToStr::numToStr(9.9).'<br>';
echo '10 = '.\FDSoil\Func\NumToStr::numToStr(10).'<br>';
echo '11 = '.\FDSoil\Func\NumToStr::numToStr(11).'<br>';
echo '12 = '.\FDSoil\Func\NumToStr::numToStr(12).'<br>';
echo '13 = '.\FDSoil\Func\NumToStr::numToStr(13).'<br>';
echo '14 = '.\FDSoil\Func\NumToStr::numToStr(14).'<br>';
echo '15 = '.\FDSoil\Func\NumToStr::numToStr(15).'<br>';
echo '16 = '.\FDSoil\Func\NumToStr::numToStr(16).'<br>';
echo '17 = '.\FDSoil\Func\NumToStr::numToStr(17).'<br>';
echo '18 = '.\FDSoil\Func\NumToStr::numToStr(18).'<br>';
echo '19 = '.\FDSoil\Func\NumToStr::numToStr(19).'<br>';
echo '20.51 = '.\FDSoil\Func\NumToStr::numToStr(20.51).'<br>';
echo '21.52 = '.\FDSoil\Func\NumToStr::numToStr(21.52).'<br>';
echo '30.53 = '.\FDSoil\Func\NumToStr::numToStr(30.53).'<br>';
echo '32.54 = '.\FDSoil\Func\NumToStr::numToStr(32.54).'<br>';
echo '40.55 = '.\FDSoil\Func\NumToStr::numToStr(40.55).'<br>';
echo '43.56 = '.\FDSoil\Func\NumToStr::numToStr(43.56).'<br>';
echo '50.57 = '.\FDSoil\Func\NumToStr::numToStr(50.57).'<br>';
echo '54.58 = '.\FDSoil\Func\NumToStr::numToStr(54.58).'<br>';
echo '60.59 = '.\FDSoil\Func\NumToStr::numToStr(60.59).'<br>';
echo '65 = '.\FDSoil\Func\NumToStr::numToStr(65).'<br>';
echo '70 = '.\FDSoil\Func\NumToStr::numToStr(70).'<br>';
echo '76 = '.\FDSoil\Func\NumToStr::numToStr(76).'<br>';
echo '80 = '.\FDSoil\Func\NumToStr::numToStr(80).'<br>';
echo '87 = '.\FDSoil\Func\NumToStr::numToStr(87).'<br>';
echo '90 = '.\FDSoil\Func\NumToStr::numToStr(90).'<br>';
echo '98 = '.\FDSoil\Func\NumToStr::numToStr(98).'<br>';
echo '99 = '.\FDSoil\Func\NumToStr::numToStr(99).'<br>';
echo '100 = '.\FDSoil\Func\NumToStr::numToStr(100).'<br>';
echo '200 = '.\FDSoil\Func\NumToStr::numToStr(200).'<br>';
echo '300 = '.\FDSoil\Func\NumToStr::numToStr(300).'<br>';
echo '400 = '.\FDSoil\Func\NumToStr::numToStr(400).'<br>';
echo '500 = '.\FDSoil\Func\NumToStr::numToStr(500).'<br>';
echo '600 = '.\FDSoil\Func\NumToStr::numToStr(600).'<br>';
echo '700 = '.\FDSoil\Func\NumToStr::numToStr(700).'<br>';
echo '800 = '.\FDSoil\Func\NumToStr::numToStr(800).'<br>';
echo '900 = '.\FDSoil\Func\NumToStr::numToStr(900).'<br>';
echo '1000 = '.\FDSoil\Func\NumToStr::numToStr(1000).'<br>';
echo '2000 = '.\FDSoil\Func\NumToStr::numToStr(2000).'<br>';
echo '3000 = '.\FDSoil\Func\NumToStr::numToStr(3000).'<br>';
echo '4000 = '.\FDSoil\Func\NumToStr::numToStr(4000).'<br>';
echo '5000 = '.\FDSoil\Func\NumToStr::numToStr(5000).'<br>';
echo '6000 = '.\FDSoil\Func\NumToStr::numToStr(6000).'<br>';
echo '7000 = '.\FDSoil\Func\NumToStr::numToStr(7000).'<br>';
echo '8000 = '.\FDSoil\Func\NumToStr::numToStr(8000).'<br>';
echo '9000 = '.\FDSoil\Func\NumToStr::numToStr(9000).'<br>';
echo '10000 = '.\FDSoil\Func\NumToStr::numToStr(10000).'<br>';
echo '20000 = '.\FDSoil\Func\NumToStr::numToStr(20000).'<br>';
echo '30000 = '.\FDSoil\Func\NumToStr::numToStr(30000).'<br>';
echo '40000 = '.\FDSoil\Func\NumToStr::numToStr(40000).'<br>';
echo '50000 = '.\FDSoil\Func\NumToStr::numToStr(50000).'<br>';
echo '60000 = '.\FDSoil\Func\NumToStr::numToStr(60000).'<br>';
echo '70000 = '.\FDSoil\Func\NumToStr::numToStr(70000).'<br>';
echo '80000 = '.\FDSoil\Func\NumToStr::numToStr(80000).'<br>';
echo '90000 = '.\FDSoil\Func\NumToStr::numToStr(90000).'<br>';
echo '100000 = '.\FDSoil\Func\NumToStr::numToStr(100000).'<br>';
echo '200000 = '.\FDSoil\Func\NumToStr::numToStr(200000).'<br>';
echo '300000 = '.\FDSoil\Func\NumToStr::numToStr(300000).'<br>';
echo '400000 = '.\FDSoil\Func\NumToStr::numToStr(400000).'<br>';
echo '500000 = '.\FDSoil\Func\NumToStr::numToStr(500000).'<br>';
echo '600000 = '.\FDSoil\Func\NumToStr::numToStr(600000).'<br>';
echo '700000 = '.\FDSoil\Func\NumToStr::numToStr(700000).'<br>';
echo '800000 = '.\FDSoil\Func\NumToStr::numToStr(800000).'<br>';
echo '900000 = '.\FDSoil\Func\NumToStr::numToStr(900000).'<br>';
echo '1000000 = '.\FDSoil\Func\NumToStr::numToStr(1000000).'<br>';
echo '2000000 = '.\FDSoil\Func\NumToStr::numToStr(2000000).'<br>';
echo '3000000 = '.\FDSoil\Func\NumToStr::numToStr(3000000).'<br>';
echo '3001007 = '.\FDSoil\Func\NumToStr::numToStr(3001007). '<br>';
echo '3000107 = '.\FDSoil\Func\NumToStr::numToStr(3000107). '<br>';
echo '3000017 = '.\FDSoil\Func\NumToStr::numToStr(3000017). '<br>';
echo '3000007 = '.\FDSoil\Func\NumToStr::numToStr(3000007). '<br>';
echo '4000000 = '.\FDSoil\Func\NumToStr::numToStr(4000000).'<br>';
echo '5000000 = '.\FDSoil\Func\NumToStr::numToStr(5000000).'<br>';
echo '6000000 = '.\FDSoil\Func\NumToStr::numToStr(6000000).'<br>';
echo '7000000 = '.\FDSoil\Func\NumToStr::numToStr(7000000).'<br>';
echo '8000000 = '.\FDSoil\Func\NumToStr::numToStr(8000000).'<br>';
echo '9000000 = '.\FDSoil\Func\NumToStr::numToStr(9000000).'<br>';
echo '9100000 = '.\FDSoil\Func\NumToStr::numToStr(9100000).'<br>';
echo '9250000 = '.\FDSoil\Func\NumToStr::numToStr(9250000).'<br>';
echo '9253000 = '.\FDSoil\Func\NumToStr::numToStr(9253000).'<br>';
echo '9.253.682,50 = '.\FDSoil\Func\NumToStr::numToStr(9253682.50).'<br>';
echo '10000000 = '.\FDSoil\Func\NumToStr::numToStr(10000000).'<br>';
echo '984.253.682,50 = '.\FDSoil\Func\NumToStr::numToStr(984253682.50).'<br>';die();*/


