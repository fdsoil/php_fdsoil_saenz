<?php
trait FuncArr {

    /** Codifica arreglo base 64.
    * Descripción: Codifica arreglo a base 64.
    * @param array $array Arreglo.
    * @return array Arreglo codificado en base 64.*/ 
    public function base64EncodeArrVal($arrOld)
    {
        $arrNew=[];                             
        for ($i = 0; $i < count($arrOld); $i++)
           $arrNew[$i] = base64_encode($arrOld[$i]);             
        return $arrNew;
    }

    /** Decodifica arreglo base 64.
    * Descripción: Decodifica arreglo previamente codificado con base 64.
    * @param array $array Arreglo.
    * @return array Arreglo decodificado en base 64.*/ 
    public function base64DecodeArrVal($arrOld)
    {
        $arrNew=[];		
        for ($i = 0; $i < count($arrOld); $i++) 
           $arrNew[$i] = base64_decode($arrOld[$i]);  
        return $arrNew;
    }

    /** Codifica arreglo base 64.
    * Descripción: Codifica arreglo asociativo a base 64, incluyendo índices.
    * @param array $array Arreglo.
    * @return array Arreglo asociativo codificado en base 64.*/
    public function base64EncodeArrValKey($arrOld)
    {
        $arrNew=[];
        foreach ($arrOld as $key => $value)
           $arrNew[base64_encode($key)] = base64_encode($value);
        return $arrNew;
    }

    /** Decodifica arreglo asociativo base 64.
    * Descripción: Decodifica arreglo asociativo previamente codificado con base 64, incluyendo índices.
    * @param array $array Arreglo.
    * @return array Arreglo asociativo decodificado en base 64.*/
    public function base64DecodeArrValKey($arrOld)
    {
        $arrNew=[];
        foreach ($arrOld as $key => $value)
           $arrNew[base64_decode($key)] = base64_decode($value);
        return $arrNew;
    }

    /** Codifica arreglo url.
    * Descripción: Codifica arreglo a url.
    * @param array $array Arreglo.
    * @return array Arreglo codificado en url.*/ 
    public function urlEncodeArrVal($arrOld)
    {
        $arrNew=[];                             
        for ($i = 0; $i < count($arrOld); $i++)
           $arrNew[$i] = urlencode($arrOld[$i]);             
        return $arrNew;
    }

    /** Decodifica arreglo url.
    * Descripción: Decodifica arreglo previamente codificado con url.
    * @param array $array Arreglo.
    * @return array Arreglo decodificado en url.*/ 
    public function urlDecodeArrVal($arrOld)
    {
        $arrNew=[];		
        for ($i = 0; $i < count($arrOld); $i++) 
           $arrNew[$i] = urldecode($arrOld[$i]);  
        return $arrNew;
    }

    /** Codifica arreglo url.
    * Descripción: Codifica arreglo asociativo a url, incluyendo índices.
    * @param array $array Arreglo.
    * @return array Arreglo asociativo codificado en url.*/
    public function urlEncodeArrValKey($arrOld)
    {
        $arrNew=[];
        foreach ($arrOld as $key => $value)
           $arrNew[urlencode($key)] = urlencode($value);
        return $arrNew;
    }

    /** Decodifica arreglo asociativo url.
    * Descripción: Decodifica arreglo asociativo previamente codificado con url, incluyendo índices.
    * @param array $array Arreglo.
    * @return array Arreglo asociativo decodificado en base 64.*/
    public function urlDecodeArrValKey($arrOld)
    {
        $arrNew=[];
        foreach ($arrOld as $key => $value)
           $arrNew[urldecode($key)] = urldecode($value);
        return $arrNew;
    }

    /** Convierte arreglo a cadena de caracteres.
    * Descripción: Convertir un arreglo a cadena de caracteres.
    * @param array $array Arreglo.
    * @return string Cadena de caracteres separados por comas (,).*/ 
    public function array_to_string($array) {
        $string = '';
        for ($i = 0; $i < count($array); $i++) {
            $string.=$array[$i] . ',';
        }
        return substr($string, 0, strlen($string) - 1);
    } 

    /** Convertir arreglo a objeto JSON.
    * Descripción: Convierte un arreglo a un objeto JSON	
    * @param array $arr Arreglo.
    * @return object $strJSON Devuelve objeto JSON.*/ 
    public function arrayToJSON($arr){
       $strJSON='';
       foreach ($arr as $key => $value) 		
           $strJSON.=' "'.$key.'": "'.$value.'",';  
       $strJSON=substr( $strJSON, 0, strlen( $strJSON)-strlen( ','));
       return $strJSON;
    }

    /** Convertir arreglo a XML.
    * Descripción: Convierte un arreglo a un XML (Lenguaje de Marcas Extensible)'	
    * @param array $arr Arreglo.
    * @return element $strXML Devuelve un XML.*/ 
    public function arrayToXML($arr){
        foreach ($arr as $key => $value) 
            $strXML.="\t".'<'.$key.'>'.$value.'</'.$key.'>'."\n";              
        return $strXML;
    }

    /** Convierte una cadena de caracteres en forma de arreglo a matriz.
    * Descripción: Convertir una cadena de caracteres en forma de arreglo bidimencional a matriz.
    *
    * Nota: La cadena de caracteres en forma de arreglo bidimencional debe tener caracteres especiales
    * específicos que funcionen como separadores de las columnas y las filas respectivamente.
    * @param string $string Cadena de caracteres en forma de arreglo bidimencional con caracteres especiales
    * específicos que funcionen como separadores de las columnas y las filas respectivamente.
    * @param string $separate1 Caracter especial por el cual se separarán las filas.
    * @param string $separate2 Caracter especial por el cual se separarán las columnas.
    * @return array $subarray Matriz (arreglo bidimencional).*/
    public function explode_string_to_matrix($string, $separate1, $separate2) {
        $array = explode($separate1, $string);
        for ($i = 0; $i < count($array); $i++) {
            $subarray[$i] = explode($separate2, $array[$i]);
        }
        return $subarray;
    }

    /** Comprueba en un arreglo la existencia de un índice especificado.
    * Descripción: Comprobar en un arreglo o matriz la existencia de un índice (clave) específico.
    * Desactualizado ( PHP posee una función similar que hace el mismo trabajo recomendada y llamada: array_key_exists() )
    * @param array $array Arreglo o matriz
    * @param string $key Índice (clave)	
    * @return boolean Devuelve TRUE si existe el índice (clave) y FALSE si no existe.*/ 
    public function isThereThisKeyInTheArray($array, $key) {
       $return = false;
       foreach ($array as $c => $v) {
           if ($c == $key) {
               $return = true;
               break;
           }
       }
       return $return;
    }

    /** Convierte matriz a cadena de caracteres en forma de arreglo.
    * Descripción: Convertir matriz (arreglo bidimencional) a cadena de caracteres en forma de arreglo	
    * @param array $matrix Matriz (arreglo bidimencional)
    * @param string $separate1 Caracter especial por el cual se separarán las columnas.
    * @param string $separate2 Caracter especial por el cual se separarán las filas.
    * @return string Cadena de caracteres en forma de arreglo.*/ 
    function matrixToString($matrix,$separate1,$separate2){
        $numRow=count($matrix);
        $strMatrix='';
        for ($n=0;$n<count($matrix);$n++){      
            foreach ($matrix[$n] as $key => $value){
                $strMatrix.=$value.$separate1;
            }
            $strMatrix=substr($strMatrix,0,strlen($strMatrix)-1).$separate2;
        }   
        return substr($strMatrix,0,strlen($strMatrix)-1);
    }

    /** Reemplaza en una matriz los acentos y convierte las cadenas en mayúsculas.
    * Descripción: Reemplazar en una matriz (arreglo bidimencional) de cadena de caracteres, 
    * los acentos y convierte todas las cadena de caracteres en mayúsculas.	
    * @param array $matrix Matriz con cadenas de caracteres
    * @return array $matrix Matriz con cadenas de caracteres sin acentos y en mayúscula.*/
    function matrixUnaccentedUppercase($matrix){
        for ($i = 0; $i < count($matrix); $i++){
            foreach($matrix[$i] as $campo=>$valor){		
                $matrix[$i][$campo]=$this->unaccentedUppercase($valor);
            }
        }
        return $matrix;
    }

    public function pasPropOfObjToArr( $oJSON, $arrKey, $arrObjProp = [])
    { 
        foreach ($arrKey as $val1):
            foreach ($oJSON->$val1 as $key => $val2):
                $arrObjProp[$key]=$val2;
            endforeach;
        endforeach;

        return $arrObjProp;
    }

    /** Reindexa un vector.
    * Descripción: Reindexar un vector.
    * @param array $vector Arreglo (vector).
    * @return array $return Arreglo reindexado.*/ 
    public function reindex_vector($vector) {
        $i = 0;
        foreach ($vector as $c => $v) {
            $return[$i] = $v;
            $i++;
        }
        return $return;
    }

    /** Remover caracteres especiales de un arreglo de cadena de texto.
    * Descripción: Remueve los caracteres especiales de un arreglo de cadena de texto. 
    * @param array $array Arreglo de cadena de texto.
    * @return array Devuelve el arreglo de cadena de texto sin caracteres especiales.*/ 
    function removeSpecialCharacters($array) {
        foreach ($array as $key => $value) {
            $array[$key]=str_replace('"',"",$array[$key]); 
            $array[$key]=str_replace("'","",$array[$key]); 
            $array[$key]=str_replace("#","",$array[$key]);
            $array[$key]=str_replace(">","",$array[$key]);
            $array[$key]=str_replace("<","",$array[$key]);
            $array[$key]=str_replace("$","",$array[$key]);
            $array[$key]=str_replace("¬","",$array[$key]);
            $array[$key]=str_replace("|","",$array[$key]);
            $array[$key]=str_replace("~","",$array[$key]);
            $array[$key]=str_replace("}","",$array[$key]);
            $array[$key]=str_replace("{","",$array[$key]);
            $array[$key]=str_replace("]","",$array[$key]);
            $array[$key]=str_replace("[","",$array[$key]);
            $array[$key]=str_replace("·","",$array[$key]);
            $array[$key]=str_replace("´","",$array[$key]);
            $array[$key]=str_replace("`","",$array[$key]);
            $array[$key]=str_replace("¨","",$array[$key]);
            $array[$key]=str_replace("^","",$array[$key]);
            $array[$key]=str_replace("=","",$array[$key]);
            $array[$key]=str_replace("\\","",$array[$key]);
      }
      return $array;
    }

    /** Muestra el contenido de un arreglo.
    * Descripción: Mostrar en pantalla el contenido de un arreglo o matriz. 
    *
    * Nota: Este método detiene la ejecución del programa para que el arreglo pueda ser mostrado en pantalla.
    * @param array $array Arreglo o matriz
    * @return string Muestra en pantalla el contenido del arreglo o matriz.*/        
    public function seeArray($array){
        echo '<pre>';print_r($array);echo '</pre>';die();
    }

    /** Extrae una columna de una matriz.
    * Descripción: Extraer una columna de una matriz o arreglo bidimencinal.
    * @param array $matrix Matriz
    * @param string $key Indice (clave) de la columna que se extraerá de la matriz
    * @return array $array Columna extraida de la matriz.*/ 
    public function separate_matrix_to_array($matrix, $key) {
        for ($i = 0; $i < count($matrix); $i++) {
            $array[$i] = $matrix[$i][$key];
        }
        return $array;
    }

    /** Ordena una matriz por una columna especificada.
    * Descripción:  Ordenar una matriz (arreglo bidimencional) por una columna específica.
    * @param array $matrix Matriz (arreglo bidimencional)
    * @param integer $sortNumCol Número de la columna por la cual se ordenará la matriz
    * @return array $matrix Devuelve una matriz ordenada por una columna especificada.*/
    public function sortMatrixBubble($matrix, $sortNumCol){  
        for($i=1;$i<count($matrix)-1;$i++){                  
            for($j=$i+1;$j<count($matrix);$j++){
                if ($matrix[$i][$sortNumCol]<$matrix[$j][$sortNumCol]){
                    $arrayAux=$matrix[$i];
                    $matrix[$i]=$matrix[$j];
                    $matrix[$j]=$arrayAux;
                }
            }
        } 
        return $matrix;
    }

}

/* End of the Trait */
