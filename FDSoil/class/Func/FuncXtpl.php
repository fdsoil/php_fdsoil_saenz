<?php
use \FDSoil\XTemplate as XTemplate;

trait FuncXtpl
{

    private function btnsArrBuild($aBtn)
    {
        $arr=[];
        for ($i = 0; $i < count($aBtn); $i++){
            $path = array_key_exists("btnPath",$aBtn[$i])
                     ?in_array($aBtn[$i]["btnPath"],['FDSoil','appOrg','myApp'])
                      ?"../../../".$_SESSION[$aBtn[$i]["btnPath"]]."/html"
                      :$aBtn[$i]["btnPath"]
                     :"../../../".$_SESSION['FDSoil']."/html";
            $xtpl = new XTemplate($path."/button".$aBtn[$i]["btnName"].".html");
            if ($aBtn[$i]["btnName"]==='Return') {
               if ( array_key_exists( "btnBack" , $aBtn[$i] ) )
                   $back = "location.href='../".$aBtn[$i]["btnBack"]."/'";
               else if ( array_key_exists( "btnUnBlock", $aBtn[$i] ) )
                   $back = $aBtn[$i]["btnUnBlock"];
               else if ( array_key_exists( "btnSubmit", $aBtn[$i] ) )
                   $back = $aBtn[$i]["btnSubmit"];
               else  
                   $back = "history.back(-1);";
               $xtpl->assign('BACK', $back);
           }
           if (array_key_exists("btnId",$aBtn[$i]))
               $xtpl->assign('ID', $aBtn[$i]["btnId"]);
           if (array_key_exists("btnImg",$aBtn[$i]))
               $xtpl->assign('IMG', $aBtn[$i]["btnImg"]);
           if (array_key_exists("btnLabel",$aBtn[$i]))
               $xtpl->assign('LABEL', $aBtn[$i]["btnLabel"]);
           $xtpl->assign('CLICK', array_key_exists("btnClick",$aBtn[$i])
                                   ?$aBtn[$i]["btnClick"]
                                   :"document.forms[0].submit();");
           $styleDisplay = array_key_exists("btnDisplay",$aBtn[$i])
                           ?str_replace("{fld:value}", $aBtn[$i]["btnDisplay"], "style=\"display:{fld:value}\"")
                           :'';
           $xtpl->assign('STYLE_DISPLAY', $styleDisplay);
           self::appShowId($xtpl);
           $xtpl->parse('main');
           $arr[$i]=$xtpl->out_var('main');
        }
        return $arr;
    }

    private function btnsGroupBuild($aXtplBtns)
    {
        $xtpl = new XTemplate("../../../".$_SESSION['FDSoil']."/html/buttonsPanel.html");
        $xtplBtns = '';
        for ($i = 0; $i < count($aXtplBtns); $i++)
            $xtplBtns.=$aXtplBtns[$i];
        $xtpl->assign('BOTTONS', $xtplBtns);
        $xtpl->parse('main');
        return $xtpl->out_var('main');
    }

    public function btnsPutPanel($xtpl, $aBtns)
    {
        $xtpl->assign('BUTTONS_PANEL', self::btnsGroupBuild(self::btnsArrBuild($aBtns)));
    }

    private function btnRecordAddAux($aBtn)
    {
        $xtpl = new XTemplate("../../../".$_SESSION['FDSoil']."/html/buttonRecordAdd.html");
        $xtpl->assign('ID', array_key_exists("btnId",$aBtn)?$aBtn["btnId"]:"");
        $xtpl->assign('NAME_OF_RECORD', array_key_exists("btnRecordName",$aBtn)?$aBtn["btnRecordName"]:"Nuevo Registro");
        $xtpl->assign('DISPLAY', array_key_exists("btnDisplay",$aBtn)?"display:".$aBtn["btnDisplay"].";":"");
        self::appShowId($xtpl);
        $xtpl->parse('main');
        return $xtpl->out_var('main');
    }

    public function btnRecordAdd($xtpl, $aBtn)
    {
        $xtpl->assign('BUTTON_ADD', self::btnRecordAddAux($aBtn));
    }

    private function btnRecordEditAux($aBtn)
    {
        $xtpl = new XTemplate("../../../".$_SESSION['FDSoil']."/html/buttonRecordEdit.html");
        $xtpl->assign('ID', $aBtn["btnId"]);
        $xtpl->assign('DISPLAY', array_key_exists("btnDisplay",$aBtn)?"style=display:".$aBtn["btnDisplay"].";":"");
        self::appShowId($xtpl);
        $xtpl->parse('main');
        return $xtpl->out_var('main');
    }

    public function btnRecordEdit($xtpl, $aBtn)
    {
        $xtpl->assign('BUTTON_EDIT', self::btnRecordEditAux($aBtn));
    }

    private function btnRecordEditBlockAux($aReg)
    {
        $xtpl = new XTemplate("../../../".$_SESSION['FDSoil']."/html/buttonRecordEditBlock.html");
        self::appShowId($xtpl);
        if ($aReg['editando']=='t') {/* Registro Bloqueado */
            if ($aReg['id_user_edit']==$_SESSION['id_usuario']) {/* Registro Bloqueado por el Mismo Usuario */
                $xtpl->assign('EDITAR_BLOCK', 'bandera');
                $xtpl->assign('TITLE_EDIT_BLOCK', 'Registro Pre-Editado por: Usted mismo');
                $xtpl->assign('SUBMIT_EDIT_ID', 'submitEdit('.$aReg['id'].');');
                $xtpl->assign('CURSOR', 'pointer');
            } else {/* Registro Bloqueado por Otro Usuario*/
                $xtpl->assign('EDITAR_BLOCK', 'lock');
                $xtpl->assign('TITLE_EDIT_BLOCK', 'Registro Bloqueado por: '.$aReg['user_edit']);
                $xtpl->assign('SUBMIT_EDIT_ID', '');
                $xtpl->assign('CURSOR', 'default');
            }                
        } else {/* Registro no Bloqueado */
            $xtpl->assign('EDITAR_BLOCK', 'edit');
            $xtpl->assign('TITLE_EDIT_BLOCK', 'Editar Registro');
            $xtpl->assign('SUBMIT_EDIT_ID', 'submitEdit('.$aReg['id'].');');
            $xtpl->assign('CURSOR', 'pointer');
        }
        $xtpl->parse('main');
        return $xtpl->out_var('main');
    }

    public function btnRecordEditBlock($xtpl, $aReg)
    {
        $xtpl->assign('BUTTON_EDIT_BLOCK', self::btnRecordEditBlockAux($aReg));
    }

    private function btnExitAux()
    {
        $xtpl = new XTemplate("../../../".$_SESSION['FDSoil']."/html/buttonExit.html");
        self::appShowId($xtpl);
        $xtpl->parse('main');
        return $xtpl->out_var('main');
    }

    public function btnExit($xtpl)
    {
        $xtpl->assign('BUTTON_EXIT', self::btnExitAux());
    }

    private function btnRecordDeleteAux($aBtn)
    {
        $xtpl = new XTemplate("../../../".$_SESSION['FDSoil']."/html/buttonRecordDelete.html");
        $xtpl->assign('ID', $aBtn["btnId"]);
        $xtpl->assign('DISPLAY', array_key_exists("btnDisplay",$aBtn)?"style=display:".$aBtn["btnDisplay"].";":"");
        self::appShowId($xtpl);
        $xtpl->parse('main');
        return $xtpl->out_var('main');
    }

    public function btnRecordDelete($xtpl, $aBtn)
    {
        $xtpl->assign('BUTTON_DELETE', self::btnRecordDeleteAux($aBtn));
    }

    private function btnRecordPrintAux($aBtn)
    {
        $xtpl = new XTemplate("../../../".$_SESSION['FDSoil']."/html/buttonRecordPrint.html");
        $xtpl->assign('ID', $aBtn["btnId"]);
        $xtpl->assign('DISPLAY', array_key_exists("btnDisplay",$aBtn)?"style=display:".$aBtn["btnDisplay"].";":"");
        self::appShowId($xtpl);
        $xtpl->parse('main');
        return $xtpl->out_var('main');
    }

    public function btnRecordPrint($xtpl, $aBtn)
    {
        $xtpl->assign('BUTTON_PRINT', self::btnRecordPrintAux($aBtn));
    }

    private function btnRecordSearchAux($aBtn)
    {
        $xtpl = new XTemplate("../../../".$_SESSION['FDSoil']."/html/buttonRecordSearch.html");
        $xtpl->assign('ID', array_key_exists("btnId",$aBtn)?$aBtn["btnId"]:"");
        $xtpl->assign('NAME_OF_RECORD', array_key_exists("btnRecordName",$aBtn)?$aBtn["btnRecordName"]:"Consultar Registro");
        $xtpl->assign('DISPLAY', array_key_exists("btnDisplay",$aBtn)?"display:".$aBtn["btnDisplay"].";":"");
        self::appShowId($xtpl);
        $xtpl->parse('main');
        return $xtpl->out_var('main');
    }

    public function btnRecordSearch($xtpl, $aBtn)
    {
        $xtpl->assign('BUTTON_SEARCH', self::btnRecordSearchAux($aBtn));
    }

    private function btnRecordFilterAux($aBtn)
    {
        $xtpl = new XTemplate("../../../".$_SESSION['FDSoil']."/html/buttonRecordFilter.html");
        $xtpl->assign('ID', array_key_exists("btnId",$aBtn)?$aBtn["btnId"]:"");
        $xtpl->assign('NAME_OF_RECORD', array_key_exists("btnRecordName",$aBtn)?$aBtn["btnRecordName"]:"Filtrar Registro");
        $xtpl->assign('DISPLAY', array_key_exists("btnDisplay",$aBtn)?"display:".$aBtn["btnDisplay"].";":"");
        self::appShowId($xtpl);
        $xtpl->parse('main');
        return $xtpl->out_var('main');
    }

    public function btnRecordFilter($xtpl, $aBtn)
    {
        $xtpl->assign('BUTTON_FILTER', self::btnRecordFilterAux($aBtn));
    }

    private function btnUndoAux($aBtn)
    {
        $xtpl = new XTemplate("../../../".$_SESSION['FDSoil']."/html/buttonUndo.html");
        $xtpl->assign('CLICK', $aBtn["btnClick"]);
        self::appShowId($xtpl);
        $xtpl->parse('main');
        return $xtpl->out_var('main');
    }

    public function btnUndo($xtpl, $aBtn)
    {
        $xtpl->assign('BUTTON_UNDO', self::btnUndoAux($aBtn));
    }

    private function btnUploadAux($aBtn)
    {
        $xtpl = new XTemplate("../../../".$_SESSION['FDSoil']."/html/buttonUpload.html");
        $xtpl->assign('CLICK', $aBtn["btnClick"]);
        self::appShowId($xtpl);
        $xtpl->parse('main');
        return $xtpl->out_var('main');
    }

    public function btnUpload($xtpl, $aBtn)
    {
        $xtpl->assign('BUTTON_UPLOAD', self::btnUploadAux($aBtn));
    }

    private function btnDownloadAux($aBtn)//Bajar Archivo
    {
        $xtpl = new XTemplate("../../../".$_SESSION['FDSoil']."/html/buttonDownload.html");
        $xtpl->assign('CLICK', $aBtn["btnClick"]);
        self::appShowId($xtpl);
        $xtpl->parse('main');
        return $xtpl->out_var('main');
    }

    public function btnDownload($xtpl, $aBtn)
    {
        $xtpl->assign('BUTTON_DOWNLOAD', self::btnDownloadAux($aBtn));
    }

    private function btnSaveAux($aBtn)
    {
        $xtpl = new XTemplate("../../../".$_SESSION['FDSoil']."/html/buttonSave.html");
        $xtpl->assign('CLICK', $aBtn["btnClick"]);
        self::appShowId($xtpl);
        $xtpl->parse('main');
        return $xtpl->out_var('main');
    }

    public function btnSave($xtpl, $aBtn)
    {
        $xtpl->assign('BUTTON_SAVE', self::btnSaveAux($aBtn));
    }

    private function btnPrintAux($aBtn)
    {
        $xtpl = new XTemplate("../../../".$_SESSION['FDSoil']."/html/buttonPrint.html");
        $xtpl->assign('CLICK', $aBtn["btnClick"]);
        self::appShowId($xtpl);
        $xtpl->parse('main');   
        return $xtpl->out_var('main');
    }

    public function btnPrint($xtpl, $aBtn)
    {
        $xtpl->assign('BUTTON_PRINT', self::btnPrintAux($aBtn));
    }

    /** Datos del Usuario.
    * Descripción: Muestra los datos del usuario en el layout.
    * @return object Objeto xtpl con los datos del usuario.*/
    function usuarioData()
    {
        $xtpl = new XTemplate("../../../".$_SESSION['FDSoil']."/html/userData.html");
        self::usuarioShowId($xtpl);
        $xtpl->parse('main');
        return $xtpl->out_var('main');
    }

    function usuarioShowId($xtpl)
    {
        $xtpl->assign('ID_USUARIO', $_SESSION['id_usuario']);
        $xtpl->assign('USUARIO', $_SESSION['usuario']);
        $xtpl->assign('NOMBRE_APELLIDO', $_SESSION['nombre_apellido']);
        $xtpl->assign('CEDULA', substr($_SESSION['cedula'],0,1).'-'.substr($_SESSION['cedula'],1,strlen($_SESSION['cedula'])-1));
        $strFileName = "../../../".$_SESSION['myApp']."/config/app.json";
        file_exists($strFileName)?$oJSON=json_decode(file_get_contents($strFileName)):die("File nor Found " . $strFileName);
        if ($oJSON->usuario->show->sub_title1 !== "" && $oJSON->usuario->show->sub_title1 !== ""){
            $xtpl->assign('TITLE_USUARIO_AUX_1', $oJSON->usuario->show->sub_title1);
            $xtpl->assign('TITLE_USUARIO_AUX_2', $oJSON->usuario->show->sub_title2);
            if ($oJSON->usuario->show->sub_value1 !== "" && $oJSON->usuario->show->sub_value2 !== ""){
                $xtpl->assign('VALUE_USUARIO_AUX_1', $oJSON->usuario->show->sub_value1);
                $xtpl->assign('VALUE_USUARIO_AUX_2', $oJSON->usuario->show->sub_value2);
            }
            else{
                include_once("../../../".$_SESSION['myApp']."/config/usuario.php");
                $xtpl->assign('VALUE_USUARIO_AUX_1', $usuario['show']['sub_value1']);
                $xtpl->assign('VALUE_USUARIO_AUX_2', $usuario['show']['sub_value2']);
            }
        }
    }

    function bldSolapas($xtpl, $aSolapa)
    {
        for ($i = 0; $i < count($aSolapa);$i++)
            $xtpl->assign('SOLAPA_'.$i, $aSolapa[$i]);
    }

}

/* End of the Trait */
