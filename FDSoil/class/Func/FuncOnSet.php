<?php
trait FuncOnSet
{

    public function appShowId($xtpl)
    {
        $xtpl->assign('MYAPP', $_SESSION['myApp']);      
        $xtpl->assign('APPORG', $_SESSION['appOrg']);
        $xtpl->assign('FDSOIL', $_SESSION['FDSoil']);
        $xtpl->assign('MYAPP_AS', strtolower($_SESSION['myApp']));
        $xtpl->assign('APPORG_AS', strtolower($_SESSION['appOrg']));
        $xtpl->assign('FDSOIL_AS', strtolower($_SESSION['FDSoil']));
        if (array_key_exists('pag_ini_default', $_SESSION))
           $xtpl->assign('PAG_INI_DEFAULT', strtolower($_SESSION['pag_ini_default']));
    }

/*    public function initIndex($menu="")
    {
        header('Content-type: text/html; charset=utf-8');
        include_once("../../../".$_SESSION['FDSoil']."/packs/xtpl/xtemplate.class.php");
        $xtpl = new XTemplate('view.html');    
        $noScript="<noscript><p>Para poder visualizar esta página web, necesitas tener activado JavaScript.</p></noscript>";
        $xtpl->assign('NOSCRIPT', $noScript); 
        self::appShowId($xtpl);
	if ($menu==='MENU')
            $this->menuShow($xtpl);
        return $xtpl;
    }    

    public function initIndex1($menu="")
    {
        header('Content-type: text/html; charset=utf-8');
        include_once("../../../".$_SESSION['FDSoil']."/packs/xtpl/xtemplate.class.php");
        //$xtpl = new XTemplate("../../../".$_SESSION['appOrg']."/layout/view.html");    
        $xtpl = new XTemplate('view.html');    
        $noScript="<noscript><p>Para poder visualizar esta página web, necesitas tener activado JavaScript.</p></noscript>";
        $xtpl->assign('NOSCRIPT', $noScript); 
        self::appShowId($xtpl);
	if ($menu==='MENU')
            $this->menuShow($xtpl);
        return $xtpl;
    }  
*/    
    public function validarPaquetesRequeridos()
    {
    	$requeridos=array('Core', 'date','PDO','pdo_pgsql','SimpleXML','xmlreader', 'soap','pdo_pgsql','gd','curl', 'json', 'session');
    	$cargados  = get_loaded_extensions();
		//echo "<pre>";	
		//print_r($cargados); die();//get_extension_funcs('PEAR')); die();
		foreach ($requeridos as $value) {
			if(!in_array($value,$cargados)){ 
				die("Error no tienes cargado el módulo $value");
			}
		}
		//$p=function_exists('pdo');//get_extension_funcs('Core');//get_loaded_extensions();
    	//print_r($cargados); die();
    	return false;
    }
 
    private function menuShow($xtpl)
    {
         $xtpl->assign('MENU', $_SESSION['menu']);
    }
   
}

/* End of the Trait */
