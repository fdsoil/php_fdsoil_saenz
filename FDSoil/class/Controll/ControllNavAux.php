<?php

trait ControllNavAux
{
    /**
     * ControlAux
     *
     * @author Ernesto Jiménez <fdsoil123@gmail.com>
     * @version 3.0
     * Auxiliar del Controlador Frontal:
     * 
     */

    private function reqsURI()
    {
         $aResp = [];
         $aResp['path'] = '../../../../..';
         $aResp['dir'] = self::asDirPhy($this->_aURI[$this->_nPos]);
         $aResp['file'] = '';
         $aResp['params'] = [];
         $j = 0;
         if ( $this->_aURI[$this->_nPos+1] === '' && $this->_nPos+1 === ( count($this->_aURI) - 1 ) )
             $aResp['file'] = '/index.php';
         else if ( in_array( $this->_aURI[$this->_nPos+2] , $this->_aFilesName ) )
             $aResp['file'] = '/'.$this->_aURI[$this->_nPos+2].'.php';
         else {
             $aResp['file'] = '/index.php';
             $aResp['params'][$j++] = $this->_aURI[$this->_nPos+2];
         }
         for ( $i = $this->_nPos+3 ; $i < count($this->_aURI) ; $i++ ) {
             $aResp['params'][$j++] = $this->_aURI[$i];
             if ( $i > 4)
                 $aResp['path'] .= '/..';
         }
         return $aResp;
    }

    private function formBegin()
    {
        $view="../../../".$_SESSION['FDSoil']."/html/";
        $view.=($this->_module.$this->_aReqs['file']==="admin_acceso/index.php")?"acceso.html":"layout.html";
        $this->_oLayout = new \FDSoil\XTemplate($view);      
        self::noScript($this->_oLayout);
        \FDSoil\Func::appShowId($this->_oLayout);
    }

    private function formEnd()
    {
        $this->_oLayout->assign('PATH', $this->_aReqs['path']); 
        $this->_oLayout->assign('DIR', $this->_aReqs['dir']); 
        $this->_oLayout->assign('MODULE', $this->_module); 
        $this->_oLayout->assign('USER_DATA', $this->_aView['userData']);
        if ( $this->_aView['load'] != [] )
            $this->_oLayout->assign('LOAD', $this->_aView['load']);
        $this->_oLayout->assign('INCLUDE', $this->_aView['include']);
        $this->_oLayout->assign('CONTENT', $this->_aView['content']);
        $this->_oLayout->parse('main'); 
        $this->_oLayout->out('main');
    }                                                                                                                  
                                                                                                                             
    private function noScript($xtpl)                                                                                         
    {                                                                                                                        
        $noScript="<noscript><p>Para poder visualizar esta página web, necesitas tener activado JavaScript.</p></noscript>"; 
        $xtpl->assign('NOSCRIPT', $noScript);                                                                                
    }

    private function asDirPhy($dir)
    {    
         $resp = "";    
         if ( $dir === strtolower($_SESSION['FDSoil']) )
            $resp = "FDSoil";
         else if ( $dir === strtolower($_SESSION['appOrg']) )
            $resp = "appOrg";
         else if ( $dir === strtolower($_SESSION['myApp']) )
            $resp = "myApp";
         return $_SESSION[$resp];
    }                  

}

