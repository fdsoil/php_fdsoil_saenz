function valid_date_range(fecha_previa,fecha_posterior){
    var fecha_prev = new Date(changeDateFormat(fecha_previa,'-'));
    var fecha_post = new Date(changeDateFormat(fecha_posterior,'-'));
    if((fecha_post.getTime()-fecha_prev.getTime()) >=0){
        return true;
    }else{
        return false;
    }
}
function date_range(fecha_previa,fecha_posterior){
    var fecha_prev = new Date(changeDateFormat(fecha_previa,'-'));
    var fecha_post = new Date(changeDateFormat(fecha_posterior,'-'));
    return (fecha_post.getTime()-fecha_prev.getTime())/86400000;
}

function changeDateFormat(fecha,separador) {
    var f = new Array();
    var caracter = ''
    var new_date ='';
    if (separador =='-') {
        caracter = /[/]/;
        if (caracter.test(fecha)) { //si esta en formato DD/MM/AAAA se cambia a AAAA-MM-DD
            f = fecha.split("/");
            new_date =f[2] + separador + f[1] + separador +f[0];
        }
    }else if (separador =='/') {
        caracter = /[-]/;
        if (caracter.test(fecha)) { //si esta en formato AAAA-MM-DD se cambia a DD/MM/AAAA
            f = fecha.split("-");
            new_date = f[2] + separador + f[1] + separador +f[0];
        }
    }else{
        return false;
    }
    return new_date;
}

function dateOfToday(format, separator){	
	var f = new Date();
	separator=separator==null?'':separator;
	switch (format.toUpperCase()){
  		case 'YYYYMMDD': 
			return f.getFullYear()+separator+pad((f.getMonth()+1),2)+separator+pad(f.getDate(),2);
                    	break;
		case 'YYMMDD': 
			return f.getFullYear().toString().substr(2,2)+separator+pad((f.getMonth() +1),2)+separator+pad(f.getDate(),2);
                    	break;
  		case 'DDMMYYYY': 
			return  pad(f.getDate(),2)+separator+pad((f.getMonth()+1),2)+separator+f.getFullYear();
                    	break;
		case 'DDMMYY':
			return  pad(f.getDate(),2)+separator+pad((f.getMonth() +1),2)+separator+f.getFullYear().toString().substr(2,2);
                    	break;
   		default: 
			return f;
	}
}

function pad(n, width, z) {
  z = z || '0';
  n = n + '';
  return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}


