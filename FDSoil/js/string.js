/** Convierte una cadena de caracteres con la primera letra de cada 
* palabra en mayúscula y todas las demás letras en minúscula.
* Este método implementa la función auxiliar firstUpperOtherLowerAux().
* @param cadena (string) Cadena de caracteres para convertir.
* @return resp (string) Cadena de caracteres con la primera letra de cada 
* palabra en mayúscula y todas las demás letras en minúscula.*/
function firstUpperOtherLower(cadena){
   var resp='';
   var arreglo=cadena.split(' ');
   for(var i in arreglo){
       resp+=firstUpperOtherLowerAux(arreglo[i])+' ';
   }
   
   return resp.substring(0,resp.length-1);
}

/** Función auxiliar implementada por firstUpperOtherLower(). 
* Esta función recibe por parámetro palabra por palabra respectivamente de la cadena principal.
* @param cadena (string) Cadena de caracteres que representa una palabra.
* @return firstUpper+otherLower (string) Cadena de caracteres que representa una palabra 
* donde la primera letra es mayúscula y todas las demás letras en minúscula.*/
function firstUpperOtherLowerAux(cadena){
    var firstUpper=cadena.substring(0,1).toUpperCase();
    var otherLower=cadena.substring(1,cadena.length).toLowerCase();
    return firstUpper+otherLower;
}

/** Crea una cadena de caracteres aleatoriamente con letras mayúsculas, minúsculas y números.
* @param numLeng (integer) Representa el largo que debe tener la cadena resultante.
* @return text (string) Cadena de caracteres aleatoria con letras mayúsculas, minúsculas y números.*/
function createRandomStr(numLeng){
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for( var i=0; i < numLeng; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
} 

/** Elimina todos los espacios en blanco de una cadena de caracteres.
* @param string (string) Cadena de caracteres.
* @return string (string) Cadena de caracteres sin espacios en blanco.*/
function removeSpaces(string){
    return string.split(' ').join('');
}



String.prototype.removeAccents = function removeAccents()
{
	var __r = 
	{
		'À':'A','Á':'A','Â':'A','Ã':'A','Ä':'A','Å':'A','Æ':'E',
		'È':'E','É':'E','Ê':'E','Ë':'E',
		'Ì':'I','Í':'I','Î':'I',
		'Ò':'O','Ó':'O','Ô':'O','Ö':'O',
		'Ù':'U','Ú':'U','Û':'U','Ü':'U',
		'Ñ':'N'
	};
	
	return this.replace(/[ÀÁÂÃÄÅÆÈÉÊËÌÍÎÒÓÔÖÙÚÛÜÑ]/gi, function(m)
	{
		var ret = __r[m.toUpperCase()];

		if (m === m.toLowerCase())
			ret = ret.toLowerCase();

		return ret;
	});
};

