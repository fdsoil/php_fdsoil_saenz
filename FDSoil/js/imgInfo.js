function imgInfo( idDad, sSrcImg, timeSleep, nObjWidth, nObjHeight ){
    this.removeWindows = function (timeSleep){ 
        hide('id_div_img','explode',timeSleep);
        hide('id_div_background','clip',timeSleep);
        document.getElementById('id_div_group').parentNode.removeChild(document.getElementById('id_div_group'));    
    }
    var objDivGroup=document.createElement('div');
    objDivGroup.id='id_div_group';
    var objDivBackground=document.createElement('div');
    objDivBackground.id='id_div_background';
    objDivBackground.setAttribute('class','fondo_opaco_information');
    objDivBackground.setAttribute('style','display: none');   
    objDivGroup.appendChild(objDivBackground);
    var objImg = document.createElement('img');
    objImg.setAttribute("src", sSrcImg);
    objImg.setAttribute('onclick','removeWindows('+timeSleep+');');
    nObjWidth  = objImg.width  === 0 ? nObjWidth  : objImg.width ;
    nObjHeight = objImg.height === 0 ? nObjHeight : objImg.height;
    var wW = window.innerWidth  || document.documentElement.clientWidth  || document.body.clientWidth;
    var hH = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
    var w = (wW-nObjWidth) / 2;
    var h = (hH-nObjHeight)/ 3;
    var objDivImg=document.createElement('div');
    objDivImg.id='id_div_img';
    objDivImg.setAttribute('class','ventana_modal_information');
    objDivImg.setAttribute('style','display: none; left:'+w+';top:'+h);
    objDivImg.appendChild(objImg); 
    objDivGroup.appendChild(objDivImg); 
    var objDad=document.getElementById(idDad);
    objDad.appendChild(objDivGroup);
    show('id_div_img','explode',timeSleep);
    show('id_div_background','clip',timeSleep);
}



