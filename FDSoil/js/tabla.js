/** Elimina 'todas' las filas de una tabla.
* Generalmente la tabla debe quedar mínimo con una o dos fila, 
* la(s) cual(es) representa(n) el encabezado (th).
* Para indicar este mínimo se implementa el parametro 'limit'.
* @param idTable (string) Identificador de la tabla a la cual se eliminarán las filas.
* @param limit (integer) Número de filas mínimas (encabezado) con que quedará la tabla.*/
function deleteAllRowsTable(idTable,limit){
    for(var i = document.getElementById(idTable).rows.length; i > limit;i--){
    document.getElementById(idTable).deleteRow(i -1);
    }
}

/** Convierte el contenido (filas y columnas) de un elemento (table) en un arreglo bidimensional (matriz).
* @param idTable (string) Identificador de la tabla la cual se le aplicará la conversión.
* @param isThereId (boolean) Indicativo que expresa si las filas tienen identificador.
* @return matrix (array) Arreglo bidimensional con el contenido de la tabla.*/
function tableToMatrix(idTabla,isThereId){    
    var objTable= document.getElementById(idTabla);
    var matrix=new Array();
    for (rowIndex=0; rowIndex<objTable.rows.length;rowIndex++){
        matrix[rowIndex]=new Array();
        var row=objTable.rows[rowIndex];
        var cells=row.cells;
        for (cellIndex=0;cellIndex<cells.length;cellIndex++){ 
            var startColNum=0; 
            if (isThereId){
                 matrix[rowIndex][0]=row.id;
                var startColNum=1;
            }
            matrix[rowIndex][cellIndex+startColNum]=cells[cellIndex].innerHTML;            
        }        
   }    
   return matrix;
}

/** Elimar una fila (elemento TR) de una tabla (elemento TABLE).
* @param idTable (string) Identificador de la tabla a la cual se le borrará una fila.
* @param idRow (string) Identificador de la fila que se borrará de la tabla.*/
function delRow(idTable,idRow){
	document.getElementById(idTable).deleteRow(idRow.parentNode.parentNode.rowIndex);
}

/** Convierte una cadena de carácteres tipo arreglo en la creación de un elemento TABLE. 
* Una cadena de carácteres tipo arreglo es separada por carácteres especiales 
* que indican y delimitan las filas y las columnas. 
* El número de columnas de las filas debe ser constante.
* Este método implementa la función auxiliar srtArrayToTableAux();
* @param idTabla (string) Identificador del elemento TABLE que se creará.
* @param strArray (string) Cadena de carácteres tipo arreglo.
* @param separate1 (string) Caracter especial que indica y delimita las columnas.
* @param separate2 (string) Caracter especial que indica y delimita las filas.
* @return objTable (object) Elemento TABLE con los datos resultantes de la cadena de carácteres tipo arreglo.*/
function srtArrayToTable(idTabla,strArray,separate1,separate2){        
    var rowArray=strArray.split(separate2);
    var objTable=document.createElement('table');
    objTable.setAttribute('id',idTabla);
    objTable.appendChild(srtArrayToTableAux(rowArray[0].split(separate1),'th'));
    for (i=1; i < rowArray.length; i++)
        objTable.appendChild(srtArrayToTableAux(rowArray[i].split(separate1),'td'));    
    return objTable;
}

/** Función auxiliar que convierte una cadena de carácteres tipo arreglo la creación de un elemento TABLE.
* @param cellArray (array) Arreglo unidimensional que contiene los valores de las columna.
* @param cellType (string) Indica uno de los dos tipos de elementos que definen una fila (TH o TD).
* @return tr (object) Elemento TR.*/
function srtArrayToTableAux(cellArray,cellType){
    var tr=document.createElement('tr');
    var cell=new Array();
    var node=new Array();
    for (var i=0;i<cellArray.length;i++){
        cell[i]=document.createElement(cellType);
        node[i]=document.createTextNode(cellArray[i]);
        cell[i].appendChild(node[i]);
        tr.appendChild(cell[i]);
    }
    return tr;
}

/** Pinta (maquilla) las filas (elemento TR) de un elemento TABLE,
* unas claras y otras oscuras de forma intermitente.
* Esta función necesita definada las clases css: losnone y lospare.
* @param idTable (string) Identificador del elemento TABLE que será maquillado.*/
function paintTRsClearDark(idTable) {
        var objTable= document.getElementById(idTable);
  	if (objTable.rows.length == 1)
            return false;
	for (rowIndex = 1; rowIndex < objTable.rows.length; rowIndex++) 
            objTable.rows[rowIndex].className=((rowIndex% 2) == 0)?'losnone':'lospare';              
}

/** Pinta (maquilla) las filas (elemento TR) de un elemento TABLE que implementa jQueryTable,
* unas claras y otras oscuras de forma intermitente.
* Esta función necesita definada las clases css: even y odd.
* @param idTable (string) Identificador del elemento TABLE que será maquillado.*/
function paintTRsClearDarkJQuery(idTable) {
        var objTable= document.getElementById(idTable);
  	if (objTable.rows.length == 1)
            return false;
	for (rowIndex = 1; rowIndex < objTable.rows.length; rowIndex++) 
            objTable.rows[rowIndex].className=((rowIndex% 2) == 0)?'even':'odd';              
}

/** Asigna un valor a un atributo de una fila (elemento TR) de una tabla (elemento TABLE).
* @param idTable (string) Identificador del elemento TABLE.
* @param trIndex (integer) Número que representa el índice del elemento TR al cual se le asignará el atributo.
* @param attribute (string) Nombre del atributo que se asignará al elemento TR.
* @param value (string) Valor que se le asignará al atributo especificado.*/
function setAttributeTR(idTable,trIndex,attribute,value){
    var objTable=document.getElementById(idTable);  
    for (rowIndex=1;rowIndex<objTable.rows.length;rowIndex++) 
        if (rowIndex==trIndex)
            objTable.rows[rowIndex].setAttribute(attribute,value);                     
}

/** Asigna un valor a un atributo de varias columnas (elemento TD),
* de varias filas (elemento TR), de una tabla (elemento TABLE), respectivamente.
* @param idTable (string) Identificador del elemento TABLE.
* @param attribute (string) Nombre del atributo que se asignará.
* @param value (string) Valor que se le asignará al atributo especificado.
* @param fromTD (integer) Número que indica desde que índice de columnas (elemento TD) se asignará el atributo.
* @param toTD (integer) Número que indica hasta que índice de columnas (elemento TD) se asignará el atributo.
* @param fromTR (integer) Número que indica desde que índice de filas (elemento TR) se asignará el atributo.
* @param toTR (integer) Número que indica hasta que índice de filas (elemento TR) se asignará el atributo.*/
function setAttributeTD(idTable,attribute,value, fromTD, toTD, fromTR, toTR){
    var objTable=document.getElementById(idTable);
    for (rowIndex=0; rowIndex<objTable.rows.length;rowIndex++){
        var row=objTable.getElementsByTagName('tr')[rowIndex];
        var cells=row.getElementsByTagName('td');
        for (cellIndex=0;cellIndex<cells.length;cellIndex++) 
            if (cellIndex>=fromTD && cellIndex<=toTD && rowIndex>=fromTR && rowIndex<=toTR ) 
                cells[cellIndex].setAttribute(attribute,value);
    }
}

/** Coloca un formato de número entero a varias columnas (elemento TD),
* de varias filas (elemento TR), de una tabla (elemento TABLE), respectivamente.
* @param idTable (string) Identificador del elemento TABLE.
* @param fromTD (integer) Número que indica desde que índice de columnas (elemento TD) se colocará el formato.
* @param toTD (integer) Número que indica hasta que índice de columnas (elemento TD) se colocará el formato.
* @param fromTR (integer) Número que indica desde que índice de filas (elemento TR) se colocará el formato.
* @param toTR (integer) Número que indica hasta que índice de filas (elemento TR) se colocará el formato.*/
function putFormatTD(idTable, fromTD, toTD, fromTR, toTR){
    var objTable=document.getElementById(idTable);
    for (rowIndex = 0; rowIndex < objTable.rows.length; rowIndex++){
        var row = objTable.getElementsByTagName('tr')[rowIndex];
        var cells = row.getElementsByTagName('td');
        for (cellIndex = 0; cellIndex < cells.length; cellIndex++) 
            if (cellIndex>=fromTD && cellIndex<=toTD && rowIndex>=fromTR && rowIndex<=toTR ) 
                cells[cellIndex].innerHTML=putFormat(parseInt(cells[cellIndex].innerHTML));                                          
    }
}

/** Coloca un formato de número flotante a varias columnas (elemento TD),
* de varias filas (elemento TR), de una tabla (elemento TABLE), respectivamente.
* @param idTable (string) Identificador del elemento TABLE.
* @param fromTD (integer) Número que indica desde que índice de columnas (elemento TD) se colocará el formato.
* @param toTD (integer) Número que indica hasta que índice de columnas (elemento TD) se colocará el formato.
* @param fromTR (integer) Número que indica desde que índice de filas (elemento TR) se colocará el formato.
* @param toTR (integer) Número que indica hasta que índice de filas (elemento TR) se colocará el formato.*/
function putFormatFloatTD(idTable, fromTD, toTD, fromTR, toTR){
    var objTable=document.getElementById(idTable);
    for (rowIndex = 0; rowIndex < objTable.rows.length; rowIndex++){
        var row = objTable.getElementsByTagName('tr')[rowIndex];
        var cells = row.getElementsByTagName('td');
        for (cellIndex = 0; cellIndex < cells.length; cellIndex++) 
            if (cellIndex>=fromTD && cellIndex<=toTD && rowIndex>=fromTR && rowIndex<=toTR ) 
                cells[cellIndex].innerHTML=putFloatFormat(parseFloat(cells[cellIndex].innerHTML),'.',',',2);                                          
    }
}

/** Elimar una fila (elemento TR) de una tabla (elemento TABLE) que implementa jQueryTable.
* @param idRow (string) Identificador de la fila que se borrará de la tabla.
* @param objTable (object) Elemento TABLE a la cual se le borrará una fila.*/
function tableDelRowJQry(idRow, objTable){
	objTable.deleteRow(idRow.parentNode.parentNode.rowIndex);
}

/** Refresca y activa un elemento TABLE que implementa jQueryTable.
* @param idTabla (string) Identificador del elemento TABLE que implementa jQueryTable.*/
function jQryTableRefresh(idTabla){    

    if (document.getElementById(idTabla+'_length')){   
        var obj1 = document.getElementById(idTabla+'_length');
        var padre1 = obj1.parentNode;
        padre1.removeChild(obj1);
    }        
    if (document.getElementById(idTabla+'_filter')){   
        var obj2 = document.getElementById(idTabla+'_filter');
        var padre2 = obj2.parentNode;
        padre2.removeChild(obj2);
    }      
    if (document.getElementById(idTabla+'_info')){
        var obj3 = document.getElementById(idTabla+'_info');
        var padre3 = obj3.parentNode;
        padre3.removeChild(obj3);
    }      
    if (document.getElementById(idTabla+'_paginate')){
        var obj4 = document.getElementById(idTabla+'_paginate');
        var padre4 = obj4.parentNode;
        padre4.removeChild(obj4);
    }    
    $(document).ready(function(){$('#'+idTabla).dataTable();});  
}

/** Valida que un elemento TABLE, tenga como mínimo el número de filas (elemento TR) definido en el parámetro minRow.
* Esta función necesita de otra que recorra la tabla y llene el valor de un elemento INPUT tipo hidden 
* con el identificador especificado en el parámetro 'id_campo'.  
* Esta función quedó obsoleta y remplazada por valTable() la cual esta en el archivo <<validateData.js>>.
* @param id_campo (string) Identificador del elemento INPUT tipo hidden donde se vaciará el barrido del elemento TABLE.
* @param id_mensaje (string) Identificador del elemento donde se mostrará el mensaje de validación.
* @param mensaje (string) Este parámetro contendrá el mensaje de validación que se mostrará al usuario.
* @param minRow (integer) Número que representa el mínimo de filas (elemento TR) que debe tener el elemento TABLE.
* @return (boolean) Devuelve FALSE si el número de filas es menor al número pasado por el parámetro minRow,
* de lo contrario devuelve TRUE.*/
function validateTable(id_campo,id_mensaje,mensaje, minRow){
    obj=document.getElementById(id_campo);
    objmsj=document.getElementById(id_mensaje);
    var arreglo=obj.value.split(',');
    if(arreglo.length < minRow){
        objmsj.innerHTML = ''+mensaje;
        return false;
    }
    else{
        objmsj.innerHTML = '';
        return true;
    }
}

/** Convierte una matriz (arreglo bidimensional) en la creación de un elemento TABLE .
* Este método implementa la función auxiliar matrixToTableAux().
* @param idTabla (string) Identificador del elemento TABLE que será creado.
* @param matrix (array) Matriz (arreglo bidimensional) que alimentará el elemento TABLE que será creado.
* @param isThereId (boolean) Valor lógico que indica si la fila tendrá identificador.
* @return objTable (object) Elemento TABLE creado a partir de una matriz (arreglo bidimensional).*/
function matrixToTable(idTabla,matrix,isThereId){ 
    var objTable=document.createElement('table');
    objTable.id=idTabla;
    objTable.appendChild(matrixToTableAux(matrix[0],'th',isThereId));
    for (i=1; i < matrix.length; i++)
        objTable.appendChild(matrixToTableAux(matrix[i],'td',isThereId));    
    return objTable;
}

/** Función auxiliar implementada por matrixToTable() 
* para convertir una matriz (arreglo bidimensional) en la creación de un elemento TABLE.
* @param cellArray (array) Arreglo unidimensional que contiene los valores de las columna.
* @param cellType (string) Indica uno de los dos tipos de elementos que definen una fila (TH o TD).
* @param isThereId (boolean) Valor lógico que indica si la fila tendrá identificador.
* @return tr (object) Elemento TR.*/
function matrixToTableAux(cellArray,cellType,isThereId){
    var tr=document.createElement('tr');
    var cell=new Array();
    var node=new Array();
    for (var i=0;i<cellArray.length;i++){
        if (isThereId && i==0) {
            tr.id=cellArray[i++];
        }
        cell[i]=document.createElement(cellType);
        node[i]=document.createTextNode(cellArray[i]);
        cell[i].appendChild(node[i]);
        tr.appendChild(cell[i]);       
    }
    return tr;
}

/** Agrega una fila (elemento TR) a un elemento TABLE. Este método implementa la función auxiliar addRowToTableAux().
* @param idTabla (string) Identificador del elemento TABLE.
* @param row (array) Arreglo unidimensional que contiene los valores de las columnas de la fila.
* @param isThereId (boolean) Indica si la fila tiene identificador.*/
function addRowToTable(idTabla,row,isThereId){ 
    document.getElementById(idTabla).appendChild(addRowToTableAux(row,isThereId));    
}

/** Función auxiliar implementada por addRowToTable().
* @param row (array) Arreglo unidimensional que contiene los valores de las columnas de la fila.
* @param isThereId (boolean) Indica si la fila tiene identificador.
* @return tr (object) Elemento TR.*/
function addRowToTableAux(row,isThereId){
    var tr=document.createElement('tr');
    var cell=new Array();
    var node=new Array();
    for (var i=0;i<row.length;i++){
        if (isThereId && i==0) 
            tr.id=row[i++];        
        cell[i]=document.createElement('td');
        node[i]=document.createTextNode(row[i]);
        cell[i].appendChild(node[i]);
        tr.appendChild(cell[i]);       
    }
    return tr;
}

/** Convierte una matriz (arreglo bidimensional) en un elemento TABLE existente en el DOM.
* Este método implementa la función auxiliar matrixToExitTablaAux().
* @param idTabla (string) Identificador del elemento TABLE existente.
* @param matrix (array) Matriz (arreglo bidimensional) que alimentará el elemento TABLE existente.
* @param isThereId (boolean) Valor lógico que indica si la fila tendrá identificador.*/
function matrixToExitTabla(idTabla,matrix,isThereId){ 
    var objTable=document.getElementById(idTabla);
    //objTable.appendChild(matrixToTableAux(matrix[0],'th',isThereId));
    for (i=0; i < matrix.length; i++)
        objTable.tBodies[0].appendChild(matrixToExitTablaAux(matrix[i],'td',isThereId));    
   // return objTable;
}

/** Función auxiliar implementada por matrixToExitTabla() 
* para convertir una matriz (arreglo bidimensional) en un elemento TABLE existente en el DOM.
* @param cellArray (array) Arreglo unidimensional que contiene los valores de las columna.
* @param cellType (string) Indica uno de los dos tipos de elementos que definen una fila (TH o TD).
* @param isThereId (boolean) Valor lógico que indica si la fila tendrá identificador.*/
function matrixToExitTablaAux(cellArray,cellType,isThereId){
    var tr=document.createElement('tr');
    var cell=new Array();
    var node=new Array();
    for (var i=0;i<cellArray.length;i++){
        if (isThereId && i==0) {
            tr.id=cellArray[i++];
        }
        cell[i]=document.createElement(cellType);
        node[i]=document.createTextNode(cellArray[i]);
        cell[i].appendChild(node[i]);
        tr.appendChild(cell[i]);       
    }
    return tr;
}

/** Convierte una cadena de carácteres tipo arreglo bidimensional (matriz) en elemento TABLE existente. 
* Una cadena de carácteres tipo arreglo es separada por carácteres especiales 
* que indican y delimitan las filas y las columnas. 
* El número de columnas de las filas debe ser constante.
* Este método implementa la función auxiliar strMatrixToExistTableAux();
* @param idTabla (string) Identificador del elemento TABLE que se creará.
* @param strMatrix (string) Cadena de carácteres tipo arreglo bidimensional (matriz).
* @param separate1 (string) Caracter especial que indica y delimita las columnas.
* @param separate2 (string) Caracter especial que indica y delimita las filas.
* @return objTable (object) Elemento TABLE con los datos resultantes de la cadena 
* de carácteres tipo arreglo bidimensional (matriz).*/
function strMatrixToExistTable(idTabla,strMatrix,separate1,separate2,thereIsTh){
    var rowArray=strMatrix.split(separate2);
    var objTable=document.getElementById(idTabla);
    var num=0;
    if (thereIsTh)
        objTable.appendChild(strMatrixToExistTableAux(rowArray[num++].split(separate1),'th'));
    
    for (i=num; i < rowArray.length; i++)
        objTable.appendChild(strMatrixToExistTableAux(rowArray[i].split(separate1),'td'));    
    return objTable;
}

/** Función auxiliar que convierte una cadena de carácteres tipo arreglo bidimensional (matriz) 
* en un elemento TABLE existente.
* @param cellArray (array) Arreglo unidimensional que contiene los valores de las columna.
* @param cellType (string) Indica uno de los dos tipos de elementos que definen una fila (TH o TD).
* @return tr (object) Elemento TR.*/
function strMatrixToExistTableAux(cellArray,cellType){
    var tr=document.createElement('tr');
    var cell=new Array();
    var node=new Array();
    for (var i=0;i<cellArray.length;i++){
        cell[i]=document.createElement(cellType);
        node[i]=document.createTextNode(cellArray[i]);
        cell[i].appendChild(node[i]);
        tr.appendChild(cell[i]);
    }
    return tr;
}

/** Obtener el índice de una fila (elemento TR) de una tabla (elemento TABLE).
* @param idTabla (string) Identificador de la tabla (elemento TABLE).
* @param idTr (string) Identificador de la fila (elemento TR).
* @return indice (integer) Índice de la fila (elemento TR) especificada.*/
function getIndexOfRecordInTable(idTabla, idTr){
    var indice = null;
    var objTable=document.getElementById(idTabla);
    for (rowIndex = 0; rowIndex < objTable.rows.length; rowIndex++){
        if (objTable.rows[rowIndex].id==idTr){
            indice=objTable.rows[rowIndex].rowIndex;            
        }
    }
    return indice;
}

function thereIsOnlyOneMainRecord(sIdTable){

    var oTab = document.getElementById(sIdTable);
    var nTot = 0;

        for (var i = 0; i < oTab.rows.length; i++) 
            if (oTab.rows[i].cells[0].id == 1)
                nTot++;
  
    return nTot;

}

function valOnlyOneMainRecord(valor, idEdit, oTbl, oSel, sMainRecord){  

    var nLen = oTbl.rows.length;
    var bItHas = false;
    var sMsg = '';
    var idTr = '';   
    
    for (var i=0; i < nLen; i++) 
        if (oTbl.rows[i].cells[0].id==1){
            bItHas = true;
	    idTr=oTbl.rows[i].id;
	}
    	
    if (valor != 1 && !bItHas) { 
        oSel.value=1;
        sMsg='Debe Registrar Primero Un '+sMainRecord;
    }else if (valor == 1 && bItHas && idEdit != idTr) {    
        oSel.value=0;
        sMsg='Debe Poseer Solo Un '+sMainRecord;   
    } else if (valor != 1 && bItHas && idEdit == idTr) {
	oSel.value=1;
        sMsg='Debe Poseer Obligatoriamente Un '+sMainRecord;
    }

    if (sMsg!='') 
        alert(sMsg, 'Disculpe...');
    
}

function tableFill(idTabla,strArray,separate1,separate2){      
    var rowArray=strArray.split(separate2);
    var objTable=document.getElementById(idTabla);        
    for (i=0; i < rowArray.length; i++)
        objTable.appendChild(tableFillAux(rowArray[i].split(separate1),'td'));    
    return objTable;
}

function tableFillAux(cellArray,cellType){
    var tr=document.createElement('tr');
    var cell=new Array();
    var node=new Array();
    for (var i=0;i<cellArray.length;i++){
        cell[i]=document.createElement(cellType);
        node[i]=document.createTextNode(cellArray[i]);
        cell[i].appendChild(node[i]);
        tr.appendChild(cell[i]);
    }
    return tr;
}

function tableFillJSON(idTabla,registro){     
    var objTable=document.getElementById(idTabla); 
    var tr=new Array();
    var cell=new Array();
    var node=new Array();
    for (i=0; i < registro.length; i++){
        tr[i]=document.createElement('tr');
        for (var campo in registro[i]){
            cell[campo]=document.createElement('td');
            node[campo]=document.createTextNode(registro[i][campo]);
            cell[campo].appendChild(node[campo]);
            tr[i].appendChild(cell[campo]);
            objTable.tBodies[0].appendChild(tr[i]);
        }
    }
}

function fillTableThatExists(idTabla,strArray,separate1,separate2){      
    var rowArray=strArray.split(separate2);
    var objTable=document.getElementById(idTabla);        
    for (i=0; i < rowArray.length; i++)
        objTable.appendChild(fillTableThatExistsAux(rowArray[i].split(separate1)));    
    return objTable;
}

function fillTableThatExistsAux(cellArray){
    var tr=document.createElement('tr');
    var cell=new Array();
    var node=new Array();
    for (var i=0;i<cellArray.length;i++){
        cell[i]=document.createElement('td');
        node[i]=document.createTextNode(cellArray[i]);
        cell[i].appendChild(node[i]);
        tr.appendChild(cell[i]);
    }
    return tr;
}

function fillTableThatExistsJSON(idTabla,registro){     
    var objTable=document.getElementById(idTabla); 
    var tr=new Array();
    var cell=new Array();
    var node=new Array();
    for (i=0; i < registro.length; i++){
        tr[i]=document.createElement('tr');
        for (var campo in registro[i]){
            cell[campo]=document.createElement('td');
            node[campo]=document.createTextNode(registro[i][campo]);
            cell[campo].appendChild(node[campo]);
            tr[i].appendChild(cell[campo]);
            objTable.tBodies[0].appendChild(tr[i]);
        }
    }
}


/*
function srtArrayToTabla(idTabla,strArray,separate1,separate2, isThereId, isThereTBody){       
    var rowArray=strArray.split(separate2);
    var objTable=document.getElementById(idTabla).tBodies[0];
    for (i=0; i < rowArray.length; i++)//i=1
        objTable.appendChild(srtArrayToTablaAux(rowArray[i].split(separate1),'td', isThereId));    
}

function srtArrayToTablaAux(cellArray,cellType, isThereId){
    var tr=document.createElement('tr');
    var cell=new Array();
    var node=new Array();
    var j=0;
    if (isThereId)
	tr.id=cellArray[j++];
    for (var i=j;i<(cellArray.length-1);i++){
        cell[i]=document.createElement(cellType);
        node[i]=document.createTextNode(cellArray[i]);
        cell[i].appendChild(node[i]);
        tr.appendChild(cell[i]);
    }
	tr.setAttribute('title',cellArray[i]);
    return tr;
}*/

/*
function matrixToExitTable(idTabla,matrix,isThereId){ 
    var objTable=document.getElementBy('table');
    objTable.id=idTabla;
    objTable.appendChild(matrixToTableAux(matrix[0],'th',isThereId));
    for (i=1; i < matrix.length; i++)
        objTable.appendChild(matrixToTableAux(matrix[i],'td',isThereId));    
    return objTable;
}

function matrixToExitTableAux(cellArray,cellType,isThereId){
    var tr=document.createElement('tr');
    var cell=new Array();
    var node=new Array();
    for (var i=0;i<cellArray.length;i++){
        if (isThereId && i==0) {
            tr.id=cellArray[i++];
        }
        cell[i]=document.createElement(cellType);
        node[i]=document.createTextNode(cellArray[i]);
        cell[i].appendChild(node[i]);
        tr.appendChild(cell[i]);       
    }
    return tr;
}*/

