/** Tabula una pestaña en el solapador.
* Aquí se solapan elementos DIV. Dichos elementos se identifican con el prefijo 'div' más el número de posición
* que respectivamente conforman el arreglo unidimencional de elementos DIV. Para enumerar los elementos DIV se
* debe comenzar desde cero (0) en adelante. Ej: div0, div1, div2, div3, div4... 
* Respectivamente se deben identificar las pestañas que ejecutan la acción 
* y las imagenes que especifican la pestaña activa. Ej: Tab0, Tab1, Tab2, Tab3... y chk0, chk1, chk2... 
* Ver también: {@link seguirTab}, {@link regresarTab} y {@link ocultar_mostrar_objeto}. 
* @param nTab integer Número identificador de la pestaña que se quiere que esté visble. 
* @param tTab integer Tamaño del arreglo de elementos DIV.*/
function Tab(nTab,tTab){
    var tabEndLabel='t';
    var nTabLast_T=0;
	for (var i=0;i<tTab;i++){
			//document.getElementById('chk'+i).style.visibility = 'hidden';
		if (nTab==i){			
                        show('div'+i,250);//document.all('div'+i).style.display="";
			//document.getElementById('chk'+i).style.visibility = 'visible';	                        
			//document.getElementById('Tab'+i).style.color="#000000";
                        document.getElementById('Tab'+i).style.background="#A9F5F2";
		}
		else{
                        hide('div'+i,250);//document.all('div'+i).style.display="none";
			//document.getElementById('chk'+i).style.visibility = 'hidden';
                        document.getElementById('Tab'+i).style.background="#D8D8D8";
			//document.getElementById('Tab'+i).style.color="#6E6E6E";
                        //document.getElementById('Tab'+i).style.background="#fff";
		}
                tabEndLabel=document.getElementById('div'+i).getAttribute('label');
                if (document.getElementById('div'+i).getAttribute('label')=='t')
                    nTabLast_T=i;
	}	
        
	if (nTab==0){
		document.getElementById('id_regresar').className='desHabilitarBoton';
		document.getElementById('id_seguir').className='HabilitarBoton';
	}
	else if(nTab==tTab-1 || (tabEndLabel=='f' && nTab==nTabLast_T)){
		document.getElementById('id_regresar').className = 'HabilitarBoton';
		document.getElementById('id_seguir').className = 'desHabilitarBoton';
	}
	else{
		document.getElementById('id_regresar').className = 'HabilitarBoton';
		document.getElementById('id_seguir').className = 'HabilitarBoton';
	}
}

/** Activa la siguiente pestaña del solapador.
* Ver también: {@link regresarTab}, {@link Tab} y {@link ocultar_mostrar_objeto}. 
* @param tTab integer Tamaño del arreglo de elementos DIV.*/
function segirTab(tTab){
	for (var i=0;i<tTab-1;i++){	
		if (document.all('div'+i).style.display!="none"){                
                        if (document.getElementById('div'+(i+1)).getAttribute('label')=="f"){                            
                            do {                                
                                i++;
                            }
                            while (document.getElementById('div'+(i+1)).getAttribute('label')=="f");
                        }
                        Tab(i+1,tTab);
			return;
		}
		
	}
}

/** Activa la anterior pestaña del solapador.
* Ver también: {@link segirTab}, {@link Tab} y {@link ocultar_mostrar_objeto}.
* @param tTab integer Tamaño del arreglo de elementos DIV.*/
function regresarTab(tTab){
	for (var i=1;i<=tTab-1;i++){
		if (document.all('div'+i).style.display!="none"){
                        if (document.getElementById('div'+(i-1)).getAttribute('label')=="f"){                            
                            do {                                
                                i--;
                            }
                            while (document.getElementById('div'+(i-1)).getAttribute('label')=="f");
                        }         
			Tab(i-1,tTab);
			return;	
		}
		
	}
}

/** Muestra u oculta el objeto que especifica cual pestaña está activa en el solapador de elementos DIV.
* Ver también: {@link Tab}, {@link segirTab},  y {@link regresarTab}.
* @param id_nombre string Nombre del elemento IMG.*/
/*function ocultar_mostrar_objeto(id_nombre){//coloca los check a las pestañas
    var objeto = document.getElementById(id_nombre);
	if (objeto.style.visibility == 'visible'){
		objeto.style.visibility = 'hidden';
	}
	else{
		objeto.style.visibility = 'visible';
	}
}*/
