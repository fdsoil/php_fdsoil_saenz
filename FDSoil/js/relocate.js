/** Ejecuta un sumit a una página específica, pasando los valores suministrados bajo el método POST.
* @param page (string) Representa la ruta y el archivo de la página a donde se envía el sumit.
* @param params (array) Lista de valores que se enviarán por el método POST.*/
function relocate(page,params, blank){
    var body = document.body;
    form=document.createElement('form'); 
    form.method = 'POST'; 
    form.action = page;
    //if (blank=='_blank')
	//form.target='_blank';
    form.name = 'jsform';
    for (index in params){
        var input = document.createElement('input');
	input.type='hidden';
	input.name=index;
	input.id=index;
	//if (blank=='_blank')
        	//input.formtarget='_blank';
	input.value=params[index];
	form.appendChild(input);
    }	  		  			  
    body.appendChild(form);
    form.submit();
}

/** Ejecuta un sumit a una página específica en otra pestaña del navegador, 
* pasando los valores suministrados bajo el método POST.
* @param page (string) Representa la ruta y el archivo de la página a donde se envía el sumit.
* @param params (array) Lista de valores que se enviarán por el método POST.*/
function relocateBlank(page,params){
    var body = document.body;
    form=document.createElement('form'); 
    form.method = 'POST'; 
    form.action = page;
    //if (blank=='_blank')
    form.target='_blank';
    form.name = 'jsform';
    for (index in params){
        var input = document.createElement('input');
	input.type='hidden';
	input.name=index;
	input.id=index;
	//if (blank=='_blank')
        input.formtarget='_blank';
	input.value=params[index];
	form.appendChild(input);
    }	  		  			  
    body.appendChild(form);
    form.submit();
}

    /*function insertAfter(e,i){ 
        if(e.nextSibling){ 
            e.parentNode.insertBefore(i,e.nextSibling); 
        } else { 
            e.parentNode.appendChild(i); 
        }
    Los parámetros son:

    e: el nodo tras el que se quiere insertar otro.
    i: el nodo que se quiere insertar.

    }*/

