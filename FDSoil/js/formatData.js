function jQryFormatData(obj){
    $(obj).ready(function(){
        $("[data-format=lowercase]")
            .blur(function(){this.value = this.value.toLowerCase();})
            .css({"text-transform": "lowercase"});
        $("[data-format=uppercase]")
            .blur(function(){this.value = this.value.toUpperCase();})
            .css({"text-transform": "uppercase"});
        $("[data-format=integer]")                
            .keypress(function(event){return formato_numeric(this, '.', ',', event);})
            .css({"text-align":"right"});
        $("[data-format=float]")                
            .keypress(function(event){return formato_float(this, '.', ',', event);})
            .css({"text-align":"right"});
        $("[data-format=float3d]")                
            .keypress(function(event){return formato_float_3d(this, '.', ',', event);})
            .css({"text-align":"right"});     
    });	
}

/*
function jQryFormatData(obj){
    $(obj).ready(function(){
        $("[data-format=lowercase]")
            .keypress(function(event){return (this.value.length==0 && ((document.all)?event.keyCode:event.which)==32)?false:true;})               
            .keyup(function(event){this.value = this.value.toLowerCase();})
            .css({"text-transform": "lowercase"});
        $("[data-format=uppercase]")
            .keypress(function(event){return (this.value.length==0 && ((document.all)?event.keyCode:event.which)==32)?false:true;})
            .keyup(function(event){this.value = this.value.toUpperCase();})
            .css({"text-transform": "uppercase"});
        $("[data-format=integer]")                
            .keypress(function(event){return formato_numeric(this, '.', ',', event);})
            .css({"text-align":"right"});
        $("[data-format=float]")                
            .keypress(function(event){return formato_float(this, '.', ',', event);})
            .css({"text-align":"right"});
        $("[data-format=float3d]")                
            .keypress(function(event){return formato_float_3d(this, '.', ',', event);})
            .css({"text-align":"right"});     
    });	
}
*/
