var createObjInputChk = function (strName)
{
    var removeObj = () =>
    {
        var objTr = document.getElementById("tr_"+strName);
        objTr.parentNode.removeChild(objTr);
        var objSelelect=document.getElementById("id_select_campos");
        for (i=0; i<(objSelelect.length); i++){
            if (objSelelect[i].value==strName.toUpperCase())
                objSelelect[i].removeAttribute("disabled");
        }
    }
    obj = document.createElement('input');
    obj.type="checkbox"
    obj.id = "chk_"+strName;
    obj.checked="checked"
    obj.addEventListener('click', function() { removeObj(); });
    return obj;
}

var addTypeInputFilter = function (strName)
{
    var objTr = document.createElement('tr');
    objTr.id = "tr_"+strName;
    var arrTd = [];
    var arrInput = [];
    arrTd[0] = document.createElement('td');
    arrInput[0] = createObjInputChk(strName);
    arrTd[0].appendChild(arrInput[0]);
    objTr.appendChild(arrTd[0]);

    arrTd[1] = document.createElement('td');
    arrTd[1].appendChild(document.createTextNode(strLabel(strName)));
    objTr.appendChild(arrTd[1]);
    arrTd[2] = document.createElement('td');
    arrInput[1] = document.createElement('input');
    arrInput[1].id = "id_"+strName;
    arrInput[1].name = strName;
    arrTd[2].appendChild(arrInput[1]);
    objTr.appendChild(arrTd[2]);        
    document.getElementById('tab_mi_filtro_aux').appendChild(objTr);
}

var addTypeSelectFilter = function ( strName, arr )
{
    var objTr = document.createElement('tr');
    var arrTd = [];
    var oInput = [];
    arrTd[0] = document.createElement('td');
    arrInput[0] = createObjInputChk(strName);
    arrTd[0].appendChild(oInput[0]);
    objTr.appendChild(arrTd[0]);

    arrTd[1] = document.createElement('td');
    arrTd[1].appendChild(document.createTextNode(strLabel(strName)));
    objTr.appendChild(arrTd[1]);

    arrTd[2] = document.createElement('td');
    var oSelect = document.createElement('select');
    oSelect.id = "id_"+strName;
    oSelect.name = strName;

    var aOptions = [];
    var i;
    
    for (i=0; i<(arr.length); i++){
        aOptions[i]=document.createElement("option");	
        aOptions[i].value = arr[i][0];
        aOptions[i].text = arr[i][1];
        oSelect.appendChild(aOptions[i]);
    }

    arrTd[2].appendChild(oSelect);
    objTr.appendChild(arrTd[2]);
       
    document.getElementById('tab_mi_filtro_aux').appendChild(objTr);
}

var addTypeDateFilter = function (strName)
{
    var objTr = document.createElement('tr');
    objTr.id = "tr_"+strName;
    var arrTd = [];
    var arrInput = [];
    arrTd[0] = document.createElement('td');
    arrInput[0] = createObjInputChk(strName);
    arrTd[0].appendChild(arrInput[0]);
    objTr.appendChild(arrTd[0]);

    arrTd[1] = document.createElement('td');
    arrTd[1].appendChild(document.createTextNode(strLabel(strName+" Desde")));
    objTr.appendChild(arrTd[1]);
    arrTd[2] = document.createElement('td');
    arrInput[0] = document.createElement('input');
    arrInput[0].id = "id_" + strName + "_desde";
    arrInput[0].name = strName + "_desde";
    arrInput[0].setAttribute("readonly","readonly");
    arrTd[2].appendChild(arrInput[0]);
    var aScript = [];
    aScript[0] = document.createElement('script');
    aScript[0].innerHTML = "Calendar.setup("
                           + "{inputField : 'id_"+strName+"_desde'," 
                           + "ifFormat : '%d/%m/%Y', "
                           + "button : 'id_"+strName+"_desde', "
                           + "align : 'Tr'});";
    arrTd[2].appendChild(aScript[0]); 
    objTr.appendChild(arrTd[2]);
    arrTd[3] = document.createElement('td');
    arrTd[3].appendChild(document.createTextNode("Hasta")); 
    objTr.appendChild(arrTd[3]);   
    arrTd[4] = document.createElement('td');
    arrInput[1] = document.createElement('input');
    arrInput[1].id = "id_" + strName + "_hasta";
    arrInput[1].name = strName + "_hasta";
    arrInput[1].setAttribute("readonly","readonly");
    arrTd[4].appendChild(arrInput[1]);
    aScript[1] = document.createElement('script');
    aScript[1].innerHTML = "Calendar.setup("
                           + "{inputField : 'id_"+strName+"_hasta'," 
                           + "ifFormat : '%d/%m/%Y', "
                           + "button : 'id_"+strName+"_hasta', "
                           + "align : 'Tr'});";
    arrTd[4].appendChild(aScript[1]); 
    objTr.appendChild(arrTd[4]);    
    document.getElementById('tab_mi_filtro_aux').appendChild(objTr);
}

var addTypeMultipleSelectFilter = function ( strName, arr )
{
    var objTr = document.createElement('tr');
    objTr.id = "tr_"+strName;
    var arrTd = [];
    arrTd[0] = document.createElement('td');
    var arrInput = [];
    arrInput[0] = createObjInputChk(strName);
    arrTd[0].appendChild(arrInput[0]);
    objTr.appendChild(arrTd[0]);

    arrTd[1] = document.createElement('td');
    arrTd[1].appendChild(document.createTextNode(strLabel(strName)));
    objTr.appendChild(arrTd[1]);

    arrTd[2] = document.createElement('td');

    var arrSelect = [];
    arrSelect[0] = document.createElement('select');
    arrSelect[0].id = "id_"+strName+"_list";
    arrSelect[0].name = strName+"_list";
    arrSelect[0].multiple = "multiple";
    arrSelect[0].addEventListener('dblclick', function(){ 
        move('right', document.getElementById("id_"+strName+"_list"), document.getElementById("id_"+strName+"_selected"));
        getValues(document.getElementById("id_"+strName+"_selected"), document.getElementById("strArr_"+strName));
    });

    var aOptions = [];
    var i;
    
    for (i=0; i<(arr.length); i++){
        aOptions[i]=document.createElement("option");	
        aOptions[i].value = arr[i][0];
        aOptions[i].text = arr[i][1];
        arrSelect[0].appendChild(aOptions[i]);
    }

    arrTd[2].appendChild(arrSelect[0]);
    objTr.appendChild(arrTd[2]);

    arrTd[3] = document.createElement('td');
    arrInput[1] = document.createElement('input');
    arrInput[1].type="button"
    arrInput[1].value=">>"
    arrInput[1].setAttribute(
        "onclick", 
        "move('right', document.getElementById('id_"+strName+"_list'), document.getElementById('id_"+strName+"_selected'));"
        +"getValues(document.getElementById('id_"+strName+"_selected'), document.getElementById('strArr_"+strName+"'));" 
    );

    arrTd[3].appendChild(arrInput[1]);

    var oBr = document.createElement('br');
    arrTd[3].appendChild(oBr);

    arrInput[2] = document.createElement('input');
    arrInput[2].type="button"
    arrInput[2].value="<<"
    arrInput[2].setAttribute(
        "onclick", 
        "move('left', document.getElementById('id_"+strName+"_list'), document.getElementById('id_"+strName+"_selected'));" 
        +"getValues(document.getElementById('id_"+strName+"_selected'), document.getElementById('strArr_"+strName+"'));" 
    );

    arrTd[3].appendChild(arrInput[2]);

    objTr.appendChild(arrTd[3]);

    arrTd[4] = document.createElement('td');
    arrSelect[1] = document.createElement('select');
    arrSelect[1].id = "id_"+strName+"_selected";
    arrSelect[1].name = strName+"_selected";
    arrSelect[1].multiple = "multiple";
    arrSelect[1].addEventListener('dblclick', function(){ 
        move('left', document.getElementById("id_"+strName+"_list"), document.getElementById("id_"+strName+"_selected"));
        getValues(document.getElementById("id_"+strName+"_selected"), document.getElementById("strArr_"+strName));
    });
    arrTd[4].appendChild(arrSelect[1]);
    objTr.appendChild(arrTd[4]);

    arrInput[3] = document.createElement('input');
    arrInput[3].id = "strArr_"+strName;
    arrInput[3].name = "strArr_"+strName;
    arrInput[3].type = "hidden";
    arrInput[3].value = "";
    arrTd[4].appendChild(arrInput[3]);
    objTr.appendChild(arrTd[4]);
       
    document.getElementById('tab_mi_filtro_aux').appendChild(objTr);
}

var addTypeInputMultipleSelectFilter = function (strName)
{

    var insertOption = (elemento) =>
    {
        var x = document.getElementById("id_"+strName+"_selected");
        var option = document.createElement("option");
        option.value = elemento;
        option.text = elemento;
        x.add(option);
        initPanelCaja();
    }

    var deleteOption = () =>
    {
        var x = document.getElementById("id_"+strName+"_selected");
        x.remove(x.selectedIndex);
    }

    var initPanelCaja = () => { document.getElementById("id_"+strName).value = ''; }

    var objTr = document.createElement('tr');
    objTr.id = "tr_"+strName;
    var arrTd = [];
    var arrInput = [];
    arrTd[0] = document.createElement('td');
    arrInput[0] = createObjInputChk(strName);

    arrTd[0].appendChild(arrInput[0]);
    objTr.appendChild(arrTd[0]);

    arrTd[1] = document.createElement('td');
    arrTd[1].appendChild(document.createTextNode(strLabel(strName)));
    objTr.appendChild(arrTd[1]);
    arrTd[2] = document.createElement('td');

    var objDiv=document.createElement('div');
    objDiv.className="autocomplete";

    arrInput[1] = document.createElement('input');
    arrInput[1].id = "id_"+strName;
    arrInput[1].name = strName;
    objDiv.appendChild(arrInput[1]);
    arrTd[2].appendChild(objDiv);
    objTr.appendChild(arrTd[2]);

    arrTd[3] = document.createElement('td');
    arrInput[1] = document.createElement('input');
    arrInput[1].type="button"
    arrInput[1].value=">>"
    arrInput[1].addEventListener('click', function(){ 
        insertOption(document.getElementById("id_"+strName).value);
        getValues(document.getElementById("id_"+strName+"_selected"), document.getElementById("strArr_"+strName));
    });

    arrTd[3].appendChild(arrInput[1]);

    objTr.appendChild(arrTd[3]);

    arrTd[4] = document.createElement('td');
    var oSelect = document.createElement('select');
    oSelect.id = "id_"+strName+"_selected";
    oSelect.name = strName+"_selected";
    oSelect.multiple = "multiple";
    oSelect.size = 10;
    oSelect.addEventListener('dblclick', function(){ 
        deleteOption();
        getValues(document.getElementById("id_"+strName+"_selected"), document.getElementById("strArr_"+strName));
    });
    arrTd[4].appendChild(oSelect);
    objTr.appendChild(arrTd[4]);

    arrInput[3] = document.createElement('input');
    arrInput[3].id = "strArr_"+strName;
    arrInput[3].name = "strArr_"+strName;
    arrInput[3].type = "hidden";
    arrInput[3].value = "";
    arrTd[4].appendChild(arrInput[3]);
    objTr.appendChild(arrTd[4]);
    
    document.getElementById('tab_mi_filtro_aux').appendChild(objTr);
}

var addTypeInputRangeFilter = function (strName)
{
    var objTr = document.createElement('tr');
    objTr.id = "tr_"+strName;
    var arrTd = [];
    var arrInput = [];
    arrTd[0] = document.createElement('td');
    arrInput[0] = createObjInputChk(strName);
    arrTd[0].appendChild(arrInput[0]);
    objTr.appendChild(arrTd[0]);

    arrTd[1] = document.createElement('td');
    arrTd[1].appendChild(document.createTextNode(strLabel(strName+" Desde")));
    objTr.appendChild(arrTd[1]);
    arrTd[2] = document.createElement('td');

    var arrDiv=[];
    arrDiv[0]=document.createElement('div');
    arrDiv[0].className="autocomplete";

    arrInput[0] = document.createElement('input');
    arrInput[0].id = "id_" + strName + "_desde";
    arrInput[0].name = strName + "_desde";
    arrDiv[0].appendChild(arrInput[0]);
    arrTd[2].appendChild(arrDiv[0]);
    objTr.appendChild(arrTd[2]);

    arrTd[3] = document.createElement('td');
    arrTd[3].appendChild(document.createTextNode("Hasta")); 
    objTr.appendChild(arrTd[3]);   

    arrTd[4] = document.createElement('td');

    arrDiv[1]=document.createElement('div');
    arrDiv[1].className="autocomplete";

    arrInput[1] = document.createElement('input');
    arrInput[1].id = "id_" + strName + "_hasta";
    arrInput[1].name = strName + "_hasta";

    arrDiv[1].appendChild(arrInput[1]);
    arrTd[4].appendChild(arrDiv[1]);
   
    objTr.appendChild(arrTd[4]);    
    document.getElementById('tab_mi_filtro_aux').appendChild(objTr);
}

var strLabel = function (strOld)
{
    var arr = strOld.split("_");
    var strNew = '';
    var i;
    for ( i=0; i < arr.length; i++ )
        strNew += arr[i].substr(0,1).toUpperCase() + arr[i].substr(1, arr[i].length)+ " ";
    return strNew.substr(0, strNew.length-1);
}

