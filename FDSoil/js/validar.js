/** Variable global que cuenta el tiempo que dura el usuario sin tocar el teclado ni el mouse.*/
var timer;

/**Control de Tiempo de Sesión;
* controla el tiempo desde que el usuario activa la sesión y no toca el teclado ni el mouse.
* Si el usuario no toca el teclado ni el mouse durante un tiempo especificado en la función resetTimerSession(),
* entonces el sistema cierra la sesión automáticamente. Implementa la variable global 'timer'*/
function timeControlSession()
{
	document.body.setAttribute('onkeypress',"resetTimerSession();");
	document.body.setAttribute('onmousemove',"resetTimerSession();");
    resetTimerSession();
}

/** Resetea el tiempo de la variable global 'timer', cada vez que el usuario activa el mouse o toca el teclado;
* de lo contrario ejecuta la función logOut(); esta última función redirecciona la página al cierre forzado de sesión.*/
function resetTimerSession()
{
	clearTimeout(timer);
	timer=setTimeout(logOut, 900000); //3000 //1380000
}

/** Redirecciona la página al cierre forzado de sesión.*/
function logOut()
{
	location.href="../../../"+FDSoil.toLowerCase()+"/admin_session_closed/";
}

/**Control de Tiempo de Ventana;
* controla el tiempo desde que el usuario activa la ventana y no toca el teclado ni el mouse.
* Si el usuario no toca el teclado ni el mouse durante un tiempo especificado en la función resetTimerWindow(),
* entonces el sistema cierra la ventana en cuestión, automáticamente. Implementa la variable global 'timer'*/
function timeControlWindow()
{
	document.body.setAttribute('onkeypress',"resetTimerWindow();");
	document.body.setAttribute('onmousemove',"resetTimerWindow();");
    resetTimerWindow();
}

/** Resetea el tiempo de la variable global 'timer', cada vez que el usuario activa el mouse o toca el teclado;
* de lo contrario ejecuta la función windowClose(); esta última función cierra la ventana en cuestión.*/
function resetTimerWindow()
{
	clearTimeout(timer);
	timer=setTimeout(windowClose, 900000); //3000 //1380000
}

/** Cierra la ventana en cuestión.*/
function windowClose()
{
	javascript:window.close();
}

/** Valida el contenido del valor de un elemento INPUT tenga el formato de un correo electrónico;
* Esta función quedó obsoleta, es recomendable usar valEmail() del componente validateData.js.
* @param obj_origen (object) Elemento INPUT que contiene el valor para ser validado.
* @param id_destino (string) Identificador del elemento que mostrará el mensaje.
* @return (boolean) Devuelve TRUE si validacion es (ok), y FALSE en caso contrario.*/
function val_email(obj_origen,id_destino)
{
    //expresion regular
    var b=/^[^@\s]+@[^@\.\s]+(\.[^@\.\s]+)+$/;
    //comentar la siguiente linea si no se desea que aparezca el alert()//alert("Email " + (b.test(txt)?"":"no ") + "válido.");
    if (b.test(obj_origen.value)==false && obj_origen.value!=''){
        document.getElementById(id_destino).innerHTML='Formato del Correo Electrónico inválido.';
        obj_origen.focus();
    }else{
        document.getElementById(id_destino).innerHTML='';
    }
    return b.test(obj_origen.value);//devuelve verdadero si validacion OK, y falso en caso contrario
}

/** Valida que la tecla pulsada sea una letra del alfabeto, un espacio en blanco o ENTER.
* @param e (object) Evento de pulsar una tecla.
* @return (boolean) Devuelve TRUE si la tecla pulsada sea una letra del alfabeto, un espacio en blanco o ENTER;
* de lo contrario devuelve FALSE.*/
function val(e)
{
    tecla = (document.all) ? e.keyCode : e.which;
    if (tecla==32 || tecla==8) return true;
    patron =/[A-Za-z]/;
    te = String.fromCharCode(tecla);
    return patron.test(te);
}

/** Valida que cada tecla pulsada del teclado sea un valor permitido para una clave de usuario (password).
* @param e (object) Evento de pulsar una tecla.
* @return (boolean) Devuelve TRUE si la tecla pulsada cumple con las características de una clave de usuario (password),
* de lo contrario devuelve FALSE.*/
function valpassword(e)
{
    var tecla;
    tecla = (document.all) ? e.keyCode : e.which;
    if(tecla == 8 || e.which == 0)
    {
        return true;
    }
    var patron;
    patron = /[abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ0123456789|@#~$%()=^*+[]{}-_]/;
    //patron = /\d/; //solo acepta numeros, mejor dicho, no acepta digitos (letras)
    //patron = /[0-9,.]/; //solo acepta numeros, puntos y comas
    var te;
    te = String.fromCharCode(tecla);
    return patron.test(te);
}

/** Va mostrando un mensaje de cuantos caracteres restan por introducir en un valor de un elemento INPUT
* que tiene definido el atributo 'maxlength'.
* @param id_origen (string) Identificador del elemento INPUT que contiene el valor a ser evaluado.
* @param id_destino (string) Identificador del elemento (ej: DIV) que mostrará el mensaje.*/
function caracteres_restantes(id_origen,id_destino)
{
    var cantidad;
    var limite;
    var restante;
    obj_ori=document.getElementById(id_origen);
    obj_des=document.getElementById(id_destino);
    limite=obj_ori.getAttribute("maxlength");
    cantidad=(obj_ori.value.length);
    restante=limite-cantidad;
    obj_des.innerHTML='Caracteres Restantes: '+restante;
}

/** Valida que el valor de un elemento INPUT no esté vacio;
* Esta función quedó obsoleta, es recomendable usar el componente validateData.js.
* @param id_campo (string) Identificador del elemento INPUT que se evaluará su valor.
* @param id_div_msj (string) Idendificador del elemento DIV que mostrará el mensaje en el caso devolver FALSE.
* @param msj (string) Mensaje en caso de devolver FALSE.
* @return retorno (boolean) Devolverá TRUE si el valor del elemento INPUT no es vacio, de lo contrario devolverá FALSE.*/
function validar_campo(id_campo,id_div_msj,msj)
{
    var retorno = false;
    if (document.getElementById(id_campo).value != '')
        retorno = true;
    document.getElementById(id_div_msj).innerHTML=(retorno==false)?msj:'';
    return retorno;
}

/** Valida que un elemento INPUT tipo RADIO o CHECKBOX esté seleccionado;
* Esta función quedó obsoleta, es recomendable usar el componente validateData.js.
* @param array_id_element (array) Identificador del elemento INPUT tipo RADIO o CHECKBOX.
* @param id_div_msj (string) Idendificador del elemento DIV que mostrará el mensaje en el caso devolver FALSE.
* @param msj (string) Mensaje en caso de devolver FALSE.
* @return retorno (boolean) Devolverá TRUE si está seleccionado uno de sus índices, de lo contrario devolverá FALSE.*/
function val_checkbox(array_id_element,id_div_msj,msj)
{
    var retorno = false;
    var i=0;
    for(i=0; i< array_id_element.length;i++)
        if(document.getElementById(array_id_element[i]).checked)
            retorno = true;
    document.getElementById(id_div_msj).innerHTML =(retorno == false)?msj:'';
    return retorno;
}

/** Evalúa que la tecla presionada desde el teclado sea ENTER.
* @param e (object) Evento de pulsar una tecla.
* @return resp (boolean) Devuelve TRUE si la tecla presionada desde el teclado fué ENTER,
* de lo contrario devuelve FALSE.*/
function pressTheEnterKey(e)
{
    var resp=false;
    if(e!=null)
        if(((document.all)?e.keyCode: e.which)==13)
            resp=true;
    return resp;
}

/** Valida que la cadena de caractéres suministrada como parámetro
* sea un valor permitido para una clave de usuario (password).
* @param passWord (string) Cadena de caractéres a evaluar.
* @return respFinal (boolean) Devuelve TRUE si la cadena suministrada es un valor
* permitido para una clave de usuario (password), de lo contrario devuelve FALSE.*/
function validatePassWord(passWord)
{
 var acepNum=false;
 var acepCha=false;
 var acepUpp=false;
 var acepLow=false;
 var acepEsp=false;
 var acepLen=false;
 var respFinal=false;
 var obj=new appPasswrd();

    if (/[0-9]/.test(passWord))
        acepNum=true;
    if ((obj.bUpp && /[A-Z]/.test(passWord)) || (!obj.bUpp))
	acepUpp=true;
    if ((obj.bLow && /[a-z]/.test(passWord)) || (!obj.bLow))
        acepLow=true;
    if (/[|@#$%¿!¬()]/.test(passWord))
        acepEsp=true;
    if (passWord.length >= obj.rangeMin && passWord.length <= obj.rangeMax)
        acepLen=true;
    if (acepNum && acepUpp && acepLow && acepEsp && acepLen)
        respFinal=true;

    return respFinal;

}

/** Valida que la versión del navegador del cliente esté actualizada
* según las exigencias de la aplicación que se esté ejecutando.
* Entre los navegadores que evalua están: Firefox, Inernet Explorer, Opera y Chrome.
* @return resp (boolean) Devuelve TRUE si la versión del navegador del cliente está actualizada
* según las exigencias de la aplicación que se esté ejecutando, de lo contrario devuelve FALSE.*/
function validateTheBrowser()
{
   var resp=false;
   if (/Firefox[\/\s](\d+\.\d+)/.test(navigator.userAgent)){ //test for Firefox/x.x or Firefox x.x (ignoring remaining digits);
       var ffversion=new Number(RegExp.$1) //capture x.x portion and store as a number
        if (ffversion>=12) //12
            resp=true;
    }
    else if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)){ //test for MSIE x.x;
        var ieversion=new Number(RegExp.$1) // capture x.x portion and store as a number
        if (ieversion>=8) //8
            resp=true;
    } //Note: userAgent in Opera9.24 WinXP returns: Opera/9.24 (Windows NT 5.1; U; en) userAgent in Opera 8.5 (identified as IE) returns: Mozilla/4.0 (compatible; //MSIE 6.0; Windows NT 5.1) Opera 8.50 [en] //userAgent in Opera 8.5 (identified as Opera) returns: Opera/8.50 (Windows NT 5.1; U) [en]
    else if (/Opera[\/\s](\d+\.\d+)/.test(navigator.userAgent)){ //test for Opera/x.x or Opera x.x (ignoring remaining decimal places);
        var oprversion=new Number(RegExp.$1) // capture x.x portion and store as a number
        if (oprversion>=10) //10
            resp=true;
    }
    if (/Chrome[\/\s](\d+\.\d+)/.test(navigator.userAgent)){
         var cversion=new Number(RegExp.$1)
         if (cversion>=23) //25
             resp=true;
    }
    return resp;
}

/** Cambia el estatus del elemento IMG que oculta o muestra un panel de actualización de datos.
* Dicha alteración sucede cambiando el título y la clase (css).
* @param imgObjId (object) Elemento IMG que cambiará de estatus.*/
function changeTheObDeniedWritingImg(imgObjId)
{
    var objImg=document.getElementById(imgObjId);
    if (objImg.className=='img_show'){
        objImg.setAttribute('title','Ocultar Panel');
        objImg.setAttribute('class','img_hide');
    }
    else{
        objImg.setAttribute('title','Mostrar Panel');
        objImg.setAttribute('class','img_show');
    }
}

/** Valida que la tecla presionada sea número, letra mayúscula, un punto (.) o underscore (_) para llenar un campo login.
* Ver también: {@link acceptNum}, {@link acceptNumTelefono}, {@link IsNumeric}, {@link isInt}, {@link isInteger},
* {@link entero}, {@link isFloat} y {@link flotante}.
* @param e object Evento de presionar una tecla.
* @return boolean Devuelve TRUE si la tecla presionada sea número, letra mayúscula, un punto (.) o underscore (_);
* de lo contrario devuelve FALSE.*/
function acceptStrUsuario(e)
{
    var tecla;
    tecla = (document.all) ? e.keyCode : e.which;
    if(tecla == 8){
        return true;
    }
    var patron = /[0-9a-zA-Z._]/;
    var te;
    te = String.fromCharCode(tecla);
    return patron.test(te);
}

/** Valida que la tecla presionada sea número o letra mayúscula para llenar un número de factura.
* Ver también: {@link acceptNum}, {@link acceptNumTelefono}, {@link IsNumeric}, {@link isInt}, {@link isInteger},
* {@link entero}, {@link isFloat} y {@link flotante}.
* @param e object Evento de presionar una tecla.
* @return boolean Devuelve TRUE si la tecla presionada sea un número o letra mayúscula;
* de lo contrario devuelve FALSE.*/
function acceptStrNroFactura(e)
{
    var tecla;
    tecla = (document.all) ? e.keyCode : e.which;
    if(tecla == 8){
        return true;
    }
    var patron = /[0-9a-zA-Z]/;
    var te;
    te = String.fromCharCode(tecla);
    return patron.test(te);
}

function acceptStrNumeroYLetra(e)
{
    var tecla;
    tecla = (document.all) ? e.keyCode : e.which;
    if(tecla == 8){
        return true;
    }
    var patron = /[0-9a-zA-Z]/;
    var te;
    te = String.fromCharCode(tecla);
    return patron.test(te);
}

function clearValues(element)
{
    element.value='';
}

function noScript()
{
  var objP=document.createElement('p');
  var node=new Array();
  node[0]=document.createTextNode('Para poder visualizar esta página web, necesitas tener activado JavaScript.');
  objP.appendChild(node[0]);
  var objNoScrpt=document.createElement('noscript');
  node[1]=objP;
  objNoScrpt.appendChild(node[1]);
  document.getElementsByTagName('body')[0].insertBefore(objNoScrpt,document.getElementById('cuerpo'));
}

function initView( efecto = 'slow' )
{
    $(document).ready(function() {//noScript();
        timeControlSession();
        document.onpaste = function(){return bOffOnPaste;}
        document.ondragstart = function(){return bOffOnDragStart;} 
        document.oncontextmenu = function(){return bOffOnContextMenu;}
        var obj=new app();
        id_sistema(obj.name);
        $('#precarga').fadeOut( efecto );
        $('body').css({'overflow':'visible'});
        $('body').css({'display':''});
    });
}

/*function ocultar(obj)
{
    for (i = 10; i >= 0; i--)
        setTimeout("obj.style.opacity = '" + (i / 10) + "'", (10 - i) * 100)
}

function mostrar()
{
    for (i = 0; i <= 10; i++)
        setTimeout("document.getElementById('menu').style.opacity = '" + (i / 10) + "'", i * 100)
}*/

function radioGetCheckedValue(nameElements)
{
    var objRadio=document.getElementsByName(nameElements);
    var resp='';
        for(var i=0;i<objRadio.length;i++){
            if(objRadio[i].checked){
                 resp=objRadio[i].value;
                 break;
            }
        }
    return resp;
}

/*function anularBotonDerecho(e)
{

    if (navigator.appName == 'Netscape' && (e.which == 3 || e.which == 2)){
       alert(sMensaje);
       return false;
    }
    else if (navigator.appName == 'Microsoft Internet Explorer' && (event.button == 2)){
       alert(sMensaje);
    }
}*/

function b64EncodeUnicode(str)
{
    return btoa(encodeURIComponent(str).replace(/%([0-9A-Z]{2})/g, function(match, p1) {
        return String.fromCharCode('0x' + p1);
    }));
}

function b64DecodeUnicode(str)
{
    return decodeURIComponent(Array.prototype.map.call(atob(str), function(c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));
}

function utf8_to_b64( str ) //obsoleto unescape
{
  return btoa(unescape(encodeURIComponent( str )));
}

function b64_to_utf8( str ) //obsoleto escape
{
  return decodeURIComponent(escape(atob( str )));
}
