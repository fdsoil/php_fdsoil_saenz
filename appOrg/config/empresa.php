<?php

    $_SESSION['empresaPDFs']='../../../../empresaPDFs/';
    
    $numEmpPermitRegist=3;    
       
    $empresaDisplay['field_tipo_empresa']=array(    1, /* => COOPERATIVA */
                                                    2, /* => PEQUEÑA Y MEDIANA EMPRESA */
                                                    3, /* => EMPRESA DE PROPIEDAD SOCIAL DIRECTA COMUNAL */
                                                    4, /* => COGESTION */
                                                    5, /* => ORGANISMO PÚBLICO  */
                                                    6, /* => UNIDAD DE PRODUCCION SOCIAL */
                                                    7, /* => ORGANIZACIÓN SOCIO PRODUCTIVA */
                                                    8, /* => COMPAÑIA ANÓNIMA */
                                                    9, /* => SOCIEDAD ANÓNIMA */
                                                    10, /* => SOCIEDAD DE RESPONSABILIDAD LIMITADA */
                                                    11, /* => PROPIEDAD SOCIAL INDIRECTA */
                                                    12, /* => PROPIEDAD SOCIAL DIRECTA */
                                                    );
    
    $empresaDisplay['field_ubicacion']=array(   1, /* => DOMICILIO FISCAL */
                                                2, /* => PLANTA */
                                                3, /* => SUCURSAL */
                                                4, /* => ALMACEN */
                                                5, /* => JORNADA */
                                                6, /* => EVENTO */
                                                7, /* => FERIA */
                                                );
    
    $empresaDisplay['field_naturaleza_empresa']=array(  1, /* => FABRICANTE */
                                                        2, /* => DISTRIBUIDOR */
                                                        6, /* => COMERCIALIZADORA */
                                                        7, /* => PROCESADORA */
                                                        8, /* => TRANSPORTISTA */
                                                        9, /* => OPERADOR LOGISTICO */        
                                                );    
    
    $empresaDisplay['field_tipo_representante']=array(  1, /* => LEGAL */ 
                                                        2, /* => AUTORIZADO */
                                                        3, /* => COMERCIAL */
                                                        4, /* => DE COMPRAS */
                                                        5, /* => DE VENTAS */
                                                        6, /* => APODERADO */
                                                        );
    
    $empresaDisplay['fields_direcion_naturaleza']=true;
    
    $empresaDisplay['fields_obras']=true;
    
    $empresaDisplay['fields_ventas_brutas']=true;
   
    $empresaDisplay['aTabs']=array( 0=>'t',
                                    1=>'t',
                                    2=>'t',
                                    3=>'t',
                                    4=>'t',
                                    5=>'t',
                                    6=>'t',
                                    7=>'t',
                                    8=>'t',
                                    9=>'t');
    ?>


