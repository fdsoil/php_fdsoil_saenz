<?php
use \FDSoil\Func as Func;

class SubIndex
{
    public function execute()
    {
        \FDSoil\Audit::validaReferenc();
        $aView['include'] = Func::getFileJSON(__DIR__."/js/include.json");
        $xtpl = new \FDSoil\XTemplate(__DIR__."/view.html");  
        Func::appShowId($xtpl);
        $result = \appOrg\Common::vehiculoTipoList();
        while ($row = \FDSoil\DbFunc::fetchRow($result)) {
            $xtpl->assign('NIVEL_0_ID', $row[0]);
            $xtpl->assign('NIVEL_O_DES', $row[1]);
            $xtpl->parse('main.nivel_0');
        }
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    }
}

