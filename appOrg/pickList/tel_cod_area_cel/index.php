<?php
use \FDSoil\Func as Func;

class SubIndex
{
    public function execute()
    {
        \FDSoil\Audit::validaReferenc();
        $aView['include'] = Func::getFileJSON(__DIR__."/js/include.json");
        $xtpl = new \FDSoil\XTemplate(__DIR__."/view.html");  
        Func::appShowId($xtpl);
        $xtpl->assign('CLASS_TR', 'lospare');
        $xtpl->assign('ID', '0412');
        $xtpl->assign('DESCRIPCION', '0412');
        $xtpl->parse('main.nivel_0');
        $xtpl->assign('CLASS_TR', 'losnone');
        $xtpl->assign('ID', '0414');
        $xtpl->assign('DESCRIPCION', '0414');
        $xtpl->parse('main.nivel_0');
        $xtpl->assign('CLASS_TR', 'lospare');
        $xtpl->assign('ID', '0416');
        $xtpl->assign('DESCRIPCION', '0416');
        $xtpl->parse('main.nivel_0');
        $xtpl->assign('CLASS_TR', 'losnone');
        $xtpl->assign('ID', '0424');
        $xtpl->assign('DESCRIPCION', '0424');
        $xtpl->parse('main.nivel_0');
        $xtpl->assign('CLASS_TR', 'lospare');
        $xtpl->assign('ID', '0426');
        $xtpl->assign('DESCRIPCION', '0426');
        $xtpl->parse('main.nivel_0');
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    }
}



