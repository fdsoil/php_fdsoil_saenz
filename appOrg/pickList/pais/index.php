<?php
use \FDSoil\Func as Func;

class SubIndex
{
    public function execute()
    {
        //\FDSoil\Audit::validaReferenc();
        $aView['include'] = Func::getFileJSON(__DIR__."/js/include.json");
        $xtpl = new \FDSoil\XTemplate(__DIR__."/view.html");  
        Func::appShowId($xtpl);
        $aIds['ids']='0';
        $result = \appOrg\Common::lugarGeoPaisList($aIds);
        $classTR = 'lospare';
        while ($row = \FDSoil\DbFunc::fetchRow($result)) {
           $xtpl->assign('CLASS_TR', $classTR);
            $xtpl->assign('ID', $row[0]);
            $xtpl->assign('DESCRIPCION', $row[1]);
            $xtpl->parse('main.nivel_0');
            $classTR = ($classTR == 'losnone') ? 'lospare' : 'losnone';
        }
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    }
}

    
