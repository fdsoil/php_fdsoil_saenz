<?php
use \appOrg\Common as Common;
use \FDSoil\DbFunc as DbFunc;

class SubIndex
{

    public function __construct($method) { self::$method(); }

    private function lugarGeoMunicipioList() { echo DbFunc::resultToString(Common::lugarGeoMunicipioList(),'#',"%"); }

    private function lugarGeoParroquiaList() { echo DbFunc::resultToString(Common::lugarGeoParroquiaList(),'#',"%"); }

    private function lugarGeoCiudad() { echo Common::lugarGeoCiudad(); }

}

