function id_sistema(strName){  
    document.oncontextmenu = function(){return false}  
    encabezado(strName+' - FDSoil');
    titulo(strName);
    firma();
}

function titulo(strTit){
    document.getElementById('titulo').innerHTML=strTit;
}

function encabezado(strEnc){
    document.getElementById('encabezado').innerHTML=strEnc;
}

function firma(){
    document.getElementById('id_firma').innerHTML='Tool developed at revolution, under free software. Copyleft 2019.';
}

function id_acceso(strEmail,strTelefon,strWebUserRow){
    acceso_email(strEmail);
    acceso_telefon(strTelefon);
    //acceso_webUserRow(strWebUserRow);
}

function acceso_email(strEmail){
    document.getElementById('id_email').innerHTML=strEmail;
}

function acceso_telefon(strTelefon){
    document.getElementById('id_telefon').innerHTML = strTelefon;
}

/*function acceso_webUserRow(aWebUserRow){
    document.getElementById('div_webUserRow').style.display = aWebUserRow[0];
    document.getElementById('id_register').style.display = aWebUserRow[1];
    document.getElementById('id_recover_password').style.display = aWebUserRow[2];
}*/
