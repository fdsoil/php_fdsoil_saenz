<?php
use \FDSoil\DbFunc as DbFunc;

trait CommonUnidadMedida
{
    
    function unidadMedidaList()
    {
        return DbFunc::exeQryFile(self::_path()."unidad_medida_select.sql", null, false, '', 'common'); 
    }
    
    function unidadMedidaAuxList()
    {
        return DbFunc::exeQryFile(self::_path()."unidad_medida_aux_select.sql", $_POST, false, '', 'common'); 
    }

}

