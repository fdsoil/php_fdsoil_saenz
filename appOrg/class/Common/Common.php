<?php
namespace appOrg;

require_once("CommonAranceles.php");
require_once("CommonCiiu.php");
require_once("CommonColor.php");
require_once("CommonEvaluacion.php");
require_once("CommonLugarGeo.php"); 
require_once("CommonUnidadMedida.php");
require_once("CommonVehiculo.php");

class Common 
{ 
    
    use \CommonAranceles, \CommonCiiu, \CommonColor, \CommonEvaluacion, \CommonLugarGeo, \CommonUnidadMedida, \CommonVehiculo;
    
    private function _path() { return "../../../".$_SESSION['appOrg']."/class/Common/sql/common/"; }
 
    function envalajeTipoList()
    { 
        
        return \FDSoil\DbFunc::exeQryFile( self::_path() . "unidad_medida_envalaje_select.sql", $_GET, false, '', 'common' );
    }
    
}

