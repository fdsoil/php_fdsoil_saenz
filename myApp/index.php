<?php 
abstract class Index
{
    public function main()
    {
        session_start();
        $strFileName = "config/app.json";
        file_exists($strFileName) ? $oJSON = json_decode(file_get_contents($strFileName)) : die("File not Found " . $strFileName);
        require_once (__DIR__."/../".$oJSON->app->FDSoil."/class/Func/Func.php"); 
        $_SESSION = \FDSoil\Func::pasPropOfObjToArr( $oJSON, array('app'));
        header("Location: ../".strtolower($_SESSION['FDSoil'])."/admin_acceso/");
    }
}
Index::main();

