<?php
use \myApp\Maestro\Detalle as Detalle;

class SubIndex
{

    public function __construct($method)
    {
        $_POST = \FDSoil\Func::base64DecodeArrValKey($_POST);
        self::$method();
    }

    private function detalleRegister() { echo base64_encode(Detalle::detalleRegister()); }

    private function detalleGet() { echo base64_encode(json_encode(Detalle::detalleGet())); }

    private function detalleDelete() { echo base64_encode(Detalle::detalleDelete()); }

}

