<?php
session_start();
require_once("../../../".$_SESSION['FDSoil']."/class/Controll/ControllRep.php");
abstract class Index
{
    public function control()
    {
        $arr['path']=__DIR__;
        $arr['filesName']=['index'];
        $obj = new \FDSoil\Controll\ControllRep($arr);
        $obj->execute();
    }
}
Index::control();
