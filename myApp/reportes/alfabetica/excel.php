<?php 
use \FDSoil\Func as Func;
use \FDSoil\DbFunc as DbFunc;
use \myApp\Alfabetica\AlfabeticaFiltro as AlfabeticaFiltro;

class SubIndex
{
    public function execute()
    {
        header("Content-Type: application/vnd.ms-excel");
        $aRegistros = AlfabeticaFiltro::filtro();
        AlfabeticaFiltro::columnsExistsIniArr();
        AlfabeticaFiltro::columnsExistsInArr($aRegistros);
        $tRegistros=count($aRegistros);
        $tabla = "<meta http-equiv=\"content-type\" content=\"application/xhtml+xml; charset=UTF-8\"/>
        <table style=\"font-size:35px;\" width=\"100%\" cellspacing=\"1\" cellpadding=\"1\" border=\"1\" nobr=\"true\">";
        $tabla .= "<tr nobr=\"true\">";
        if (AlfabeticaFiltro::$_aColumnsExists["cedula"])
            $tabla .= "<td align=\"center\" width=\"10%\"><b>CÉDULA</b></td>";
        if (AlfabeticaFiltro::$_aColumnsExists["nombre1"])
            $tabla .= "<td align=\"center\" width=\"10%\"><b>1° NOMBRE</b></td>";
        if (AlfabeticaFiltro::$_aColumnsExists["nombre2"])
            $tabla .= "<td align=\"center\" width=\"10%\"><b>2 NOMBRE</b></td>";
        if (AlfabeticaFiltro::$_aColumnsExists["apellido1"])
            $tabla .= "<td align=\"center\" width=\"10%\"><b>1° APELLIDO</b></td>";
        if (AlfabeticaFiltro::$_aColumnsExists["apellido2"])
            $tabla .= "<td align=\"center\" width=\"10%\"><b>2° APELLIDO</b></td>";
        /*if (AlfabeticaFiltro::$_aColumnsExists["fecha_recepcion"])
            $tabla .= "<td align=\"center\" width=\"8%\"><b>FECHA_RECEPCIÓN</b></td>";
        if (AlfabeticaFiltro::$_aColumnsExists["referencia"])
            $tabla .= "<td align=\"center\" width=\"10%\"><b>N°_OFICIO</b></td>";
        if (AlfabeticaFiltro::$_aColumnsExists["fecha_documento"])
            $tabla .= "<td align=\"center\" width=\"9%\"><b>FECHA_DOCUMENTO</b></td>";
        if (AlfabeticaFiltro::$_aColumnsExists["remitente"])
            $tabla .= "<td align=\"center\" width=\"40%\"><b>REMITENTE</b></td>";
        if (AlfabeticaFiltro::$_aColumnsExists["asunto"])
            $tabla .= "<td align=\"center\" width=\"30%\"><b>ASUNTO</b></td>";
        if (AlfabeticaFiltro::$_aColumnsExists["destinatario"])
            $tabla .= "<td align=\"center\" width=\"25%\"><b>DESTINATARIO</b></td>";
        if (AlfabeticaFiltro::$_aColumnsExists["descripcion"])
            $tabla .= "<td align=\"center\" width=\"20%\"><b>DESCRIPCIÓN</b></td> ";
        if (AlfabeticaFiltro::$_aColumnsExists["status"])
            $tabla .= "<td align=\"center\" width=\"10%\"><b>ESTATUS</b></td>";
        if (AlfabeticaFiltro::$_aColumnsExists["presentante"])
            $tabla .= "<td align=\"center\" width=\"10%\"><b>PRESENTANTE</b></td>";
        if (AlfabeticaFiltro::$_aColumnsExists["receptor"])
            $tabla .= "<td align=\"center\" width=\"10%\"><b>RECEPTOR</b></td>";
        if (AlfabeticaFiltro::$_aColumnsExists["digitalizador"])
            $tabla .= "<td align=\"center\" width=\"10%\"><b>DIGITALIZADOR</b></td>";
        if (AlfabeticaFiltro::$_aColumnsExists["revisador"])
            $tabla .= "<td align=\"center\" width=\"10%\"><b>REVISADOR</b></td>";
        if (AlfabeticaFiltro::$_aColumnsExists["asignador"])
            $tabla .= "<td align=\"center\" width=\"10%\"><b>ASIGNADOR</b></td>";*/
        $tabla .= "</tr>";
        for ($i=0; $i<$tRegistros; $i++) {
            $tabla .= "<tr>";
            if (AlfabeticaFiltro::$_aColumnsExists["cedula"])
                $tabla.="<td align=\"center\" width=\"10%\">".$aRegistros[$i]['cedula']."</td>";
            if (AlfabeticaFiltro::$_aColumnsExists["nombre1"])
                $tabla.="<td align=\"center\" width=\"10%\">".$aRegistros[$i]['nombre1']."</td>";
            if (AlfabeticaFiltro::$_aColumnsExists["nombre2"])
                $tabla.="<td align=\"center\" width=\"10%\">".$aRegistros[$i]['nombre2']."</td>";
            if (AlfabeticaFiltro::$_aColumnsExists["apellido1"])
                $tabla.="<td align=\"center\" width=\"10%\">".$aRegistros[$i]['apellido1']."</td>";
            if (AlfabeticaFiltro::$_aColumnsExists["apellido2"])
                $tabla.="<td align=\"center\" width=\"10%\">".$aRegistros[$i]['apellido2']."</td>";
            /*if (AlfabeticaFiltro::$_aColumnsExists["fecha_recepcion"])
                $tabla.="<td align=\"center\" width=\"8%\">".Func::change_date_format($aRegistros[$i]['fecha_recepcion'])."</td>";
            if (AlfabeticaFiltro::$_aColumnsExists["referencia"])
                $tabla.="<td align=\"center\" width=\"10%\">".$aRegistros[$i]['referencia']."</td>";
            if (AlfabeticaFiltro::$_aColumnsExists["fecha_documento"])
                $tabla .= "<td align=\"center\" width=\"9%\">".Func::change_date_format($aRegistros[$i]['fecha_documento'])."</td>";
            if (AlfabeticaFiltro::$_aColumnsExists["remitente"])
                $tabla.="<td align=\"left\" width=\"10%\">".$aRegistros[$i]['remitente']."</td>";
            if (AlfabeticaFiltro::$_aColumnsExists["asunto"])
                $tabla.="<td align=\"left\" width=\"30%\">".mb_strtolower($aRegistros[$i]['asunto'], 'UTF-8')."</td>";
            if (AlfabeticaFiltro::$_aColumnsExists["destinatario"])
                $tabla .= "<td align=\"left\" width=\"25%\">".$aRegistros[$i]['destinatario']."</td>"; 
            if (AlfabeticaFiltro::$_aColumnsExists["descripcion"])
                $tabla .= "<td align=\"left\" width=\"20%\">".$aRegistros[$i]['descripcion']."</td>";    
            if (AlfabeticaFiltro::$_aColumnsExists["status"])
                $tabla.="<td align=\"center\" width=\"10%\">".$aRegistros[$i]['status']."</td>";
            if (AlfabeticaFiltro::$_aColumnsExists["presentante"])
                $tabla.="<td align=\"center\" width=\"10%\">".$aRegistros[$i]['presentante']."</td>";
            if (AlfabeticaFiltro::$_aColumnsExists["receptor"])
                $tabla.="<td align=\"center\" width=\"10%\">".$aRegistros[$i]['receptor']."</td>";
            if (AlfabeticaFiltro::$_aColumnsExists["digitalizador"])
                $tabla.="<td align=\"center\" width=\"10%\">".$aRegistros[$i]['digitalizador']."</td>";
            if (AlfabeticaFiltro::$_aColumnsExists["revisador"])
                $tabla.="<td align=\"center\" width=\"10%\">".$aRegistros[$i]['revisador']."</td>";
            if (AlfabeticaFiltro::$_aColumnsExists["asignador"])
                $tabla.="<td align=\"center\" width=\"10%\">".$aRegistros[$i]['asignador']."</td>";*/
            $tabla.="</tr>";
        }
        $tabla.="</table>";   
        echo $tabla;
    }
}
SubIndex::execute();

