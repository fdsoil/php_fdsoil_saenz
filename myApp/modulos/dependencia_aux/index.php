<?php
use \FDSoil\XTemplate as XTemplate;
use \FDSoil\DbFunc as DbFunc;
use \FDSoil\Func as Func;
use \myApp\Dependencia as Dependencia;

class SubIndex
{
    public function execute($aReqs)
    {
        $obj = new Dependencia();
        $aView['include'] = Func::getFileJSON(__DIR__."/js/include.json");
        $aView['userData'] = Func::usuarioData();
        $aView['load'] = [];
        $xtpl = new XTemplate(__DIR__."/view.html");
        Func::appShowId($xtpl);
        $aRegist = array_key_exists('id', $_POST)
            ? DbFunc::fetchAssoc(Dependencia::dependenciaGet('REGIST'))
                :\FDSoil\DbFunc::iniRegist('dependencia','admini');
        $xtpl->assign('ID', $aRegist['id']);
        $xtpl->assign('NOMBRE', $aRegist['nombre']);
        $xtpl->assign('NOMBRE_READONLY', array_key_exists('id', $_POST) ? 'readonly' : '');
        Func::btnsPutPanel( $xtpl, [["btnName" => "Return", "btnBack" => "dependencia"],
                                    ["btnName" => "Save"  , "btnClick"=> "valEnvio();"]]);
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    }
}