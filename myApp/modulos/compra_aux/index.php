<?php
use \FDSoil\DbFunc as DbFunc;
use \FDSoil\Func as Func;

require_once(__DIR__."/Solapa0.php");
require_once(__DIR__."/Solapa1.php");

class SubIndex
{
    use Solapa0, Solapa1;

    public function execute()
    {
        $aView['include'] = Func::getFileJSON(__DIR__."/js/include.json");
        $aView['userData'] = Func::usuarioData();
        $aView['load'] = array_key_exists('id', $_POST)?$_POST['id']:0;
        $xtpl = new \FDSoil\XTemplate(__DIR__."/view.html");
        Func::appShowId($xtpl);
        if (!array_key_exists('id', $_POST)) {
            $xtpl->assign('TAB_NONE_BLOCK1', 'none');
            $xtpl->assign('TAB_NONE_BLOCK2', 'none');
            $xtpl->assign('TAB_NONE_BLOCK3', 'none');
            $xtpl->assign('BOTONES_NONE_BLOCK', 'none');
        }
        $aSolapa[0] = self::_solapa0();
        $aSolapa[1] = self::_solapa1();
        Func::bldSolapas($xtpl, $aSolapa);
        Func::btnsPutPanel( $xtpl, [["btnName" => "Return", "btnBack" => "compra"],
                                            ["btnName" => "Save", "btnClick"=> "valEnvio(1);"],
                                            ["btnName" => "Close", "btnClick"=> "valEnvio(2);", "btnLabel" => "Maestro", "btnDisplay" => "none"]]);
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    }
}

