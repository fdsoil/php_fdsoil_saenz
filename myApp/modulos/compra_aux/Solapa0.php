<?php
use \FDSoil\XTemplate as XTemplate;
use \FDSoil\DbFunc as DbFunc;
use \FDSoil\Func as Func;
use \myApp\Maestro as Maestro;

trait Solapa0
{
    private function _solapa0()
    {
        $aRegist = array_key_exists('id', $_POST) ?
            DbFunc::fetchAssoc(Maestro::maestroGet('REGIST')) :
                \FDSoil\DbFunc::iniRegist('maestro','compra');
        $xtpl = new \FDSoil\XTemplate(__DIR__."/solapa0.html");
        Func::appShowId($xtpl);
        $xtpl->assign('ID', $aRegist['id']);
        $xtpl->assign('NUMERO', $aRegist['numero']);
        $xtpl->assign('NUMERO_READONLY', array_key_exists('id', $_POST) ? 'readonly' : '');
        $xtpl->assign('FECHA', Func::change_date_format($aRegist['fecha']));
        $xtpl->assign('RECIBIDO_POR', $aRegist['recibido_por']);
        $xtpl->assign('RECIBIDO_FECHA', Func::change_date_format($aRegist['recibido_fecha']));
        $xtpl->assign('RECIBIDO_HORA', Func::timeMilitarToNormal($aRegist['recibido_hora']));
        $xtpl->assign('EVALUADO_POR', $aRegist['evaluado_por']);
        $xtpl->assign('EVALUADO_FECHA', Func::change_date_format($aRegist['evaluado_fecha']));
        $xtpl->assign('EVALUADO_HORA', Func::timeMilitarToNormal($aRegist['evaluado_hora']));
        $result = Maestro::proveedorList();
        while ($row = DbFunc::fetchRow($result)) {
            $xtpl->assign('ID_PROVEEDOR', $row[0]);
            $xtpl->assign('DES_PROVEEDOR', $row[1]);
            $xtpl->assign('SELECTED_PROVEEDOR', ($aRegist['id_proveedor'] == $row[0]) ? 'selected' : '');
            $xtpl->parse('main.proveedor');
        }
        $result = Maestro::requisicionList();
        while ($row = DbFunc::fetchRow($result)) {
            $xtpl->assign('ID_REQUISICION', $row[0]);
            $xtpl->assign('DES_REQUISICION', $row[1]);
            $xtpl->assign('SELECTED_REQUISICION', ($aRegist['id_requisicion'] == $row[0]) ? 'selected' : '');
            $xtpl->parse('main.requisicion');
        }
        $result = Maestro::formaPagoList();
        while ($row = DbFunc::fetchRow($result)) {
            $xtpl->assign('ID_FORMA_PAGO', $row[0]);
            $xtpl->assign('DES_FORMA_PAGO', $row[1]);
            $xtpl->assign('SELECTED_FORMA_PAGO', ($aRegist['id_forma_pago'] == $row[0]) ? 'selected' : '');
            $xtpl->parse('main.forma_pago');
        }
        $xtpl->parse('main');
        return $xtpl->out_var('main');
    }
}

