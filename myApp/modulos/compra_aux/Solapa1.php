<?php
use \FDSoil\DbFunc as DbFunc;
use \FDSoil\Func as Func;
use \myApp\Maestro\Detalle as Detalle;

trait Solapa1
{
    private function _solapa1()
    {
        $xtpl = new \FDSoil\XTemplate(__DIR__."/solapa1.html");
        Func::appShowId($xtpl);
        $result = Detalle::articuloList();
        while ($row = DbFunc::fetchRow($result)) {
            $xtpl->assign('ID_ARTICULO', $row[0]);
            $xtpl->assign('DES_ARTICULO', $row[1]);
            $xtpl->parse('main.articulo');
        }
        if (array_key_exists('id', $_POST)) {
            $matrix = Detalle::detalleGet();
            $classTR = 'lospare';
            foreach ($matrix as $arr) {
                $xtpl->assign('CLASS_TR', $classTR);
                $xtpl->assign('ID', $arr['id']);
                $xtpl->assign('CANTIDAD', $arr['cantidad']);
                $xtpl->assign('PRECIO', $arr['precio']);
                $xtpl->assign('ID_ARTICULO', $arr['id_articulo']);
                $xtpl->assign('DES_ARTICULO', $arr['des_articulo']);
                $xtpl->parse('main.tab_detalle');
                $classTR = ($classTR == 'losnone') ? 'lospare' : 'losnone';
            }
        }
        $xtpl->parse('main');
        return $xtpl->out_var('main');
    }
}

