<?php
use \FDSoil\XTemplate as XTemplate;
use \FDSoil\DbFunc as DbFunc;
use \FDSoil\Func as Func;
use \myApp\Clasificador as Clasificador;

class SubIndex
{
    public function execute($aReqs)
    {
        $obj = new Clasificador();
        $aView['include'] = Func::getFileJSON(__DIR__."/js/include.json");
        $aView['userData'] = Func::usuarioData();
        $aView['load'] = [];
        $xtpl = new XTemplate(__DIR__."/view.html");
        Func::appShowId($xtpl);
        $aRegist = array_key_exists('id', $_POST)
            ? DbFunc::fetchAssoc(Clasificador::clasificadorGet('REGIST'))
                :\FDSoil\DbFunc::iniRegist('clasificador','admini');
        $xtpl->assign('ID', $aRegist['id']);
        $xtpl->assign('CODIGO', $aRegist['codigo']);
        $xtpl->assign('CODIGO_READONLY', array_key_exists('id', $_POST) ? 'readonly' : '');
        $xtpl->assign('NOMBRE', $aRegist['nombre']);
        $xtpl->assign('DESCRIPCION', $aRegist['descripcion']);
        $xtpl->assign('TIPO_CHK', $aRegist['tipo'] === 't' ? 'checked' : '');
        $xtpl->assign('VIGENCIA', $aRegist['vigencia']);
        $xtpl->assign('VIGENCIA_READONLY', array_key_exists('id', $_POST) ? 'readonly' : '');
        Func::btnsPutPanel( $xtpl, [["btnName" => "Return", "btnBack" => "clasificador"],
                                    ["btnName" => "Save"  , "btnClick"=> "valEnvio();"]]);
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    }
}