<?php
use \FDSoil\DbFunc as DbFunc;
use \FDSoil\Func as Func;

class SubIndex
{
    public function execute()
    {
        \FDSoil\Audit::validaReferenc();
        $aView['include'] = Func::getFileJSON(__DIR__."/js/include.json");
        $aView['userData'] = Func::usuarioData();
        $aView['load'] = $_SESSION['menu'];
        $xtpl = new \FDSoil\XTemplate(__DIR__."/view.html");
        Func::appShowId($xtpl);
        Func::btnRecordAdd( $xtpl ,["btnRecordName"=>"Compra"]);
        $result = \myApp\Maestro::maestroGet('LIST');
        while ($row = DbFunc::fetchAssoc($result)){
            $xtpl->assign('NUMERO', $row['numero']);
            $xtpl->assign('FECHA', Func::change_date_format($row['fecha']));
            $xtpl->assign('ID_PROVEEDOR', $row['id_proveedor']);
            $xtpl->assign('ID_REQUISICION', $row['id_requisicion']);
            $xtpl->assign('ID_FORMA_PAGO', $row['id_forma_pago']);
            $xtpl->assign('RECIBIDO_POR', $row['recibido_por']);
            $xtpl->assign('RECIBIDO_FECHA', Func::change_date_format($row['recibido_fecha']));
            $xtpl->assign('RECIBIDO_HORA', Func::timeMilitarToNormal($row['recibido_hora']));
            $xtpl->assign('EVALUADO_POR', $row['evaluado_por']);
            $xtpl->assign('EVALUADO_FECHA', Func::change_date_format($row['evaluado_fecha']));
            $xtpl->assign('EVALUADO_HORA', Func::timeMilitarToNormal($row['evaluado_hora']));
            $xtpl->assign('DES_PROVEEDOR', $row['des_proveedor']);
            $xtpl->assign('DES_PROVEEDOR', $row['des_proveedor']);
            $xtpl->assign('DES_PROVEEDOR', $row['des_proveedor']);
            $xtpl->assign('DES_PROVEEDOR', $row['des_proveedor']);
            $xtpl->assign('DES_PROVEEDOR', $row['des_proveedor']);
            $xtpl->assign('DES_REQUISICION', $row['des_requisicion']);
            $xtpl->assign('DES_REQUISICION', $row['des_requisicion']);
            $xtpl->assign('DES_REQUISICION', $row['des_requisicion']);
            $xtpl->assign('DES_REQUISICION', $row['des_requisicion']);
            $xtpl->assign('DES_REQUISICION', $row['des_requisicion']);
            $xtpl->assign('DES_REQUISICION', $row['des_requisicion']);
            $xtpl->assign('DES_FORMA_PAGO', $row['des_forma_pago']);
            Func::btnRecordEdit( $xtpl ,["btnId"=>$row['id']]);
            Func::btnRecordDelete( $xtpl ,["btnId"=>$row['id']]);
            $xtpl->parse('main.rows');
        }
        Func::btnsPutPanel( $xtpl ,[["btnName"=>"Exit"]]);
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    }
}

