function submitInsert(val)
{
    relocate('../compra_aux/', {});
}

function submitEdit(val)
{
    relocate('../compra_aux/', {'id':val});
}

function submitDelete(val)
{
    confirm('¿Desea eliminar este registro?', 'Confirmar', function(ok){
        if (ok)
            relocate('delete/', {'id':val});
        });
}

