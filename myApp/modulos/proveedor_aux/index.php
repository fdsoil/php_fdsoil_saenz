<?php
use \FDSoil\XTemplate as XTemplate;
use \FDSoil\DbFunc as DbFunc;
use \FDSoil\Func as Func;
use \myApp\Proveedor as Proveedor;

class SubIndex
{
    public function execute($aReqs)
    {
        $obj = new Proveedor();
        $aView['include'] = Func::getFileJSON(__DIR__."/js/include.json");
        $aView['userData'] = Func::usuarioData();
        $aView['load'] = [];
        $xtpl = new XTemplate(__DIR__."/view.html");
        Func::appShowId($xtpl);
        $aRegist = array_key_exists('id', $_POST)
            ? DbFunc::fetchAssoc(Proveedor::proveedorGet('REGIST'))
                :\FDSoil\DbFunc::iniRegist('proveedor','admini');
        $xtpl->assign('ID', $aRegist['id']);
        $xtpl->assign('RIF', $aRegist['rif']);
        $xtpl->assign('RIF_READONLY', array_key_exists('id', $_POST) ? 'readonly' : '');
        $xtpl->assign('NOMBRE', $aRegist['nombre']);
        $xtpl->assign('DESCRIPCION', $aRegist['descripcion']);
        $xtpl->assign('DIRECCION', $aRegist['direccion']);
        $xtpl->assign('TELEFONO', $aRegist['telefono']);
        Func::btnsPutPanel( $xtpl, [["btnName" => "Return", "btnBack" => "proveedor"],
                                    ["btnName" => "Save"  , "btnClick"=> "valEnvio();"]]);
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    }
}