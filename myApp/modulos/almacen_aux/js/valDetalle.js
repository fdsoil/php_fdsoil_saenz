function onOffPanelDetalle() 
{
    toggle('id_panel_detalle', 'blind',500);
    changeTheObDeniedWritingImg('id_img_detalle');
    initObjs(document.getElementById('id_panel_detalle'));
    document.getElementById('id_detalle').value=0;
}

function valEnvioDetalle()
{
    var oDiv=document.getElementById('id_panel_detalle');
    if (validateObjs(document.getElementById("id_panel_detalle"))) 
        sendDetalleRegister();
}

function editDetalle(obj)
{
    if(document.getElementById('id_panel_detalle').style.display=='none'){
        onOffPanelDetalle();
    }
    document.getElementById('id_detalle').value=obj.parentNode.parentNode.id;
    var oPanel=document.getElementById("id_panel_detalle");
    var oInputs=oPanel.getElementsByTagName("input");
    oInputs['cantidad'].value = obj.parentNode.parentNode.cells[0].innerHTML;
    var oSelects=oPanel.getElementsByTagName("select");
    oSelects['id_articulo'].value = obj.parentNode.parentNode.cells[1].id;
    var oTextAreas=oPanel.getElementsByTagName("textarea");
}

