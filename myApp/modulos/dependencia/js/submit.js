function submitInsert(val)
{
    relocate('../dependencia_aux/', {});
}

function submitEdit(val)
{
    relocate('../dependencia_aux/', {'id':val});
}

function submitDelete(val)
{
    confirm('¿Desea eliminar este registro?', 'Confirmar', function(ok){
        if (ok)
            relocate('delete/', {'id':val});
        });
}

