<?php
use \FDSoil\DbFunc as DbFunc;
use \FDSoil\Func as Func;

class SubIndex
{
    public function execute()
    {
        \FDSoil\Audit::validaReferenc();
        $aView['include'] = Func::getFileJSON(__DIR__."/js/include.json");
        $aView['userData'] = Func::usuarioData();
        $aView['load'] = $_SESSION['menu'];
        $xtpl = new \FDSoil\XTemplate(__DIR__."/view.html");
        Func::appShowId($xtpl);
        Func::btnRecordAdd( $xtpl ,["btnRecordName"=>"Requisicion"]);
        $result = \myApp\Maestro::maestroGet('LIST');
        while ($row = DbFunc::fetchAssoc($result)){
            $xtpl->assign('NUMERO', $row['numero']);
            $xtpl->assign('MEMO', $row['memo']);
            $xtpl->assign('ID_DEPENDENCIA', $row['id_dependencia']);
            $xtpl->assign('FECHA_SOLICITUD', Func::change_date_format($row['fecha_solicitud']));
            $xtpl->assign('RECIBIDO', $row['recibido']);
            $xtpl->assign('FECHA_RECIBIDO', Func::change_date_format($row['fecha_recibido']));
            $xtpl->assign('DES_DEPENDENCIA', $row['des_dependencia']);
            Func::btnRecordEdit( $xtpl ,["btnId"=>$row['id']]);
            Func::btnRecordDelete( $xtpl ,["btnId"=>$row['id']]);
            $xtpl->parse('main.rows');
        }
        Func::btnsPutPanel( $xtpl ,[["btnName"=>"Exit"]]);
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    }
}

