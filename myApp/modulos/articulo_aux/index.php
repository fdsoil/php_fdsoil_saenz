<?php
use \FDSoil\XTemplate as XTemplate;
use \FDSoil\DbFunc as DbFunc;
use \FDSoil\Func as Func;
use \myApp\Articulo as Articulo;

class SubIndex
{
    public function execute($aReqs)
    {
        $obj = new Articulo();
        $aView['include'] = Func::getFileJSON(__DIR__."/js/include.json");
        $aView['userData'] = Func::usuarioData();
        $aView['load'] = [];
        $xtpl = new XTemplate(__DIR__."/view.html");
        Func::appShowId($xtpl);
        $aRegist = array_key_exists('id', $_POST)
            ? DbFunc::fetchAssoc(Articulo::articuloGet('REGIST'))
                :\FDSoil\DbFunc::iniRegist('articulo','admini');
        $xtpl->assign('ID', $aRegist['id']);
        $xtpl->assign('NOMBRE', $aRegist['nombre']);
        $xtpl->assign('NOMBRE_READONLY', array_key_exists('id', $_POST) ? 'readonly' : '');
        $xtpl->assign('DESCRIPCION', $aRegist['descripcion']);
        $xtpl->assign('ID_CLASIFICADOR', $aRegist['id_clasificador']);
        $result = Articulo::clasificadorList();
        while ($row = DbFunc::fetchRow($result)) {
            $xtpl->assign('ID_CLASIFICADOR', $row[0]);
            $xtpl->assign('DES_CLASIFICADOR', $row[1]);
            $xtpl->assign('SELECTED_CLASIFICADOR', ($aRegist['id_clasificador'] == $row[0]) ? 'selected' : '');
            $xtpl->parse('main.clasificador');
        }
        Func::btnsPutPanel( $xtpl, [["btnName" => "Return", "btnBack" => "articulo"],
                                    ["btnName" => "Save"  , "btnClick"=> "valEnvio();"]]);
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    }
}