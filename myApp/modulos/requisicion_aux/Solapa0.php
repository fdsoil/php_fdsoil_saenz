<?php
use \FDSoil\XTemplate as XTemplate;
use \FDSoil\DbFunc as DbFunc;
use \FDSoil\Func as Func;
use \myApp\Maestro as Maestro;

trait Solapa0
{
    private function _solapa0()
    {
        $aRegist = array_key_exists('id', $_POST) ?
            DbFunc::fetchAssoc(Maestro::maestroGet('REGIST')) :
                \FDSoil\DbFunc::iniRegist('maestro','requisicion');
        $xtpl = new \FDSoil\XTemplate(__DIR__."/solapa0.html");
        Func::appShowId($xtpl);
        $xtpl->assign('ID', $aRegist['id']);
        $xtpl->assign('NUMERO', $aRegist['numero']);
        $xtpl->assign('NUMERO_READONLY', array_key_exists('id', $_POST) ? 'readonly' : '');
        $xtpl->assign('MEMO', $aRegist['memo']);
        $xtpl->assign('FECHA_SOLICITUD', Func::change_date_format($aRegist['fecha_solicitud']));
        $xtpl->assign('RECIBIDO', $aRegist['recibido']);
        $xtpl->assign('FECHA_RECIBIDO', Func::change_date_format($aRegist['fecha_recibido']));
        $result = Maestro::dependenciaList();
        while ($row = DbFunc::fetchRow($result)) {
            $xtpl->assign('ID_DEPENDENCIA', $row[0]);
            $xtpl->assign('DES_DEPENDENCIA', $row[1]);
            $xtpl->assign('SELECTED_DEPENDENCIA', ($aRegist['id_dependencia'] == $row[0]) ? 'selected' : '');
            $xtpl->parse('main.dependencia');
        }
        $xtpl->parse('main');
        return $xtpl->out_var('main');
    }
}

