function sendDetalleRegister() 
{
    var responseAjax = (response) =>
    {
        msjAdmin(response);
        if (inArray(response, ['C', 'A'])){
            sendDetalleGet();
            onOffPanelDetalle();
        }
    }
    var sIdVal = document.getElementById('id').value;
    var oPanel = document.getElementById('id_panel_detalle');
    var reqs = b64EncodeUnicode('id_maestro') + '=' + b64EncodeUnicode(sIdVal) + '&' + request(oPanel, true);
    var ajax = new sendAjax();
    ajax.method = 'POST';
    ajax.url = "../../../" + myApp + "/reqs/control/maestro/detalle.php/detalleRegister";
    ajax.data = reqs;
    ajax.funResponse = responseAjax;
    ajax.b64 = true;
    ajax.send();
}

function sendDetalleGet(bAsync)
{
    var responseAjax = (response) =>
    {
        fillTabDetalle(response);
        valBtnClose();
    }
    var reqs = b64EncodeUnicode("id") + "=" + b64EncodeUnicode(document.getElementById("id").value);
    var ajax = new sendAjax();
    ajax.method = 'POST';
    ajax.url = "../../../" + myApp + "/reqs/control/maestro/detalle.php/detalleGet";
    ajax.data = reqs;
    ajax.funResponse = responseAjax;
    ajax.dataType = 'json';
    ajax.async = bAsync;
    ajax.b64 = true;
    ajax.send();
}

function sendDetalleDelete(id)
{
    confirm('Desea Eliminar Este Registro?', 'Confirmar', function(ok) {
        if (ok) {
            var responseAjax = (response) =>
            {
                (response != 'B') ? msjAdmin(response) : sendDetalleGet();
            }
            var ajax = new sendAjax();
            ajax.method = 'POST';
            ajax.url = "../../../" + myApp + "/reqs/control/maestro/detalle.php/detalleDelete";
            ajax.data = b64EncodeUnicode("id")+"="+b64EncodeUnicode(id);
            ajax.funResponse = responseAjax;
            ajax.b64 = true;
            ajax.send();
        }
    });
}

