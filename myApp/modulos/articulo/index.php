<?php
use \FDSoil\DbFunc as DbFunc;
use \FDSoil\Func as Func;

class SubIndex
{
    public function execute()
    {
        \FDSoil\Audit::validaReferenc();
        $aView['include'] = Func::getFileJSON(__DIR__."/js/include.json");
        $aView['userData'] = Func::usuarioData();
        $aView['load'] = $_SESSION['menu'];
        $xtpl = new \FDSoil\XTemplate(__DIR__."/view.html");
        Func::appShowId($xtpl);
        Func::btnRecordAdd( $xtpl ,["btnRecordName"=>"Articulo"]);
        $result = \myApp\Articulo::articuloGet('LIST');
        while ($row = DbFunc::fetchAssoc($result)){
            $xtpl->assign('NOMBRE', $row['nombre']);
            $xtpl->assign('DESCRIPCION', $row['descripcion']);
            $xtpl->assign('ID_CLASIFICADOR', $row['id_clasificador']);
            $xtpl->assign('DES_CLASIFICADOR', $row['des_clasificador']);
            $xtpl->assign('DES_CLASIFICADOR', $row['des_clasificador']);
            $xtpl->assign('DES_CLASIFICADOR', $row['des_clasificador']);
            $xtpl->assign('DES_CLASIFICADOR', $row['des_clasificador']);
            $xtpl->assign('DES_CLASIFICADOR', $row['des_clasificador']);
            Func::btnRecordEdit( $xtpl ,["btnId"=>$row['id']]);
            Func::btnRecordDelete( $xtpl ,["btnId"=>$row['id']]);
            $xtpl->parse('main.rows');
        }
        Func::btnsPutPanel( $xtpl ,[["btnName"=>"Exit"]]);
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    }
}

