function submitInsert(val)
{
    relocate('../articulo_aux/', {});
}

function submitEdit(val)
{
    relocate('../articulo_aux/', {'id':val});
}

function submitDelete(val)
{
    confirm('¿Desea eliminar este registro?', 'Confirmar', function(ok){
        if (ok)
            relocate('delete/', {'id':val});
        });
}

