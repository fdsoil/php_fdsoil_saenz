<?php
use \FDSoil\DbFunc as DbFunc;
use \FDSoil\Func as Func;

class SubIndex
{
    public function execute()
    {
        \FDSoil\Audit::validaReferenc();
        $aView['include'] = Func::getFileJSON(__DIR__."/js/include.json");
        $aView['userData'] = Func::usuarioData();
        $aView['load'] = $_SESSION['menu'];
        $xtpl = new \FDSoil\XTemplate(__DIR__."/view.html");
        Func::appShowId($xtpl);
        Func::btnRecordAdd( $xtpl ,["btnRecordName"=>"Proveedor"]);
        $result = \myApp\Proveedor::proveedorGet('LIST');
        while ($row = DbFunc::fetchAssoc($result)){
            $xtpl->assign('RIF', $row['rif']);
            $xtpl->assign('NOMBRE', $row['nombre']);
            $xtpl->assign('DESCRIPCION', $row['descripcion']);
            $xtpl->assign('DIRECCION', $row['direccion']);
            $xtpl->assign('TELEFONO', $row['telefono']);
            Func::btnRecordEdit( $xtpl ,["btnId"=>$row['id']]);
            Func::btnRecordDelete( $xtpl ,["btnId"=>$row['id']]);
            $xtpl->parse('main.rows');
        }
        Func::btnsPutPanel( $xtpl ,[["btnName"=>"Exit"]]);
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    }
}

