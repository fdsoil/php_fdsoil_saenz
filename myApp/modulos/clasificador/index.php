<?php
use \FDSoil\DbFunc as DbFunc;
use \FDSoil\Func as Func;

class SubIndex
{
    public function execute()
    {
        \FDSoil\Audit::validaReferenc();
        $aView['include'] = Func::getFileJSON(__DIR__."/js/include.json");
        $aView['userData'] = Func::usuarioData();
        $aView['load'] = $_SESSION['menu'];
        $xtpl = new \FDSoil\XTemplate(__DIR__."/view.html");
        Func::appShowId($xtpl);
        Func::btnRecordAdd( $xtpl ,["btnRecordName"=>"Clasificador"]);
        $result = \myApp\Clasificador::clasificadorGet('LIST');
        while ($row = DbFunc::fetchAssoc($result)){
            $xtpl->assign('CODIGO', $row['codigo']);
            $xtpl->assign('NOMBRE', $row['nombre']);
            $xtpl->assign('DESCRIPCION', $row['descripcion']);
            $xtpl->assign('TIPO', ($row['tipo']==='t')?'TRUE':'FALSE');
            $xtpl->assign('VIGENCIA', $row['vigencia']);
            Func::btnRecordEdit( $xtpl ,["btnId"=>$row['id']]);
            Func::btnRecordDelete( $xtpl ,["btnId"=>$row['id']]);
            $xtpl->parse('main.rows');
        }
        Func::btnsPutPanel( $xtpl ,[["btnName"=>"Exit"]]);
        $xtpl->parse('main');
        $aView['content'] = $xtpl->out_var('main');
        return $aView;
    }
}

