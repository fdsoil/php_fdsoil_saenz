function submitInsert(val)
{
    relocate('../clasificador_aux/', {});
}

function submitEdit(val)
{
    relocate('../clasificador_aux/', {'id':val});
}

function submitDelete(val)
{
    confirm('¿Desea eliminar este registro?', 'Confirmar', function(ok){
        if (ok)
            relocate('delete/', {'id':val});
        });
}

