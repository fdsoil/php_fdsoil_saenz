-- Function: admini.proveedor_register(integer, character varying, character varying, text, text, character varying)

-- DROP FUNCTION admini.proveedor_register(integer, character varying, character varying, text, text, character varying);

CREATE OR REPLACE FUNCTION admini.proveedor_register(i_id integer, i_rif character varying, i_nombre character varying, i_descripcion text, i_direccion text, i_telefono character varying)
  RETURNS character AS
$BODY$
DECLARE
        v_existe boolean;
        o_return character;
BEGIN
        o_return:='';
        IF i_id=0 THEN
                SELECT CASE WHEN count(id)=0 THEN false ELSE true END INTO v_existe FROM admini.proveedor
                        WHERE rif=i_rif;
                IF  v_existe='f' THEN
                        INSERT INTO admini.proveedor(
                                rif,
                                nombre,
                                descripcion,
                                direccion,
                                telefono)
                        VALUES (
                                i_rif,
                                i_nombre,
                                i_descripcion,
                                i_direccion,
                                i_telefono);
                        o_return:= 'C';
                ELSE
                        o_return:= 'T';
                END IF;
        ELSE
                UPDATE admini.proveedor SET
                        nombre=i_nombre,
                        descripcion=i_descripcion,
                        direccion=i_direccion,
                        telefono=i_telefono
                WHERE id=i_id;
                        o_return:= 'A';
        END IF;
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION admini.proveedor_register(integer, character varying, character varying, text, text, character varying)
  OWNER TO postgres;

-- Function: admini.proveedor_delete(integer)

-- DROP FUNCTION admini.proveedor_delete(integer);

CREATE OR REPLACE FUNCTION admini.proveedor_delete(i_id integer)
  RETURNS character AS
$BODY$
DECLARE
        o_return character;
BEGIN
        o_return:='Z';
        DELETE FROM admini.proveedor WHERE id=i_id;
        o_return:='B';
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION admini.proveedor_delete(integer)
  OWNER TO postgres;

