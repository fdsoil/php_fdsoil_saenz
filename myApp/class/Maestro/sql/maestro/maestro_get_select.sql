SELECT 
    A.id,
    A.numero,
    A.fecha,
    A.id_proveedor,
    A.id_requisicion,
    A.id_forma_pago,
    A.recibido_por,
    A.recibido_fecha,
    A.recibido_hora,
    A.evaluado_por,
    A.evaluado_fecha,
    A.evaluado_hora
    ,B.rif AS des_proveedor
    ,B.nombre AS des_proveedor
    ,B.descripcion AS des_proveedor
    ,B.direccion AS des_proveedor
    ,B.telefono AS des_proveedor
    ,C.numero AS des_requisicion
    ,C.memo AS des_requisicion
    ,C.id_dependencia AS des_requisicion
    ,C.fecha_solicitud AS des_requisicion
    ,C.recibido AS des_requisicion
    ,C.fecha_recibido AS des_requisicion
    ,D.nombre AS des_forma_pago
FROM compra.maestro A
INNER JOIN compra.proveedor B ON A.id_proveedor=B.id
INNER JOIN compra.requisicion C ON A.id_requisicion=C.id
INNER JOIN compra.forma_pago D ON A.id_forma_pago=D.id
{fld:where}
ORDER BY 2;
