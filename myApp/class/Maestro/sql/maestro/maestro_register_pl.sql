SELECT compra.maestro_register(
    {fld:id},
,
    '{fld:fecha}',
    {fld:id_proveedor},
    {fld:id_requisicion},
    {fld:id_forma_pago},
    '{fld:recibido_por}',
    '{fld:recibido_fecha}',
    '{fld:recibido_hora}',
    '{fld:evaluado_por}',
    '{fld:evaluado_fecha}',
    '{fld:evaluado_hora}'
);