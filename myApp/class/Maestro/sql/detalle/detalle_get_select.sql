SELECT 
    A.id,
    A.id_maestro,
    A.id_articulo,
    A.cantidad,
    A.precio
    ,B.nombre AS des_articulo
    ,B.descripcion AS des_articulo
    ,B.id_clasificador AS des_articulo
FROM compra.detalle A
INNER JOIN compra.articulo B ON A.id_articulo=B.id
WHERE A.id_maestro = {fld:id}
ORDER BY 3;
