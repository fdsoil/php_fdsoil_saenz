-- Function: compra.maestro_register(integer, numeric, date, integer, integer, integer, character varying, date, time without time zone, character varying, date, time without time zone)

-- DROP FUNCTION compra.maestro_register(integer, numeric, date, integer, integer, integer, character varying, date, time without time zone, character varying, date, time without time zone);

CREATE OR REPLACE FUNCTION compra.maestro_register(i_id integer, i_numero numeric, i_fecha date, i_id_proveedor integer, i_id_requisicion integer, i_id_forma_pago integer, i_recibido_por character varying, i_recibido_fecha date, i_recibido_hora time without time zone, i_evaluado_por character varying, i_evaluado_fecha date, i_evaluado_hora time without time zone)
  RETURNS json AS
$BODY$
DECLARE
        v_existe boolean;
        v_id integer;
        o_return json;
BEGIN
        o_return:=array_to_json(array['']);
        IF i_id=0 THEN
                SELECT CASE WHEN count(id)=0 THEN false ELSE true END INTO v_existe FROM compra.maestro
                        WHERE numero=i_numero;
                IF  v_existe='f' THEN
                        INSERT INTO compra.maestro(
                                numero,
                                fecha,
                                id_proveedor,
                                id_requisicion,
                                id_forma_pago,
                                recibido_por,
                                recibido_fecha,
                                recibido_hora,
                                evaluado_por,
                                evaluado_fecha,
                                evaluado_hora)
                        VALUES (
                                i_numero,
                                i_fecha,
                                i_id_proveedor,
                                i_id_requisicion,
                                i_id_forma_pago,
                                i_recibido_por,
                                i_recibido_fecha,
                                i_recibido_hora,
                                i_evaluado_por,
                                i_evaluado_fecha,
                                i_evaluado_hora);
                        SELECT max(id) INTO v_id FROM compra.maestro
                                WHERE numero=i_numero;
                        o_return:= array_to_json(array['C', v_id::character varying]);
                ELSE
                        o_return:= array_to_json(array['T']);
                END IF;
        ELSE
                UPDATE compra.maestro SET
                        fecha=i_fecha,
                        id_proveedor=i_id_proveedor,
                        id_requisicion=i_id_requisicion,
                        id_forma_pago=i_id_forma_pago,
                        recibido_por=i_recibido_por,
                        recibido_fecha=i_recibido_fecha,
                        recibido_hora=i_recibido_hora,
                        evaluado_por=i_evaluado_por,
                        evaluado_fecha=i_evaluado_fecha,
                        evaluado_hora=i_evaluado_hora
                WHERE id=i_id;
                        o_return:= array_to_json(array['A']);
        END IF;
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION compra.maestro_register(integer, numeric, date, integer, integer, integer, character varying, date, time without time zone, character varying, date, time without time zone)
  OWNER TO postgres;

-- Function: compra.maestro_delete(integer)

-- DROP FUNCTION compra.maestro_delete(integer);

CREATE OR REPLACE FUNCTION compra.maestro_delete(i_id integer)
  RETURNS character AS
$BODY$
DECLARE
        o_return character;
BEGIN
        o_return:='Z';
        DELETE FROM compra.maestro WHERE id=i_id;
        o_return:='B';
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION compra.maestro_delete(integer)
  OWNER TO postgres;

-- Function: compra.detalle_register(integer, integer, integer, integer, numeric)

-- DROP FUNCTION compra.detalle_register(integer, integer, integer, integer, numeric);

CREATE OR REPLACE FUNCTION compra.detalle_register(i_id integer, i_id_maestro integer, i_id_articulo integer, i_cantidad integer, i_precio numeric)
  RETURNS character AS
$BODY$
DECLARE
        v_existe boolean;
        o_return character;
BEGIN
        o_return:='';
        IF i_id=0 THEN
                INSERT INTO compra.detalle(
                        id_maestro,
                        id_articulo,
                        cantidad,
                        precio)
                VALUES (
                        i_id_maestro,
                        i_id_articulo,
                        i_cantidad,
                        i_precio);
                o_return:= 'C';
        ELSE
                UPDATE compra.detalle SET
                        id_maestro=i_id_maestro,
                        id_articulo=i_id_articulo,
                        cantidad=i_cantidad,
                        precio=i_precio
                WHERE id=i_id;
                o_return:= 'A';
        END IF;
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION compra.detalle_register(integer, integer, integer, integer, numeric)
  OWNER TO postgres;

-- Function: compra.detalle_delete(integer)

-- DROP FUNCTION compra.detalle_delete(integer);

CREATE OR REPLACE FUNCTION compra.detalle_delete(i_id integer)
  RETURNS character AS
$BODY$
DECLARE
        o_return character;
BEGIN
        o_return:='Z';
        DELETE FROM compra.detalle WHERE id=i_id;
        o_return:='B';
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION compra.detalle_delete(integer)
  OWNER TO postgres;

-- Function: compra.detalle_register(integer, integer, integer, integer, numeric)

-- DROP FUNCTION compra.detalle_register(integer, integer, integer, integer, numeric);

CREATE OR REPLACE FUNCTION compra.detalle_register(i_id integer, i_id_maestro integer, i_id_articulo integer, i_cantidad integer, i_precio numeric)
  RETURNS character AS
$BODY$
DECLARE
        v_existe boolean;
        o_return character;
BEGIN
        o_return:='';
        IF i_id=0 THEN
                INSERT INTO compra.detalle(
                        id_maestro,
                        id_articulo,
                        cantidad,
                        precio)
                VALUES (
                        i_id_maestro,
                        i_id_articulo,
                        i_cantidad,
                        i_precio);
                o_return:= 'C';
        ELSE
                UPDATE compra.detalle SET
                        id_maestro=i_id_maestro,
                        id_articulo=i_id_articulo,
                        cantidad=i_cantidad,
                        precio=i_precio
                WHERE id=i_id;
                o_return:= 'A';
        END IF;
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION compra.detalle_register(integer, integer, integer, integer, numeric)
  OWNER TO postgres;

-- Function: compra.detalle_delete(integer)

-- DROP FUNCTION compra.detalle_delete(integer);

CREATE OR REPLACE FUNCTION compra.detalle_delete(i_id integer)
  RETURNS character AS
$BODY$
DECLARE
        o_return character;
BEGIN
        o_return:='Z';
        DELETE FROM compra.detalle WHERE id=i_id;
        o_return:='B';
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION compra.detalle_delete(integer)
  OWNER TO postgres;

-- Function: compra.detalle_register(integer, integer, integer, integer, numeric)

-- DROP FUNCTION compra.detalle_register(integer, integer, integer, integer, numeric);

CREATE OR REPLACE FUNCTION compra.detalle_register(i_id integer, i_id_maestro integer, i_id_articulo integer, i_cantidad integer, i_precio numeric)
  RETURNS character AS
$BODY$
DECLARE
        v_existe boolean;
        o_return character;
BEGIN
        o_return:='';
        IF i_id=0 THEN
                INSERT INTO compra.detalle(
                        id_maestro,
                        id_articulo,
                        cantidad,
                        precio)
                VALUES (
                        i_id_maestro,
                        i_id_articulo,
                        i_cantidad,
                        i_precio);
                o_return:= 'C';
        ELSE
                UPDATE compra.detalle SET
                        id_maestro=i_id_maestro,
                        id_articulo=i_id_articulo,
                        cantidad=i_cantidad,
                        precio=i_precio
                WHERE id=i_id;
                o_return:= 'A';
        END IF;
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION compra.detalle_register(integer, integer, integer, integer, numeric)
  OWNER TO postgres;

-- Function: compra.detalle_delete(integer)

-- DROP FUNCTION compra.detalle_delete(integer);

CREATE OR REPLACE FUNCTION compra.detalle_delete(i_id integer)
  RETURNS character AS
$BODY$
DECLARE
        o_return character;
BEGIN
        o_return:='Z';
        DELETE FROM compra.detalle WHERE id=i_id;
        o_return:='B';
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION compra.detalle_delete(integer)
  OWNER TO postgres;

