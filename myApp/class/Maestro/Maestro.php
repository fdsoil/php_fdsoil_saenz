<?php
namespace myApp;

use \FDSoil\Func as Func;
use \FDSoil\DbFunc as DbFunc;

/** Maestro: Clase para actualizar y consultar la tabla 'maestro'.
* Utiliza '\FDSoil\Func' y '\FDSoil\DbFunc' respectivamente.*/
class Maestro
{

    /** Arreglo asociativo que contiene los correspondientes nombres de
    * campos y sus características respectivas. Dichos campos son los
    * únicos valores permitidos por la clase Maestro a traves de la
    * variable $_POST, para actualizar la tabla 'maestro'.*/
    static private $_aValReqs = [
        "id" => [
            "label" => "Id",
            "required" => false
        ],
        "numero" => [
            "label" => "Numero",
            "required" => false
        ],
        "fecha" => [
            "label" => "Fecha",
            "required" => false
        ],
        "id_proveedor" => [
            "label" => "Id Proveedor",
            "required" => false
        ],
        "id_requisicion" => [
            "label" => "Id Requisicion",
            "required" => false
        ],
        "id_forma_pago" => [
            "label" => "Id Forma Pago",
            "required" => false
        ],
        "recibido_por" => [
            "label" => "Recibido Por",
            "required" => false
        ],
        "recibido_fecha" => [
            "label" => "Recibido Fecha",
            "required" => false
        ],
        "recibido_hora" => [
            "label" => "Recibido Hora",
            "required" => false
        ],
        "evaluado_por" => [
            "label" => "Evaluado Por",
            "required" => false
        ],
        "evaluado_fecha" => [
            "label" => "Evaluado Fecha",
            "required" => false
        ],
        "evaluado_hora" => [
            "label" => "Evaluado Hora",
            "required" => false
        ]
    ];

    /** Devuelve la ruta en que están ubicados los archivos .sql de la clase Maestro.
    * Descripción: Devuelve la ruta en que están ubicados los .sql (querys) de la clase Maestro.
    * @return string La ruta en que están ubicados los archivos .sql (querys) de la clase Maestro.*/
    private function _path() { return '../../../'.$_SESSION['myApp'].'/class/Maestro/sql/maestro/'; }

    /** Obtener registro(s) de la tabla 'maestro'.
    * Descripción: Obtener registro(s) de la tabla 'maestro'.
    * Si el parámetro $label trae el argumento 'LIST', devuelve todos los registros de la tabla 'maestro'.
    * De lo contrario, si el parámetro $label trae el argumento 'REGIST', devuelve un solo registro,
    * siempre y cuando el valor de $_POST['id'] coincida con el ID principal de algún registro asociado.
    * Nota: Requiere el correspondiente valor $_POST['id'] del registro específico a consultar.
    * @param string $label Con sólo dos (2) posibles valores ('LIST' o 'REGIST').
    * @return result Resultado con registro(s) de la tabla 'maestro'.*/
    public function maestroGet($label)
    {
        switch ($label) {
            case 'LIST':
                $arr['where'] = '';
                break;
            case 'REGIST':
                $arr['id'] = $_POST['id'];
                $arr['where'] = Func::replace_data($arr, ' WHERE A.id = {fld:id} ');
                break;
        }
        return DbFunc::exeQryFile(self::_path().'maestro_get_select.sql', $arr);
    }

    /** Actualiza registro de la tabla 'maestro'.
    * Descripción: Realiza actualización del registro ( insert o update ) de la tabla 'maestro'.
    * Nota: Requiere de los respectivos valores procedentes del $_POST para actualizar el registro.*/
    public function maestroRegister()
    {
        $aMsjReqs = Func::valReqs($_POST, self::$_aValReqs);
        if (!$aMsjReqs){
            $_POST = Func::formatReqs($_POST, self::$_aValReqs);
            $row = DbFunc::fetchRow(DbFunc::exeQryFile(self::_path().'maestro_register_pl.sql',$_POST, true, 'ACTUALIZANDO REGISTRO'));
            $msj = $row[0];
        } else {
            $_SESSION['messages'] = $aMsjReqs;
            $msj = 'N';
        }
        return $msj;
    }

    /** Elimina registro de la tabla 'maestro'.
    * Descripción: Realiza eliminación del registro ( delete ) de la tabla 'maestro'.
    * Nota: Requiere el valor $_POST['id'] para buscar y eliminar registro en base de datos.*/ 
    public function maestroDelete()
    {
        $row = DbFunc::fetchRow(DbFunc::exeQryFile(self::_path().'maestro_delete_pl.sql', $_POST, true, 'ELIMINANDO REGISTRO'));
        if ($row[0]!='B')
            Func::adminMsj($row[0],1);
        else
            header("Location: ".$_SERVER['HTTP_REFERER']);
    }

    /** Lista de tabla foránea 'proveedor'.
    * Descripción: Devuelve el resultado de la consulta de la de tabla foránea 'proveedor'.
    * @return result Resultado de la consulta de la de tabla foránea 'proveedor'.*/ 
    public function proveedorList()
    {
        return DbFunc::exeQryFile(self::_path().'proveedor_list_select.sql', $_POST);
    }

    /** Lista de tabla foránea 'requisicion'.
    * Descripción: Devuelve el resultado de la consulta de la de tabla foránea 'requisicion'.
    * @return result Resultado de la consulta de la de tabla foránea 'requisicion'.*/ 
    public function requisicionList()
    {
        return DbFunc::exeQryFile(self::_path().'requisicion_list_select.sql', $_POST);
    }

    /** Lista de tabla foránea 'forma_pago'.
    * Descripción: Devuelve el resultado de la consulta de la de tabla foránea 'forma_pago'.
    * @return result Resultado de la consulta de la de tabla foránea 'forma_pago'.*/ 
    public function formaPagoList()
    {
        return DbFunc::exeQryFile(self::_path().'forma_pago_list_select.sql', $_POST);
    }

}

