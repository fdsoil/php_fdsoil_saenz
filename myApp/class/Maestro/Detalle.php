<?php
namespace myApp\Maestro;

use \FDSoil\Func as Func;
use \FDSoil\DbFunc as DbFunc;

/** Detalle: Clase para actualizar y consultar la tabla 'detalle'.
* Utiliza '\FDSoil\Func' y '\FDSoil\DbFunc' respectivamente.*/
class Detalle
{

    /** Devuelve la ruta en que están ubicados los archivos .sql de la clase Detalle.
    * Descripción: Devuelve la ruta en que están ubicados los .sql (querys) de la clase Detalle.
    * @return string La ruta en que están ubicados los archivos .sql (querys) de la clase Detalle.*/
    private function _path() { return '../../../'.$_SESSION['myApp'].'/class/Maestro/sql/detalle/'; }

    /** Actualiza registro de la tabla 'detalle'.
    * Descripción: Realiza actualización del registro ( insert o update ) de la tabla 'detalle'.
    * Nota: Requiere de los respectivos valores procedentes del $_POST para actualizar el registro.*/
    public function detalleRegister()
    {
        $row = DbFunc::fetchRow(DbFunc::exeQryFile(self::_path().'detalle_register_pl.sql',$_POST, true, 'ACTUALIZANDO REGISTRO'));
        return $row[0];
    }

    /** Obtener registro(s) de la tabla 'detalle'.
    * Descripción: Obtener registro(s) de la tabla 'detalle'.
    * Nota: Requiere el correspondiente valor $_POST identificativo del registro específico a consultar.
    * @return result Resultado con registro(s) de la tabla 'detalle'.*/
    public function detalleGet()
    {
        return DbFunc::fetchAllAssoc(DbFunc::exeQryFile(self::_path().'detalle_get_select.sql', $_POST));
    }

    /** Elimina registro de la tabla 'detalle'.
    * Descripción: Realiza eliminación del registro ( delete ) de la tabla 'detalle'.
    * Nota: Requiere el valor $_POST identificativo para buscar y eliminar registro en base de datos.*/ 
    public function detalleDelete()
    {
        $row = DbFunc::fetchRow(DbFunc::exeQryFile(self::_path().'detalle_delete_pl.sql', $_POST, true, 'ELIMINANDO REGISTRO'));
        return $row[0];
    }

    /** Lista de tabla foránea 'articulo'.
    * Descripción: Devuelve el resultado de la consulta de la de tabla foránea 'articulo'.
    * @return result Resultado de la consulta de la de tabla foránea 'articulo'.*/ 
    public function articuloList() { return DbFunc::exeQryFile(self::_path().'articulo_list_select.sql', $_POST); }

}

