SELECT 
    id,
    codigo,
    nombre,
    descripcion,
    tipo,
    vigencia
FROM admini.clasificador 
{fld:where}
ORDER BY 2;
