-- Function: admini.clasificador_register(integer, character, character varying, text, boolean, numeric)

-- DROP FUNCTION admini.clasificador_register(integer, character, character varying, text, boolean, numeric);

CREATE OR REPLACE FUNCTION admini.clasificador_register(i_id integer, i_codigo character, i_nombre character varying, i_descripcion text, i_tipo boolean, i_vigencia numeric)
  RETURNS character AS
$BODY$
DECLARE
        v_existe boolean;
        o_return character;
BEGIN
        o_return:='';
        IF i_id=0 THEN
                SELECT CASE WHEN count(id)=0 THEN false ELSE true END INTO v_existe FROM admini.clasificador
                        WHERE codigo=i_codigo
                                        AND vigencia=i_vigencia;
                IF  v_existe='f' THEN
                        INSERT INTO admini.clasificador(
                                codigo,
                                nombre,
                                descripcion,
                                tipo,
                                vigencia)
                        VALUES (
                                i_codigo,
                                i_nombre,
                                i_descripcion,
                                i_tipo,
                                i_vigencia);
                        o_return:= 'C';
                ELSE
                        o_return:= 'T';
                END IF;
        ELSE
                UPDATE admini.clasificador SET
                        nombre=i_nombre,
                        descripcion=i_descripcion,
                        tipo=i_tipo,
                WHERE id=i_id;
                        o_return:= 'A';
        END IF;
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION admini.clasificador_register(integer, character, character varying, text, boolean, numeric)
  OWNER TO postgres;

-- Function: admini.clasificador_delete(integer)

-- DROP FUNCTION admini.clasificador_delete(integer);

CREATE OR REPLACE FUNCTION admini.clasificador_delete(i_id integer)
  RETURNS character AS
$BODY$
DECLARE
        o_return character;
BEGIN
        o_return:='Z';
        DELETE FROM admini.clasificador WHERE id=i_id;
        o_return:='B';
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION admini.clasificador_delete(integer)
  OWNER TO postgres;

