-- Function: admini.dependencia_register(integer, character varying)

-- DROP FUNCTION admini.dependencia_register(integer, character varying);

CREATE OR REPLACE FUNCTION admini.dependencia_register(i_id integer, i_nombre character varying)
  RETURNS character AS
$BODY$
DECLARE
        v_existe boolean;
        o_return character;
BEGIN
        o_return:='';
        IF i_id=0 THEN
                SELECT CASE WHEN count(id)=0 THEN false ELSE true END INTO v_existe FROM admini.dependencia
                        WHERE nombre=i_nombre;
                IF  v_existe='f' THEN
                        INSERT INTO admini.dependencia(
                                nombre)
                        VALUES (
                                i_nombre);
                        o_return:= 'C';
                ELSE
                        o_return:= 'T';
                END IF;
        ELSE
                UPDATE admini.dependencia SET
                WHERE id=i_id;
                        o_return:= 'A';
        END IF;
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION admini.dependencia_register(integer, character varying)
  OWNER TO postgres;

-- Function: admini.dependencia_delete(integer)

-- DROP FUNCTION admini.dependencia_delete(integer);

CREATE OR REPLACE FUNCTION admini.dependencia_delete(i_id integer)
  RETURNS character AS
$BODY$
DECLARE
        o_return character;
BEGIN
        o_return:='Z';
        DELETE FROM admini.dependencia WHERE id=i_id;
        o_return:='B';
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION admini.dependencia_delete(integer)
  OWNER TO postgres;

