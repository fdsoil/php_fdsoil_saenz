<?php
namespace myApp;

use \FDSoil\Func as Func;
use \FDSoil\DbFunc as DbFunc;

/** Articulo: Clase para actualizar y consultar la tabla 'articulo'.
* Utiliza '\FDSoil\Func' y '\FDSoil\DbFunc' respectivamente.*/
class Articulo
{

    /** Arreglo asociativo que contiene los correspondientes nombres de
    * campos y sus características respectivas. Dichos campos son los
    * únicos valores permitidos por la clase Articulo a traves de la
    * variable $_POST, para actualizar la tabla 'articulo'.*/
    static private $_aValReqs = [
        "id" => [
            "label" => "Id",
            "required" => false
        ],
        "nombre" => [
            "label" => "Nombre",
            "required" => false
        ],
        "descripcion" => [
            "label" => "Descripcion",
            "required" => false
        ],
        "id_clasificador" => [
            "label" => "Id Clasificador",
            "required" => false
        ]
    ];

    /** Devuelve la ruta en que están ubicados los archivos .sql de la clase Articulo.
    * Descripción: Devuelve la ruta en que están ubicados los .sql (querys) de la clase Articulo.
    * @return string La ruta en que están ubicados los archivos .sql (querys) de la clase Articulo.*/
    private function _path() { return '../../../'.$_SESSION['myApp'].'/class/Articulo/sql/articulo/'; }

    /** Obtener registro(s) de la tabla 'articulo'.
    * Descripción: Obtener registro(s) de la tabla 'articulo'.
    * Si el parámetro $label trae el argumento 'LIST', devuelve todos los registros de la tabla 'articulo'.
    * De lo contrario, si el parámetro $label trae el argumento 'REGIST', devuelve un solo registro,
    * siempre y cuando el valor de $_POST['id'] coincida con el ID principal de algún registro asociado.
    * Nota: Requiere el correspondiente valor $_POST['id'] del registro específico a consultar.
    * @param string $label Con sólo dos (2) posibles valores ('LIST' o 'REGIST').
    * @return result Resultado con registro(s) de la tabla 'articulo'.*/
    public function articuloGet($label)
    {
        switch ($label) {
            case 'LIST':
                $arr['where'] = '';
                break;
            case 'REGIST':
                $arr['id'] = $_POST['id'];
                $arr['where'] = Func::replace_data($arr, ' WHERE A.id = {fld:id} ');
                break;
        }
        return DbFunc::exeQryFile(self::_path().'articulo_get_select.sql', $arr);
    }

    /** Actualiza registro de la tabla 'articulo'.
    * Descripción: Realiza actualización del registro ( insert o update ) de la tabla 'articulo'.
    * Nota: Requiere de los respectivos valores procedentes del $_POST para actualizar el registro.*/
    public function articuloRegister()
    {
        $aMsjReqs = Func::valReqs($_POST, self::$_aValReqs);
        if (!$aMsjReqs){
            $_POST = Func::formatReqs($_POST, self::$_aValReqs);
            $row = DbFunc::fetchRow(DbFunc::exeQryFile(self::_path().'articulo_register_pl.sql',$_POST, true, 'ACTUALIZANDO REGISTRO'));
            $msj = $row[0];
            $np = 2;
        } else {
            $_SESSION['messages'] = $aMsjReqs;
            $msj = 'N';
            $np = 1;
        }
        Func::adminMsj($msj,$np);
    }

    /** Elimina registro de la tabla 'articulo'.
    * Descripción: Realiza eliminación del registro ( delete ) de la tabla 'articulo'.
    * Nota: Requiere el valor $_POST['id'] para buscar y eliminar registro en base de datos.*/ 
    public function articuloDelete()
    {
        $row = DbFunc::fetchRow(DbFunc::exeQryFile(self::_path().'articulo_delete_pl.sql', $_POST, true, 'ELIMINANDO REGISTRO'));
        if ($row[0]!='B')
            Func::adminMsj($row[0],1);
        else
            header("Location: ".$_SERVER['HTTP_REFERER']);
    }

    /** Lista de tabla foránea 'clasificador'.
    * Descripción: Devuelve el resultado de la consulta de la de tabla foránea 'clasificador'.
    * @return result Resultado de la consulta de la de tabla foránea 'clasificador'.*/ 
    public function clasificadorList()
    {
        return DbFunc::exeQryFile(self::_path().'clasificador_list_select.sql', $_POST);
    }

}

