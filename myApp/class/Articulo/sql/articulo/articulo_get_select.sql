SELECT 
    A.id,
    A.nombre,
    A.descripcion,
    A.id_clasificador
    ,B.codigo AS des_clasificador
    ,B.nombre AS des_clasificador
    ,B.descripcion AS des_clasificador
    ,B.tipo AS des_clasificador
    ,B.vigencia AS des_clasificador
FROM admini.articulo A
INNER JOIN admini.clasificador B ON A.id_clasificador=B.id
{fld:where}
ORDER BY 2;
