-- Function: admini.articulo_register(integer, character varying, text, integer)

-- DROP FUNCTION admini.articulo_register(integer, character varying, text, integer);

CREATE OR REPLACE FUNCTION admini.articulo_register(i_id integer, i_nombre character varying, i_descripcion text, i_id_clasificador integer)
  RETURNS character AS
$BODY$
DECLARE
        v_existe boolean;
        o_return character;
BEGIN
        o_return:='';
        IF i_id=0 THEN
                SELECT CASE WHEN count(id)=0 THEN false ELSE true END INTO v_existe FROM admini.articulo
                        WHERE nombre=i_nombre;
                IF  v_existe='f' THEN
                        INSERT INTO admini.articulo(
                                nombre,
                                descripcion,
                                id_clasificador)
                        VALUES (
                                i_nombre,
                                i_descripcion,
                                i_id_clasificador);
                        o_return:= 'C';
                ELSE
                        o_return:= 'T';
                END IF;
        ELSE
                UPDATE admini.articulo SET
                        descripcion=i_descripcion,
                        id_clasificador=i_id_clasificador
                WHERE id=i_id;
                        o_return:= 'A';
        END IF;
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION admini.articulo_register(integer, character varying, text, integer)
  OWNER TO postgres;

-- Function: admini.articulo_delete(integer)

-- DROP FUNCTION admini.articulo_delete(integer);

CREATE OR REPLACE FUNCTION admini.articulo_delete(i_id integer)
  RETURNS character AS
$BODY$
DECLARE
        o_return character;
BEGIN
        o_return:='Z';
        DELETE FROM admini.articulo WHERE id=i_id;
        o_return:='B';
        RETURN o_return;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION admini.articulo_delete(integer)
  OWNER TO postgres;

